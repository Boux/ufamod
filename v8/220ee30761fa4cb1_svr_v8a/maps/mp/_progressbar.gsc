/*
  _progressbar.gsc by ShadowLord


  Creates a Progress bar on the caller of the function
  Name    = name of the progress bar ( string )
  x       = starting x for the progress bar ( integer between 0 and 480 )
  y       = starting y for the progress bar ( integer between 0 and 640 )
  width   = total width of the progress bar's background ( integer between 0 and 480 )
  height  = total heigth of the progress bar's background ( integer between 0 and 640 )
  alignX  = horizontal alignment for the progress bar ( string "left," "right," or "center" )
  alignY  = vertical alignment for the progress bar ( "up," "middle," or "bottom" )
  bAlpha  = transparency of the progress bar's background ( float between 0 and 1 or undefined )
  pAlpha  = transparency of the progress bar ( float between 0 and 1 or undefined )
  bColor  = color of the progress bar's background ( ( red, green, blue ) or undefined )
  pColor  = color of the progress bar ( ( red, green, blue ) or undefined )
  bShader = shader of the progress bar's background
  pShader = shader of the progress bar
  pTime   = time it takes for the progress bar to reach 100%
*/
main( x, y, iWidth, iHeight, sAlignX, sAlignY, bAlpha, pAlpha, bColor, pColor, bShader, pShader, pTime )
{
	//Background
	self.ufa_progressbarbackground = newClientHudElem( self );
	self.ufa_progressbarbackground.alignX = sAlignX;
	self.ufa_progressbarbackground.alignY = sAlignY;
	self.ufa_progressbarbackground.x = x;
	self.ufa_progressbarbackground.y = y;
	if( isDefined( bAlpha ) )  self.ufa_progressbarbackground.alpha = bAlpha;
	if( isDefined( bColor ) )  self.ufa_progressbarbackground.color = bColor;
	self.ufa_progressbarbackground setShader( bShader, iWidth, iHeight );

	//Progress Bar
	self.ufa_progressbar = newClientHudElem( self );
	self.ufa_progressbar.alignX = sAlignX;
	self.ufa_progressbar.alignY = sAlignY;
	self.ufa_progressbar.x = x;
	self.ufa_progressbar.y = y;
	if( isDefined( pAlpha ) )  self.ufa_progressbar.alpha = pAlpha;
	if( isDefined( pColor ) )  self.ufa_progressbar.color = pColor;
	self.ufa_progressbar setShader( pShader, 0, ( iHeight - 4 ) );
	self.ufa_progressbar scaleOverTime( pTime, ( iWidth - 5 ), ( iHeight - 4 ) );
}
