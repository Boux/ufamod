// ----------------------------------------------------------------------------------
//	PlayerSpawnLoadout
//
// 		Sets up the player weapons
// ----------------------------------------------------------------------------------
PlayerSpawnLoadout()
{	
	self thread maps\mp\gametypes\_teams_level::giveWeaponLevel();
	self thread maps\mp\gametypes\_teams_level::showPlayerSkills();

	self setweaponslotammo("primary", "999");
	self setweaponslotammo("primaryb", "999");
	self setweaponslotammo("pistol", "999");
	self setweaponslotammo("grenade", "999");
	self setweaponslotammo("smokegrenade", "999");
//	self setweaponslotammo("satchel", "999");
	self setweaponslotclipammo("primary", "999");
	self setweaponslotclipammo("primaryb", "999");
	self setweaponslotclipammo("pistol", "999");
	self setweaponslotclipammo("grenade", "999");
	self setweaponslotclipammo("smokegrenade", "999");
//	self setweaponslotclipammo("satchel", "999");
}

// ----------------------------------------------------------------------------------
//	GetGunAmmo
//
// 		returns the ammo count that the player will get for the weapon
// ----------------------------------------------------------------------------------
GetGunAmmo(weapon)
{
	// if battle rank is on then call the battle rank function
	if ( isDefined(level.battlerank) && level.battlerank)
	{
		return maps\mp\gametypes\_rank_ldm_ltdm_gmi::GetGunAmmo(weapon);
	}
	
	switch(weapon)
	{
		//American Weapons
		case "m1carbine_mp":
			return 60;
		case "m1garand_mp":
			return 56;
		case "springfield_mp": 
			return 25;
		case "thompson_mp": 
		case "thompson_semi_mp": 
			return 120;
		case "bar_mp": 
		case "bar_slow_mp": 
			return 100;
		case "mg30cal_mp":
			return 225;
		//British Weapons
		case "enfield_mp":
			return 60;
		case "sten_mp": 
		case "sten_silenced_mp":
			return 128;
		case "bren_mp": 
			return 90;
		//Russian Weapons
		case "mosin_nagant_mp":
			return 60;
		case "svt40_mp":
			return 60;
		case "mosin_nagant_sniper_mp":
			return 25;
		case "ppsh_mp":
		case "ppsh_semi_mp":
			return 142;
		case "dp28_mp":
			return 225;
		//German Weapons
		case "kar98k_mp": 
			return 60;
		case "gewehr43_mp":
			return 60;
		case "kar98k_sniper_mp":
			return 25;
		case "mp40_mp":
			return 128;
		case "mp44_mp":
		case "mp44_semi_mp":
			return 90;
		case "mg34_mp":
			return 225;
		case "panzerfaust_mp":
			return 1;
		case "panzerschreck_mp":
			return 3;
		case "bazooka_mp":
			return 3;
		case "fg42_mp":
		case "fg42_semi_mp":
			return 60;
		case "flamethrower_mp":
			return 300;
		// unrecognized weapon
		default:
		   	return 0;
		}
		
	return 0;
}

// ----------------------------------------------------------------------------------
//	GetPistolAmmo
//
// 		returns the ammo count that the player will get for the weapon
// ----------------------------------------------------------------------------------
GetPistolAmmo(weapon)
{
	// if battle rank is on then call the battle rank function
	if ( isDefined(level.battlerank) && level.battlerank)
	{
		return maps\mp\gametypes\_rank_ldm_ltdm_gmi::GetPistolAmmo(weapon);
	}
	
	// fill em up
	return 999;
}