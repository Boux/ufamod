main()
{
	spawnpointname = "mp_teamdeathmatch_spawn";
	spawnpoints = getentarray(spawnpointname, "classname");
	// Get deathmatch spawn points if TDM does not exist
	if(!spawnpoints.size)
	{
		spawnpointname = "mp_deathmatch_spawn";
		spawnpoints = getentarray(spawnpointname, "classname");
	}
	if(!spawnpoints.size)
	{
		maps\mp\gametypes\_callbacksetup::AbortLevel();
		return;
	}

	for(i = 0; i < spawnpoints.size; i++)
		spawnpoints[i] placeSpawnpoint();

	level.callbackStartGameType = ::Callback_StartGameType;
	level.callbackPlayerConnect = ::Callback_PlayerConnect;
	level.callbackPlayerDisconnect = ::Callback_PlayerDisconnect;
	level.callbackPlayerDamage = ::Callback_PlayerDamage;
	level.callbackPlayerKilled = ::Callback_PlayerKilled;

	maps\mp\gametypes\_callbacksetup::SetupCallbacks();

	allowed[0] = "jump";
	maps\mp\gametypes\_gameobjects::main(allowed);
	
	maps\mp\gametypes\_rank_gmi::InitializeBattleRank();
	maps\mp\gametypes\_secondary_gmi::Initialize();
	
	if(getCvar("scr_jump_timelimit") == "")		// Time limit per map
		setCvar("scr_jump_timelimit", "60");
	else if(getCvarFloat("scr_jump_timelimit") > 60)
		setCvar("scr_jump_timelimit", "60");
	level.timelimit = getCvarFloat("scr_jump_timelimit");
	setCvar("ui_jump_timelimit", level.timelimit);
	makeCvarServerInfo("ui_jump_timelimit", "60");

	if(getCvar("scr_jump_scorelimit") == "")		// Score limit per map
		setCvar("scr_jump_scorelimit", "50");
	level.scorelimit = getCvarInt("scr_jump_scorelimit");
	setCvar("ui_jump_scorelimit", level.scorelimit);
	makeCvarServerInfo("ui_jump_scorelimit", "50");

	if(getCvar("scr_forcerespawn") == "")		// Force respawning
		setCvar("scr_forcerespawn", "0");

	if(getCvar("scr_battlerank") == "")		
		setCvar("scr_battlerank", "0");	//default is ON
	level.battlerank = getCvarint("scr_battlerank");
	setCvar("ui_battlerank", level.battlerank);
	makeCvarServerInfo("ui_battlerank", "0");

	if(getCvar("scr_shellshock") == "")		// controls whether or not players get shellshocked from grenades or rockets
		setCvar("scr_shellshock", "0");
	setCvar("ui_shellshock", getCvar("scr_shellshock"));
	makeCvarServerInfo("ui_shellshock", "0");

	if(!isDefined(game["compass_range"]))		// set up the compass range.
		game["compass_range"] = 1024;		
	setCvar("cg_hudcompassMaxRange", game["compass_range"]);
	makeCvarServerInfo("cg_hudcompassMaxRange", "0");

	if(getCvar("scr_drophealth") == "")		// Free look spectator
		setCvar("scr_drophealth", "1");

	// turn off ceasefire
	level.ceasefire = 0;
	setCvar("scr_ceasefire", "0");

	killcam = getCvar("scr_killcam");
	if(killcam != "0")				// Kill cam
		killcam = "0";
	setCvar("scr_killcam", killcam, true);
	level.killcam = getCvarInt("scr_killcam");

	if(getCvar("scr_allow_jump") == "")		// Jump Mod
		setCvar("scr_allow_jump", "1");
	if(getCvar("scr_jump_friendlyfire") == "")	// Anti-Kill
		setCvar("scr_jump_friendlyfire", "1");
	if(getCvar("ufa_savejump") == "")		// Anti-savejump toggle enable = disable anti-savejump, disable = enable anti-savejump
		setCvar("ufa_savejump", "1");

	// CJ Vote
	if(getCvar("scr_jump_vote") == "")
		setCvar("scr_jump_vote", "0");
	
	// CJ Vote Time
	if(getCvar("scr_jump_votetime") == "")
		setCvar("scr_jump_votetime", "10");
	level.votewait = getCvarInt("scr_jump_votetime");

	if(!isDefined(game["state"]))
		game["state"] = "playing";

	// this is just to define this variable to other scripts that use it dont crash
	level.drawfriend = 0;

	level.QuickMessageToAll = true;
	level.mapended = false;
	level.healthqueue = [];
	level.healthqueuecurrent = 0;

	if(level.killcam >= 1)
		setarchive(true);
}

Callback_StartGameType()
{
	ufa\master\_settings::Callback_StartGameType();
	maps\mp\_ufacmds::Callback_StartGameType();
	thread ufa\master\_voting::main(); // Drofder

	// defaults if not defined in level script
	if(!isDefined(game["allies"]))
		game["allies"] = "russian";
	if(!isDefined(game["axis"]))
		game["axis"] = "german";

	if(!isDefined(game["layoutimage"]))
		game["layoutimage"] = "default";
	layoutname = "levelshots/layouts/hud@layout_" + game["layoutimage"];
	precacheShader(layoutname);
	setCvar("scr_layoutimage", layoutname);
	makeCvarServerInfo("scr_layoutimage", "");

	// server cvar overrides
	if(getCvar("scr_allies") != "")
		game["allies"] = getCvar("scr_allies");
	if(getCvar("scr_axis") != "")
		game["axis"] = getCvar("scr_axis");

	game["menu_serverinfo"] = "serverinfo_" + getCvar("g_gametype");
	game["menu_team"] = "team_" + game["allies"] + game["axis"];
	game["menu_weapon_allies"] = "weapon_" + game["allies"];
	game["menu_weapon_axis"] = "weapon_" + game["axis"];
	game["menu_viewmap"] = "viewmap";
	game["menu_callvote"] = "callvote";
	game["menu_quickcommands"] = "quickcommands";
	game["menu_quickstatements"] = "quickstatements";
	game["menu_quickresponses"] = "quickresponses";
	game["menu_quickvehicles"] = "quickvehicles";
	game["menu_quickrequests"] = "quickrequests";
	game["menu_quickjump"] = "quickjump";

	precacheString(&"MPSCRIPT_PRESS_ACTIVATE_TO_RESPAWN");
	precacheString(&"MPSCRIPT_KILLCAM");
	precacheString(&"GMI_MP_CEASEFIRE");
	precacheString(&"^7Total^0-^4Saves^0-^5:");
	precacheString(&"^7Total^0-^2Loads^0-^3:");
	precacheString(&"^93^0-^9Last^0-^9Saved^0-^9Positions");
	precacheString(&"^1Unknown");

	precacheMenu(game["menu_serverinfo"]);
	precacheMenu(game["menu_team"]);
	precacheMenu(game["menu_weapon_allies"]);
	precacheMenu(game["menu_weapon_axis"]);
	precacheMenu(game["menu_viewmap"]);
	precacheMenu(game["menu_callvote"]);
	precacheMenu(game["menu_quickcommands"]);
	precacheMenu(game["menu_quickstatements"]);
	precacheMenu(game["menu_quickresponses"]);
	precacheMenu(game["menu_quickvehicles"]);
	precacheMenu(game["menu_quickrequests"]);
	precacheMenu(game["menu_quickjump"]);

	precacheShader("black");
	precacheShader("hudScoreboard_mp");
	precacheShader("gfx/hud/hud@mpflag_none.tga");
	precacheShader("gfx/hud/hud@mpflag_spectator.tga");
	precacheStatusIcon("gfx/hud/hud@status_dead.tga");
	precacheStatusIcon("gfx/hud/hud@status_connecting.tga");
	precacheItem("item_health");

	maps\mp\gametypes\_teams::modeltype();
	maps\mp\gametypes\_teams::precache();
	maps\mp\gametypes\_teams::initGlobalCvars();
	maps\mp\gametypes\_teams::initWeaponCvars();
	maps\mp\gametypes\_teams::restrictPlacedWeapons();
	thread maps\mp\gametypes\_teams::updateGlobalCvars();
	thread maps\mp\gametypes\_teams::updateWeaponCvars();

	setClientNameMode("auto_change");

	thread startGame();
//	thread addBotClients(); // For development testing
	thread updateGametypeCvars();
	if(getCvar("ufa_savejump") == "0")
	{
		setCvar("ufa_savejump", "0");
	}
	else
	{
		setCvar("ufa_savejump", "1");
	}
}

Callback_PlayerConnect()
{
	maps\mp\_ufacmds::Callback_PlayerConnect(); //Added by ShadowLord

	self.statusicon = "gfx/hud/hud@status_connecting.tga";
	self waittill("begin");
	self.statusicon = "";

	if(!isdefined(self.totalSaves))
		self.totalSaves = 0;
	if(!isdefined(self.totalLoads))
		self.totalLoads = 0;

	iprintln(&"MPSCRIPT_CONNECTED", self);

	lpselfnum = self getEntityNumber();
	lpselfguid = self getGuid();
	logPrint("J;" + lpselfguid + ";" + lpselfnum + ";" + self.name + "\n");

	// set the cvar for the map quick bind
	self setClientCvar("g_scriptQuickMap", game["menu_viewmap"]);

	if(game["state"] == "intermission")
	{
		spawnIntermission();
		return;
	}

	level endon("intermission");

		if(getCvarInt("scr_allow_jump") == 1)
	{
		if(getCvarInt("ufa_savejump") == 1)
		{
			self thread ufa\master\_jump::_MeleeKeyA(); // Professor Inuyasha
			self thread ufa\master\_jump::_UseKeyA(); // Professor Inuyasha
		}
		else
		{
			self thread ufa\master\_jump::_MeleeKeyB(); // Professor Inuyasha
			self thread ufa\master\_jump::_UseKeyB(); // Professor Inuyasha
		}
	}

	// start the vsay thread
	self thread maps\mp\gametypes\_teams::vsay_monitor();

	if(isDefined(self.pers["team"]) && self.pers["team"] != "spectator")
	{
		self setClientCvar("ui_weapontab", "1");
		self.sessionteam = "none";

		if(self.pers["team"] == "allies")
			self setClientCvar("g_scriptMainMenu", game["menu_weapon_allies"]);
		else
			self setClientCvar("g_scriptMainMenu", game["menu_weapon_axis"]);

		if(isDefined(self.pers["weapon"]))
			spawnPlayer();
		else
		{
			if(getCvarInt("ufa_savejump") == 1)
			{
				self notify("end_saveposition_threadsA"); // Professor Inuyasha
			}
			else
			{
				self notify("end_saveposition_threadsB"); // Professor Inuyasha
			}

			spawnSpectator();

			if(getCvarInt("scr_allow_jump") == 1) // Professor Inuyasha
				self ufa\master\_jump::doHUDMessages(); // Professor Inuyasha

			if(self.pers["team"] == "allies")
				self openMenu(game["menu_weapon_allies"]);
			else
				self openMenu(game["menu_weapon_axis"]);
		}
	}
	else
	{
		self setClientCvar("g_scriptMainMenu", game["menu_team"]);
		self setClientCvar("ui_weapontab", "0");

		if(!isDefined(self.pers["skipserverinfo"]))
			self openMenu(game["menu_serverinfo"]);

		self.pers["team"] = "spectator";
		self.sessionteam = "spectator";

				if(getCvarInt("ufa_savejump") == 1)
		{
			self notify("end_saveposition_threadsA"); // Professor Inuyasha
		}
		else
		{
			self notify("end_saveposition_threadsB"); // Professor Inuyasha
		}

		spawnSpectator();

		if(getCvarInt("scr_allow_jump") == 1) // Professor Inuyasha
			self ufa\master\_jump::doHUDMessages(); // Professor Inuyasha
	}

	for(;;)
	{
		self waittill("menuresponse", menu, response);

		if(menu == game["menu_serverinfo"] && response == "close")
		{
			self.pers["skipserverinfo"] = true;
			self openMenu(game["menu_team"]);
		}

		if(response == "open" || response == "close")
			continue;

		if(menu == game["menu_team"])
		{
			switch(response)
			{
			case "allies":
			case "axis":
			case "autoassign":
				if(response == "autoassign")
				{
					teams[0] = "allies";
					teams[1] = "axis";
					response = teams[randomInt(2)];
				}

				if(response == self.pers["team"] && self.sessionstate == "playing")
					break;

				if(response != self.pers["team"] && self.sessionstate == "playing")
					self suicide();

				self notify("end_respawn");

				self.pers["team"] = response;
				self.pers["weapon"] = undefined;
				self.pers["savedmodel"] = undefined;

				// if there are weapons the user can select then open the weapon menu
				if ( maps\mp\gametypes\_teams::isweaponavailable(self.pers["team"]) )
				{
					if(self.pers["team"] == "allies")
					{
						menu = game["menu_weapon_allies"];
					}
					else
					{
						menu = game["menu_weapon_axis"];
					}

					self setClientCvar("ui_weapontab", "1");
					self openMenu(menu);
				}
				else
				{
					self setClientCvar("ui_weapontab", "0");
					self menu_spawn("none");
				}

				self setClientCvar("g_scriptMainMenu", menu);
				break;

			case "spectator":
				if(self.pers["team"] != "spectator")
				{
					self.pers["team"] = "spectator";
					self.pers["weapon"] = undefined;
					self.pers["savedmodel"] = undefined;
					
					self.sessionteam = "spectator";
					self setClientCvar("g_scriptMainMenu", game["menu_team"]);
					self setClientCvar("ui_weapontab", "0");

					if(getCvarInt("ufa_savejump") == 1)
					{
						self notify("end_saveposition_threadsA");
					}
					else
					{
						self notify("end_saveposition_threadsB");
					}
					spawnSpectator();
					if(getCvarInt("scr_allow_jump") == 1)
						self ufa\master\_jump::doHUDMessages();
				}
				break;

			case "weapon":
				if(self.pers["team"] == "allies")
					self openMenu(game["menu_weapon_allies"]);
				else if(self.pers["team"] == "axis")
					self openMenu(game["menu_weapon_axis"]);
				break;

			case "viewmap":
				self openMenu(game["menu_viewmap"]);
				break;

			case "callvote":
				self openMenu(game["menu_callvote"]);
				break;
			}
		}
		else if(menu == game["menu_weapon_allies"] || menu == game["menu_weapon_axis"])
		{
			if(response == "team")
			{
				self openMenu(game["menu_team"]);
				continue;
			}
			else if(response == "viewmap")
			{
				self openMenu(game["menu_viewmap"]);
				continue;
			}
			else if(response == "callvote")
			{
				self openMenu(game["menu_callvote"]);
				continue;
			}

			if(!isDefined(self.pers["team"]) || (self.pers["team"] != "allies" && self.pers["team"] != "axis"))
				continue;
				
			weapon = self maps\mp\gametypes\_teams::restrict(response);

			if(weapon == "restricted")
			{
				self openMenu(menu);
				continue;
			}

			if(isDefined(self.pers["weapon"]) && self.pers["weapon"] == weapon)
				continue;

			menu_spawn(weapon);
		}
		else if(menu == game["menu_viewmap"])
		{
			switch(response)
			{
			case "team":
				self openMenu(game["menu_team"]);
				break;

			case "weapon":
				if(self.pers["team"] == "allies")
					self openMenu(game["menu_weapon_allies"]);
				else if(self.pers["team"] == "axis")
					self openMenu(game["menu_weapon_axis"]);
				break;

			case "callvote":
				self openMenu(game["menu_callvote"]);
				break;
			}
		}
		else if(menu == game["menu_callvote"])
		{
			switch(response)
			{
			case "team":
				self openMenu(game["menu_team"]);
				break;
			case "weapon":
				if(self.pers["team"] == "allies")
					self openMenu(game["menu_weapon_allies"]);
				else if(self.pers["team"] == "axis")
					self openMenu(game["menu_weapon_axis"]);
				break;
			case "viewmap":
				self openMenu(game["menu_viewmap"]);
				break;
			}
		}
		else if(menu == game["menu_quickcommands"])
			maps\mp\gametypes\_teams::quickcommands(response);
		else if(menu == game["menu_quickstatements"])
			maps\mp\gametypes\_teams::quickstatements(response);
		else if(menu == game["menu_quickresponses"])
			maps\mp\gametypes\_teams::quickresponses(response);
		else if(menu == game["menu_quickvehicles"])
			maps\mp\gametypes\_teams::quickvehicles(response);
		else if(menu == game["menu_quickrequests"])
			maps\mp\gametypes\_teams::quickrequests(response);
		else if(menu == game["menu_quickjump"])
			quickjump(response);
	}
}

Callback_PlayerDisconnect()
{
	self ufa\master\_settings::PlayerDisconnect();
	maps\mp\_ufacmds::Callback_PlayerDisconnect(); //Added by ShadowLord

	iprintln(&"MPSCRIPT_DISCONNECTED", self);

	lpselfnum = self getEntityNumber();
	lpselfguid = self getGuid();
	logPrint("Q;" + lpselfguid + ";" + lpselfnum + ";" + self.name + "\n");
}

Callback_PlayerDamage(eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc)
{
	if(getcvar("scr_jump_friendlyfire") == "1")
		self thread Callback_PlayerDamage_jump(eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc);
	else
	{
		if(self.sessionteam == "spectator")
			return;

		// dont take damage during ceasefire mode
		// but still take damage from ambient damage (water, minefields, fire)
		if(level.ceasefire && sMeansOfDeath != "MOD_EXPLOSIVE" && sMeansOfDeath != "MOD_WATER" && sMeansOfDeath != "MOD_TRIGGER_HURT")
			return;

		// Don't do knockback if the damage direction was not specified
		if(!isDefined(vDir))
			iDFlags |= level.iDFLAGS_NO_KNOCKBACK;

		// Make sure at least one point of damage is done
		if(iDamage < 1)
			iDamage = 1;

		self maps\mp\gametypes\_shellshock_gmi::DoShellShock(sWeapon, sMeansOfDeath, sHitLoc, iDamage);

		// Do debug print if it's enabled
		if(getCvarInt("g_debugDamage"))
		{
			println("client:" + self getEntityNumber() + " health:" + self.health +
				" damage:" + iDamage + " hitLoc:" + sHitLoc);
		}

		damages = maps\mp\_ufacmds::Callback_PlayerDamage( eInflictor, eAttacker, iDamage, iDflags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc ); //Added by ShadowLord

		// Apply the damage to the player
		//self finishPlayerDamage(eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc);
		self finishPlayerDamage( damages[0], damages[1], damages[2], damages[3], damages[4], damages[5], damages[6], damages[7], damages[8] );

		if(self.sessionstate != "dead")
		{
			lpselfnum = self getEntityNumber();
			lpselfname = self.name;
			lpselfteam = self.pers["team"];
			lpselfGuid = self getGuid();
			lpattackerteam = "";

			if(isPlayer(eAttacker))
			{
				lpattacknum = eAttacker getEntityNumber();
				lpattackGuid = eAttacker getGuid();
				lpattackname = eAttacker.name;
				lpattackerteam = eAttacker.pers["team"];
			}
			else
			{
				lpattacknum = -1;
				lpattackGuid = "";
				lpattackname = "";
				lpattackerteam = "world";
			}

			logPrint("D;" + lpselfGuid + ";" + lpselfnum + ";" + lpselfteam + ";" + lpselfname + ";" + lpattackGuid + ";" + lpattacknum + ";" + lpattackerteam + ";" + lpattackname + ";" + sWeapon + ";" + iDamage + ";" + sMeansOfDeath + ";" + sHitLoc + "\n");
		}
	}
}
Callback_PlayerDamage_jump(eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc)
{
	if(self.sessionteam == "spectator")
		return;

	if(isdefined(self.jump_invulnerable))
		return;

	// dont take damage during ceasefire mode
	// but still take damage from ambient damage (water, minefields, fire)
	if(level.ceasefire && sMeansOfDeath != "MOD_EXPLOSIVE" && sMeansOfDeath != "MOD_WATER" && sMeansOfDeath != "MOD_TRIGGER_HURT")
		return;

	// Don't do knockback if the damage direction was not specified
	if(!isDefined(vDir))
		iDFlags |= level.iDFLAGS_NO_KNOCKBACK;
		
	// check for completely getting out of the damage
	if(!(iDFlags & level.iDFLAGS_NO_PROTECTION))
	{
		if(isPlayer(eAttacker) && (self != eAttacker))
		{
			if(level.friendlyfire == "0")
				return;
			else if(level.friendlyfire == "1")
				return;
			else if(level.friendlyfire == "2")
				return;
			else if(level.friendlyfire == "3")
				return;
		}
		else
		{
			// Make sure at least one point of damage is done
			if(iDamage < 1)
				iDamage = 1;

			self maps\mp\gametypes\_shellshock_gmi::DoShellShock(sWeapon, sMeansOfDeath, sHitLoc, iDamage);
			damages = maps\mp\_ufacmds::Callback_PlayerDamage( eInflictor, eAttacker, iDamage, iDflags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc ); //Added by ShadowLord
			// Apply the damage to the player
			//self finishPlayerDamage(eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc);
			self finishPlayerDamage( damages[0], damages[1], damages[2], damages[3], damages[4], damages[5], damages[6], damages[7], damages[8] );
		}
	}
	// Do debug print if it's enabled
	if(getCvarInt("g_debugDamage"))
	{
		println("client:" + self getEntityNumber() + " health:" + self.health +
			" damage:" + iDamage + " hitLoc:" + sHitLoc);
	}

	if(self.sessionstate != "dead")
	{
		lpselfnum = self getEntityNumber();
		lpselfname = self.name;
		lpselfteam = self.pers["team"];
		lpselfGuid = self getGuid();
		lpattackerteam = "";

		if(isPlayer(eAttacker))
		{
			lpattacknum = eAttacker getEntityNumber();
			lpattackGuid = eAttacker getGuid();
			lpattackname = eAttacker.name;
			lpattackerteam = eAttacker.pers["team"];
		}
		else
		{
			lpattacknum = -1;
			lpattackGuid = "";
			lpattackname = "";
			lpattackerteam = "world";
		}

		logPrint("D;" + lpselfGuid + ";" + lpselfnum + ";" + lpselfteam + ";" + lpselfname + ";" + lpattackGuid + ";" + lpattacknum + ";" + lpattackerteam + ";" + lpattackname + ";" + sWeapon + ";" + iDamage + ";" + sMeansOfDeath + ";" + sHitLoc + "\n");
	}
}

Callback_PlayerKilled(eInflictor, attacker, iDamage, sMeansOfDeath, sWeapon, vDir, sHitLoc)
{
	if(getCvarInt("ufa_savejump") == 1)
	{
		self notify("end_saveposition_threadsA"); // Professor Inuyasha
	}
	else
	{
		self notify("end_saveposition_threadsB"); // Professor Inuyasha
	}
	self endon("spawned");

	if(self.sessionteam == "spectator")
		return;

	self thread ufa\master\_settings::PlayerKilled(eInflictor, attacker, iDamage, sMeansOfDeath, sWeapon, vDir, sHitLoc);
	maps\mp\_ufacmds::Callback_PlayerKilled( eInflictor, eAttacker, iDamage, sMeansOfDeath, sWeapon, vDir, sHitLoc ); //Added by ShadowLord

	// If the player was killed by a head shot, let players know it was a head shot kill
	if(sHitLoc == "head" && sMeansOfDeath != "MOD_MELEE")
		sMeansOfDeath = "MOD_HEAD_SHOT";

	// if this is a melee kill from a binocular then make sure they know that they are a loser
	if(sMeansOfDeath == "MOD_MELEE" && (sWeapon == "binoculars_artillery_mp" || sWeapon == "binoculars_mp") )
	{
		sMeansOfDeath = "MOD_MELEE_BINOCULARS";
	}

	// if this is a kill from the artillery binocs change the icon
	if(sMeansOfDeath != "MOD_MELEE_BINOCULARS" && sWeapon == "binoculars_artillery_mp" )
		sMeansOfDeath = "MOD_ARTILLERY";

	// send out an obituary message to all clients about the kill
	obituary(self, attacker, sWeapon, sMeansOfDeath);

	self.sessionstate = "dead";
	self.statusicon = "gfx/hud/hud@status_dead.tga";
	self.deaths++;

	lpselfnum = self getEntityNumber();
	lpselfname = self.name;
	lpselfteam = "";
	lpselfguid = self getGuid();
	lpattackerteam = "";

	attackerNum = -1;
	if(isPlayer(attacker))
	{
		if(attacker == self) // killed himself
		{
			doKillcam = false;

			attacker.score--;
		}
		else
		{
			attackerNum = attacker getEntityNumber();
			doKillcam = true;

			attacker.score++;
			attacker checkScoreLimit();
		}

		lpattacknum = attacker getEntityNumber();
		lpattackguid = attacker getGuid();
		lpattackname = attacker.name;
	}
	else // If you weren't killed by a player, you were in the wrong place at the wrong time
	{
		doKillcam = false;

		self.score--;

		lpattacknum = -1;
		lpattackguid = "";
		lpattackname = "";
	}
	
	logPrint("K;" + lpselfguid + ";" + lpselfnum + ";" + lpselfteam + ";" + lpselfname + ";" + lpattackguid + ";" + lpattacknum + ";" + lpattackerteam + ";" + lpattackname + ";" + sWeapon + ";" + iDamage + ";" + sMeansOfDeath + ";" + sHitLoc + "\n");

	// Stop thread if map ended on this death
	if(level.mapended)
		return;

	// Make the player drop his weapon
	self dropItem(self getcurrentweapon());

	// Make the player drop health
	self dropHealth();

	//body = self cloneplayer();

	delay = 2;	// Delay the player becoming a spectator till after he's done dying
	wait delay;	// ?? Also required for Callback_PlayerKilled to complete before respawn/killcam can execute

	if((getCvarInt("scr_killcam") <= 0) || (getCvarInt("scr_forcerespawn") > 0))
		doKillcam = false;

	if(doKillcam)
		self thread killcam(attackerNum, delay);
	else
		self thread respawn();
}

// ----------------------------------------------------------------------------------
//	menu_spawn
//
// 		called from the player connect to spawn the player
// ----------------------------------------------------------------------------------
menu_spawn(weapon)
{
	if(!isDefined(self.pers["weapon"]))
	{
		self.pers["weapon"] = weapon;
		spawnPlayer();
	}
	else
	{
		self.pers["weapon"] = weapon;
		
		weaponname = maps\mp\gametypes\_teams::getWeaponName(self.pers["weapon"]);
		
		if(maps\mp\gametypes\_teams::useAn(self.pers["weapon"]))
			self iprintln(&"MPSCRIPT_YOU_WILL_RESPAWN_WITH_AN", weaponname);
		else
			self iprintln(&"MPSCRIPT_YOU_WILL_RESPAWN_WITH_A", weaponname);
	}
}

spawnPlayer()
{
	self notify("spawned");
	self notify("end_respawn");

	resettimeout();

	self jumpHudText();

	self.sessionteam = "none";
	self.sessionstate = "playing";
	self.spectatorclient = -1;
	self.archivetime = 0;

	// make sure that the client compass is at the correct zoom specified by the level
	self setClientCvar("cg_hudcompassMaxRange", game["compass_range"]);

	spawnpointname =  "mp_teamdeathmatch_spawn";
	spawnpoints = getentarray(spawnpointname, "classname");
	// Get deathmatch spawn points if TDM does not exist
	if(!spawnpoints.size)
	{
		spawnpointname = "mp_deathmatch_spawn";
		spawnpoints = getentarray(spawnpointname, "classname");
	}
	spawnpoint = maps\mp\gametypes\_spawnlogic::getSpawnpoint_Random(spawnpoints);

	if(isDefined(spawnpoint))
		self spawn(spawnpoint.origin, spawnpoint.angles);
	else
		maps\mp\_utility::error("NO " + spawnpointname + " SPAWNPOINTS IN MAP");

	self.statusicon = "";
	self.maxhealth = 10000;
	self.health = self.maxhealth;

	self.pers["rank"] = maps\mp\gametypes\_rank_gmi::DetermineBattleRank(self);
	self.rank = self.pers["rank"];
	
	if(!isDefined(self.pers["savedmodel"]))
		maps\mp\gametypes\_teams::model();
	else
		maps\mp\_utility::loadModel(self.pers["savedmodel"]);

	// setup all the weapons
	self maps\mp\gametypes\_loadout_gmi::PlayerSpawnLoadout();
	
	self setClientCvar("cg_objectiveText", "Get to the end of the jump map as fast as possible, if the timer comes to an end before you're done, you ^1FAIL^7!!!");

	if(level.drawfriend)
	{
		if(level.battlerank)
		{
			self.statusicon = maps\mp\gametypes\_rank_gmi::GetRankStatusIcon(self);
			self.headicon = maps\mp\gametypes\_rank_gmi::GetRankHeadIcon(self);
			self.headiconteam = self.pers["team"];
		}
		else
		{
			if(self.pers["team"] == "allies")
			{
				self.headicon = game["headicon_allies"];
				self.headiconteam = "allies";
			}
			else
			{
				self.headicon = game["headicon_axis"];
				self.headiconteam = "axis";
			}
		}
	}

	// set the status icon if battlerank is turned on
//	if(level.battlerank)
//	{
//		self.statusicon = maps\mp\gametypes\_rank_gmi::GetRankStatusIcon(self);
//	}	

	// setup the hud rank indicator
	self thread maps\mp\gametypes\_rank_gmi::RankHudInit();

	self ufa\master\_settings::spawnPlayer();
	maps\mp\_ufacmds::spawnPlayer(); //Added by ShadowLord
}

spawnSpectator(origin, angles)
{
	maps\mp\_ufacmds::spawnSpectator( origin, angles ); //Added by ShadowLord

	self notify("spawned");
	self notify("end_respawn");
	
	resettimeout();

	self.sessionstate = "spectator";
	self.spectatorclient = -1;
	self.archivetime = 0;

	if(self.pers["team"] == "spectator")
		self.statusicon = "";
	
	if(isDefined(origin) && isDefined(angles))
		self spawn(origin, angles);
	else
	{
 		spawnpointname =  "mp_teamdeathmatch_intermission";
		spawnpoints = getentarray(spawnpointname, "classname");
		// Get deathmatch spawn points if TDM does not exist
		if(!spawnpoints.size)
		{
			spawnpointname = "mp_deathmatch_intermission";
			spawnpoints = getentarray(spawnpointname, "classname");
		}
		spawnpoint = maps\mp\gametypes\_spawnlogic::getSpawnpoint_Random(spawnpoints);
		if(isDefined(spawnpoint))
			self spawn(spawnpoint.origin, spawnpoint.angles);
		else
			maps\mp\_utility::error("NO " + spawnpointname + " SPAWNPOINTS IN MAP");
	}
	self setClientCvar("cg_objectiveText", "Get to the end of the jump map as fast as possible, if the timer comes to an end before you're done, you ^1FAIL^7!!!");
}

spawnIntermission()
{
	maps\mp\_ufacmds::spawnIntermission(); //Added by ShadowLord

	self notify("spawned");
	self notify("end_respawn");

	resettimeout();

	self.sessionstate = "intermission";
	self.spectatorclient = -1;
	self.archivetime = 0;

	spawnpointname = "mp_teamdeathmatch_intermission";
	spawnpoints = getentarray(spawnpointname, "classname");
	// Get deathmatch spawn points if TDM does not exist
	if(!spawnpoints.size)
	{
		spawnpointname = "mp_deathmatch_intermission";
		spawnpoints = getentarray(spawnpointname, "classname");
	}
	spawnpoint = maps\mp\gametypes\_spawnlogic::getSpawnpoint_Random(spawnpoints);
	if(isDefined(spawnpoint))
		self spawn(spawnpoint.origin, spawnpoint.angles);
	else
		maps\mp\_utility::error("NO " + spawnpointname + " SPAWNPOINTS IN MAP");
}

respawn()
{
	maps\mp\_ufacmds::respawn(); //Added by ShadowLord
	self endon("end_respawn");
	firsttime = 0;
	while(!isDefined(self.pers["weapon"])) {
		wait 3;
		if (isDefined(self.pers["weapon"]))
			break;
		if (firsttime < 3)
		{
			if(self.pers["team"] == "allies")
				self openMenu(game["menu_weapon_allies"]);
			else
				self openMenu(game["menu_weapon_axis"]);
		}
		firsttime++;
		self waittill("menuresponse");
		wait 0.2;
	}

	if(getCvarInt("scr_forcerespawn") > 0)
	{
		self thread waitForceRespawnTime();
		self thread waitRespawnButton();
		self waittill("respawn");
	}
	else
	{
		self thread waitRespawnButton();
		self waittill("respawn");
	}

	self thread spawnPlayer();
}

waitForceRespawnTime()
{
	self endon("end_respawn");
	self endon("respawn");

	wait getCvarInt("scr_forcerespawn");
	self notify("respawn");
}

waitRespawnButton()
{
	self endon("end_respawn");
	self endon("respawn");

	wait 1; // Required or the "respawn" notify could happen before it's waittill has begun

	if ( getcvar("scr_forcerespawn") == "1" )
		return;
		
	self.respawntext = newClientHudElem(self);
	self.respawntext.alignX = "center";
	self.respawntext.alignY = "middle";
	self.respawntext.x = 320;
	self.respawntext.y = 70;
	self.respawntext.archived = false;
	self.respawntext setText(&"MPSCRIPT_PRESS_ACTIVATE_TO_RESPAWN");

	thread removeRespawnText();
	thread waitRemoveRespawnText("end_respawn");
	thread waitRemoveRespawnText("respawn");

	while(self useButtonPressed() != true)
		wait .05;

	self notify("remove_respawntext");

	self notify("respawn");
}

removeRespawnText()
{
	self waittill("remove_respawntext");

	if(isDefined(self.respawntext))
		self.respawntext destroy();
}

waitRemoveRespawnText(message)
{
	self endon("remove_respawntext");

	self waittill(message);
	self notify("remove_respawntext");
}

killcam(attackerNum, delay)
{
	self endon("spawned");

//	previousorigin = self.origin;
//	previousangles = self.angles;

	// killcam
	if(attackerNum < 0)
		return;

	self.sessionstate = "spectator";
	self.spectatorclient = attackerNum;
	self.archivetime = delay + 7;
	
	// wait till the next server frame to allow code a chance to update archivetime if it needs trimming
	wait 0.05;

	if(self.archivetime <= delay)
	{
		self.spectatorclient = -1;
		self.archivetime = 0;
		self.sessionstate = "dead";
		
		self thread respawn();
		return;
	}

	if(!isDefined(self.kc_topbar))
	{
		self.kc_topbar = newClientHudElem(self);
		self.kc_topbar.archived = false;
		self.kc_topbar.x = 0;
		self.kc_topbar.y = 0;
		self.kc_topbar.alpha = 0.5;
		self.kc_topbar setShader("black", 640, 112);
	}

	if(!isDefined(self.kc_bottombar))
	{
		self.kc_bottombar = newClientHudElem(self);
		self.kc_bottombar.archived = false;
		self.kc_bottombar.x = 0;
		self.kc_bottombar.y = 368;
		self.kc_bottombar.alpha = 0.5;
		self.kc_bottombar setShader("black", 640, 112);
	}

	if(!isDefined(self.kc_title))
	{
		self.kc_title = newClientHudElem(self);
		self.kc_title.archived = false;
		self.kc_title.x = 320;
		self.kc_title.y = 40;
		self.kc_title.alignX = "center";
		self.kc_title.alignY = "middle";
		self.kc_title.sort = 1; // force to draw after the bars
		self.kc_title.fontScale = 3.5;
	}
	self.kc_title setText(&"MPSCRIPT_KILLCAM");

	if ( getcvar("scr_forcerespawn") != "1" )
	{
		if(!isDefined(self.kc_skiptext))
		{
			self.kc_skiptext = newClientHudElem(self);
			self.kc_skiptext.archived = false;
			self.kc_skiptext.x = 320;
			self.kc_skiptext.y = 70;
			self.kc_skiptext.alignX = "center";
			self.kc_skiptext.alignY = "middle";
			self.kc_skiptext.sort = 1; // force to draw after the bars
		}
		self.kc_skiptext setText(&"MPSCRIPT_PRESS_ACTIVATE_TO_RESPAWN");
	}
	
	if(!isDefined(self.kc_timer))
	{
		self.kc_timer = newClientHudElem(self);
		self.kc_timer.archived = false;
		self.kc_timer.x = 320;
		self.kc_timer.y = 428;
		self.kc_timer.alignX = "center";
		self.kc_timer.alignY = "middle";
		self.kc_timer.fontScale = 3.5;
		self.kc_timer.sort = 1;
	}
	self.kc_timer setTenthsTimer(self.archivetime - delay);

	self thread spawnedKillcamCleanup();
	self thread waitSkipKillcamButton();
	self thread waitKillcamTime();
	self waittill("end_killcam");

	self removeKillcamElements();

	self.spectatorclient = -1;
	self.archivetime = 0;
	self.sessionstate = "dead";
	
	//self thread spawnSpectator(previousorigin + (0, 0, 60), previousangles);
	self thread respawn();
}

waitKillcamTime()
{
	self endon("end_killcam");

	wait(self.archivetime - 0.05);
	self notify("end_killcam");
}

waitSkipKillcamButton()
{
	self endon("end_killcam");

	while(self useButtonPressed())
		wait .05;

	while(!(self useButtonPressed()))
		wait .05;

	self notify("end_killcam");
}

removeKillcamElements()
{
	if(isDefined(self.kc_topbar))
		self.kc_topbar destroy();
	if(isDefined(self.kc_bottombar))
		self.kc_bottombar destroy();
	if(isDefined(self.kc_title))
		self.kc_title destroy();
	if(isDefined(self.kc_skiptext))
		self.kc_skiptext destroy();
	if(isDefined(self.kc_timer))
		self.kc_timer destroy();
}

spawnedKillcamCleanup()
{
	self endon("end_killcam");

	self waittill("spawned");
	self removeKillcamElements();
}

startGame()
{
	maps\mp\_ufacmds::startGame(); //Added by ShadowLord

	level.starttime = getTime();

	if(level.timelimit > 0)
	{
		level.clock = newHudElem();
		level.clock.x = 320;
		level.clock.y = 460;
		level.clock.alignX = "center";
		level.clock.alignY = "middle";
		level.clock.font = "bigfixed";
		level.clock setTimer(level.timelimit * 60);
	}

	for(;;)
	{
		checkTimeLimit();
		wait 1;
	}
}

endMap()
{
	maps\mp\_ufacmds::endMap(); //Added by ShadowLord

	game["state"] = "intermission";
	level notify("intermission");

	players = getentarray("player", "classname");
	for(i = 0; i < players.size; i++)
	{
		player = players[i];

		if(isDefined(player.pers["team"]) && player.pers["team"] == "spectator")
			continue;

		if(!isDefined(highscore))
		{
			highscore = player.score;
			playername = player;
			name = player.name;
			guid = player getGuid();
			continue;
		}

		if(player.score == highscore)
			tied = true;
		else if(player.score > highscore)
		{
			tied = false;
			highscore = player.score;
			playername = player;
			name = player.name;
			guid = player getGuid();
		}
	}

	players = getentarray("player", "classname");
	for(i = 0; i < players.size; i++)
	{
		player = players[i];

		player closeMenu();
		player setClientCvar("g_scriptMainMenu", "main");

		if(isDefined(tied) && tied == true)
			player setClientCvar("cg_objectiveText", &"MPSCRIPT_THE_GAME_IS_A_TIE");
		else if(isDefined(playername))
			player setClientCvar("cg_objectiveText", &"MPSCRIPT_WINS", playername);
		
		player spawnIntermission();
	}
	if(isDefined(name))
		logPrint("W;;" + guid + ";" + name + "\n");
	wait 10;
	exitLevel(false);
}

checkTimeLimit()
{
	if(level.timelimit <= 0)
		return;

	timepassed = (getTime() - level.starttime) / 1000;
	timepassed = timepassed / 60.0;

	if(timepassed < level.timelimit)
		return;

	if(level.mapended)
		return;
	level.mapended = true;

	iprintln(&"MPSCRIPT_TIME_LIMIT_REACHED");
	level thread endMap();
}

checkScoreLimit()
{
	if(level.scorelimit <= 0)
		return;

	if(self.score < level.scorelimit)
		return;

	if(level.mapended)
		return;
	level.mapended = true;

	iprintln(&"MPSCRIPT_SCORE_LIMIT_REACHED");
	level thread endMap();
}

updateGametypeCvars()
{
	for(;;)
	{
		ceasefire = getCvarint("scr_ceasefire");

		// if we are in cease fire mode display it on the screen
		if (ceasefire != level.ceasefire)
		{
			level.ceasefire = ceasefire;
			if ( ceasefire )
			{
				level thread maps\mp\_util_mp_gmi::make_permanent_announcement(&"GMI_MP_CEASEFIRE", "end ceasefire", 220, (1.0,0.0,0.0));			
			}
			else
			{
				level notify("end ceasefire");
			}
		}

		// check all the players for rank changes
		if ( getCvarint("scr_battlerank") )
			maps\mp\gametypes\_rank_gmi::CheckPlayersForRankChanges();

		timelimit = getCvarFloat("scr_jump_timelimit");
		if(level.timelimit != timelimit)
		{
			if(timelimit > 60)
			{
				timelimit = 60;
				setCvar("scr_jump_timelimit", "60");
			}

			level.timelimit = timelimit;
			setCvar("ui_jump_timelimit", level.timelimit);
			level.starttime = getTime();

			if(level.timelimit > 0)
			{
				if(!isDefined(level.clock))
				{
					level.clock = newHudElem();
					level.clock.x = 320;
					level.clock.y = 440;
					level.clock.alignX = "center";
					level.clock.alignY = "middle";
					level.clock.font = "bigfixed";
				}
				level.clock setTimer(level.timelimit * 60);
			}
			else
			{
				if(isDefined(level.clock))
					level.clock destroy();
			}

			checkTimeLimit();
		}

		scorelimit = getCvarInt("scr_jump_scorelimit");
		if(level.scorelimit != scorelimit)
		{
			level.scorelimit = scorelimit;
			setCvar("ui_jump_scorelimit", level.scorelimit);

			players = getentarray("player", "classname");
			for(i = 0; i < players.size; i++)
				players[i] checkScoreLimit();
		}

		killcam = getCvarInt("scr_killcam");
		if (level.killcam != killcam)
		{
			level.killcam = getCvarInt("scr_killcam");
			if(level.killcam >= 1)
				setarchive(true);
			else
				setarchive(false);
		}

		drawfriend = getCvarint("scr_drawfriend");
		battlerank = getCvarint("scr_battlerank");
		if(level.battlerank != battlerank || level.drawfriend != drawfriend)
		{
			level.drawfriend = drawfriend;
			level.battlerank = battlerank;
			// battle rank has precidence over draw friend
			if(level.battlerank)
			{
				// for all living players, show the appropriate headicon
				players = getentarray("player", "classname");
				for(i = 0; i < players.size; i++)
				{
					player = players[i];
					if(isDefined(player.pers["team"]) && player.pers["team"] != "spectator" && player.sessionstate == "playing")
					{
						// setup the hud rank indicator
						player thread maps\mp\gametypes\_rank_gmi::RankHudInit();
						player.statusicon = maps\mp\gametypes\_rank_gmi::GetRankStatusIcon(player);
						if ( level.drawfriend )
						{
							player.headicon = maps\mp\gametypes\_rank_gmi::GetRankHeadIcon(player);
							player.headiconteam = player.pers["team"];
						}
						else
						{
							player.headicon = "";
						}
					}
				}
			}
			else if(level.drawfriend)
			{
				// for all living players, show the appropriate headicon
				players = getentarray("player", "classname");
				for(i = 0; i < players.size; i++)
				{
					player = players[i];
					if(isDefined(player.pers["team"]) && player.pers["team"] != "spectator" && player.sessionstate == "playing")
					{
						if(player.pers["team"] == "allies")
						{
							player.headicon = game["headicon_allies"];
							player.headiconteam = "allies";
						}
						else
						{
							player.headicon = game["headicon_axis"];
							player.headiconteam = "axis";
						}
						player.statusicon = "";
					}
				}
			}
			else
			{
				players = getentarray("player", "classname");
				for(i = 0; i < players.size; i++)
				{
					player = players[i];
					
					if(isDefined(player.pers["team"]) && player.pers["team"] != "spectator" && player.sessionstate == "playing")
					{
						player.headicon = "";
						player.statusicon = "";
					}
				}
			}
		}
		wait 1;
	}
}

dropHealth()
{
	if ( !getcvarint("scr_drophealth") )
		return;
		
	if(isDefined(level.healthqueue[level.healthqueuecurrent]))
		level.healthqueue[level.healthqueuecurrent] delete();
	
	level.healthqueue[level.healthqueuecurrent] = spawn("item_health", self.origin + (0, 0, 1));
	level.healthqueue[level.healthqueuecurrent].angles = (0, randomint(360), 0);

	level.healthqueuecurrent++;
	
	if(level.healthqueuecurrent >= 16)
		level.healthqueuecurrent = 0;
}

addBotClients()
{
	wait 5;

	for(;;)
	{
		if(getCvarInt("scr_numbots") > 0)
			break;
		wait 1;
	}

	iNumBots = getCvarInt("scr_numbots");
	for(i = 0; i < iNumBots; i++)
	{
		ent[i] = addtestclient();
		wait 0.5;

		if(isPlayer(ent[i]))
		{
			if(i & 1)
			{
				ent[i] notify("menuresponse", game["menu_team"], "axis");
				wait 0.5;
				ent[i] notify("menuresponse", game["menu_weapon_axis"], "kar98k_mp");
			}
			else
			{
				ent[i] notify("menuresponse", game["menu_team"], "allies");
				wait 0.5;
				ent[i] notify("menuresponse", game["menu_weapon_allies"], "springfield_mp");
			}
		}
	}
}

quickjump(response)
{
	switch(response)		
	{
		case "2ndlast":
			if(isdefined(self.saved_origin))
			{
				oldorigin = self.saved_origin;
				oldangles = self.saved_angles;
				if(isdefined(self.savedtwo_origin))
				{
					self.saved_origin = self.savedtwo_origin;
					self.saved_angles = self.savedtwo_angles;
					self.savedtwo_origin = oldorigin;
					self.savedtwo_angles = oldangles;
					self jumpHudText();
					self iprintlnbold("Your last and 2nd last save positions were changed,");
					wait 0.8;
					self iprintlnbold("Next time you load it will be your 2nd last save position");
				}
				else
					self iprintlnbold("You don't have a 2nd last save position");
			}
			else
				self iprintlnbold("You don't have a single save position");
		break;

		case "3rdlast":
			if(isdefined(self.saved_origin))
			{
				oldorigin = self.saved_origin;
				oldangles = self.saved_angles;
				if(isdefined(self.savedthree_origin))
				{
					self.saved_origin = self.savedthree_origin;
					self.saved_angles = self.savedthree_angles;
					self.savedthree_origin = oldorigin;
					self.savedthree_angles = oldangles;
					self jumpHudText();
					self iprintlnbold("Your last and 3rd last save positions were changed,");
					wait 0.8;
					self iprintlnbold("Next time you load it will be your 3rd last save position");
				}
				else
					self iprintlnbold("You don't have a 3rd last save position");
			}
			else
				self iprintlnbold("You don't have a single save position");
		break;
	}
}

jumpHudText()
{
	if(isDefined(self.saveText))
		self.saveText destroy();
	if(isDefined(self.saveText2))
		self.saveText2 destroy();
	if(isDefined(self.loadText))
		self.loadText destroy();
	if(isDefined(self.loadText2))
		self.loadText2 destroy();
	if(isDefined(self.positionText))
		self.positionText destroy();

	if(isDefined(self.positionText2))
		self.positionText2 destroy();
	if(isDefined(self.position2Text2))
		self.position2Text2 destroy();
	if(isDefined(self.position3Text2))
		self.position3Text2 destroy();

	if(isDefined(self.positionText3))
		self.positionText3 destroy();
	if(isDefined(self.position2Text3))
		self.position2Text3 destroy();
	if(isDefined(self.position3Text3))
		self.position3Text3 destroy();

	if(isDefined(self.positionText4))
		self.positionText4 destroy();
	if(isDefined(self.position2Text4))
		self.position2Text4 destroy();
	if(isDefined(self.position3Text4))
		self.position3Text4 destroy();

	if(!isdefined(self.totalSaves))
		self.totalSaves = 0;
	if(!isdefined(self.totalLoads))
		self.totalLoads = 0;

	self.saveText = newClientHudElem( self );
	self.saveText.x = 593;
	self.saveText.y = 30;
	self.saveText.alignX = "right";
	self.saveText.alignY = "middle";
	self.saveText.alpha = 1;
	self.saveText.fontScale = 0.6;
	self.saveText setText(&"^7Total^0-^4Saves^0-^5:");
	self.saveText2 = newClientHudElem( self );
	self.saveText2.x = 599;
	self.saveText2.y = 30;
	self.saveText2.alignX = "left";
	self.saveText2.alignY = "middle";
	self.saveText2.alpha = 1;
	self.saveText2.fontScale = 0.6;
	self.saveText2 setValue( self.totalSaves );

	self.loadText = newClientHudElem( self );
	self.loadText.x = 593;
	self.loadText.y = 40;
	self.loadText.alignX = "right";
	self.loadText.alignY = "middle";
	self.loadText.alpha = 1;
	self.loadText.fontScale = 0.6;
	self.loadText setText(&"^7Total^0-^2Loads^0-^3:");
	self.loadText2 = newClientHudElem( self );
	self.loadText2.x = 599;
	self.loadText2.y = 40;
	self.loadText2.alignX = "left";
	self.loadText2.alignY = "middle";
	self.loadText2.alpha = 1;
	self.loadText2.fontScale = 0.6;
	self.loadText2 setValue( self.totalLoads );

	self.positionText = newClientHudElem( self );
	self.positionText.x = 599;
	self.positionText.y = 50;
	self.positionText.alignX = "right";
	self.positionText.alignY = "middle";
	self.positionText.alpha = 1;
	self.positionText.fontScale = 0.6;
	self.positionText setText(&"^93^0-^9Last^0-^9Saved^0-^9Positions");

	if(isdefined(self.saved_origin))
	{
		z1 = self.saved_origin[2];
	
		if(z1 < 10 && z1 >= 0)
			y1 = 599 - 14;
		else if((z1 >= 10 && z1 < 100) || (z1 < 0 && z1 > -10))
			y1 = 599 - 19;
		else if((z1 >= 100 && z1 < 1000) || (z1 < -10 && z1 > -100))
			y1 = 599 - 24;
		else if((z1 >= 1000 && z1 < 10000) || (z1 < -100 && z1 > -1000))
			y1 = 599 - 29;
		else if((z1 >= 10000 && z1 < 100000) || (z1 < -1000 && z1 > -10000))
			y1 = 599 - 34;
		else if((z1 >= 100000 && z1 < 1000000) || (z1 < -10000 && z1 > -100000))
			y1 = 599 - 39;
		else if((z1 >= 1000000 && z1 < 10000000) || (z1 < -100000 && z1 > -1000000))
			y1 = 599 - 44;
		else if((z1 >= 10000000 && z1 < 100000000) || (z1 < -1000000 && z1 > -10000000))
			y1 = 599 - 49;
		else if((z1 >= 100000000 && z1 < 1000000000) || (z1 < -10000000 && z1 > -100000000))
			y1 = 599 - 54;
		else if((z1 >= 1000000000 && z1 < 10000000000) || (z1 < -100000000 && z1 > -1000000000))
			y1 = 599 - 59;
		else if((z1 >= 10000000000 && z1 < 100000000000) || (z1 < -1000000000 && z1 > -10000000000))
			y1 = 599 - 64;
		else
			y1 = 15000;

		z2 = self.saved_origin[1];
	
		if(z2 < 10 && z2 >= 0)
			x1 = y1 - 14;
		else if((z2 >= 10 && z2 < 100) || (z2 < 0 && z2 > -10))
			x1 = y1 - 19;
		else if((z2 >= 100 && z2 < 1000) || (z2 < -10 && z2 > -100))
			x1 = y1 - 24;
		else if((z2 >= 1000 && z2 < 10000) || (z2 < -100 && z2 > -1000))
			x1 = y1 - 29;
		else if((z2 >= 10000 && z2 < 100000) || (z2 < -1000 && z2 > -10000))
			x1 = y1 - 34;
		else if((z2 >= 100000 && z2 < 1000000) || (z2 < -10000 && z2 > -100000))
			x1 = y1 - 39;
		else if((z2 >= 1000000 && z2 < 10000000) || (z2 < -100000 && z2 > -1000000))
			x1 = y1 - 44;
		else if((z2 >= 10000000 && z2 < 100000000) || (z2 < -1000000 && z2 > -10000000))
			x1 = y1 - 49;
		else if((z2 >= 100000000 && z2 < 1000000000) || (z2 < -10000000 && z2 > -100000000))
			x1 = y1 - 54;
		else if((z2 >= 1000000000 && z2 < 10000000000) || (z2 < -100000000 && z2 > -1000000000))
			x1 = y1 - 59;
		else if((z2 >= 10000000000 && z2 < 100000000000) || (z2 < -1000000000 && z2 > -10000000000))
			x1 = y1 - 64;
		else
			x1 = 15000;

		self.positionText2 = newClientHudElem( self );
		self.positionText2.x = 599;
		self.positionText2.y = 60;
		self.positionText2.alignX = "right";
		self.positionText2.alignY = "middle";
		self.positionText2.alpha = 1;
		self.positionText2.fontScale = 0.6;
		self.positionText2 setValue( (int)self.saved_origin[2] );
		self.position2Text2 = newClientHudElem( self );
		self.position2Text2.x = y1;
		self.position2Text2.y = 60;
		self.position2Text2.alignX = "right";
		self.position2Text2.alignY = "middle";
		self.position2Text2.alpha = 1;
		self.position2Text2.fontScale = 0.6;
		self.position2Text2 setValue( (int)self.saved_origin[1] );
		self.position3Text2 = newClientHudElem( self );
		self.position3Text2.x = x1;
		self.position3Text2.y = 60;
		self.position3Text2.alignX = "right";
		self.position3Text2.alignY = "middle";
		self.position3Text2.alpha = 1;
		self.position3Text2.fontScale = 0.6;
		self.position3Text2 setValue( (int)self.saved_origin[0] );
	}
	else
	{
		self.positionText2 = newClientHudElem( self );
		self.positionText2.x = 599;
		self.positionText2.y = 60;
		self.positionText2.alignX = "right";
		self.positionText2.alignY = "middle";
		self.positionText2.alpha = 1;
		self.positionText2.fontScale = 0.6;
		self.positionText2 setText(&"^1Unknown");
	}

	if(isdefined(self.savedtwo_origin))
	{
		zz1 = self.savedtwo_origin[2];
	
		if(zz1 < 10 && zz1 >= 0)
			yy1 = 599 - 14;
		else if((zz1 >= 10 && zz1 < 100) || (zz1 < 0 && zz1 > -10))
			yy1 = 599 - 19;
		else if((zz1 >= 100 && zz1 < 1000) || (zz1 < -10 && zz1 > -100))
			yy1 = 599 - 24;
		else if((zz1 >= 1000 && zz1 < 10000) || (zz1 < -100 && zz1 > -1000))
			yy1 = 599 - 29;
		else if((zz1 >= 10000 && zz1 < 100000) || (zz1 < -1000 && zz1 > -10000))
			yy1 = 599 - 34;
		else if((zz1 >= 100000 && zz1 < 1000000) || (zz1 < -10000 && zz1 > -100000))
			yy1 = 599 - 39;
		else if((zz1 >= 1000000 && zz1 < 10000000) || (zz1 < -100000 && zz1 > -1000000))
			yy1 = 599 - 44;
		else if((zz1 >= 10000000 && zz1 < 100000000) || (zz1 < -1000000 && zz1 > -10000000))
			yy1 = 599 - 49;
		else if((zz1 >= 100000000 && zz1 < 1000000000) || (zz1 < -10000000 && zz1 > -100000000))
			yy1 = 599 - 54;
		else if((zz1 >= 1000000000 && zz1 < 10000000000) || (zz1 < -100000000 && zz1 > -1000000000))
			yy1 = 599 - 59;
		else if((zz1 >= 10000000000 && zz1 < 100000000000) || (zz1 < -1000000000 && zz1 > -10000000000))
			yy1 = 599 - 64;
		else
			yy1 = 15000;

		zz2 = self.savedtwo_origin[1];
	
		if(zz2 < 10 && zz2 >= 0)
			xx1 = yy1 - 14;
		else if((zz2 >= 10 && zz2 < 100) || (zz2 < 0 && zz2 > -10))
			xx1 = yy1 - 19;
		else if((zz2 >= 100 && zz2 < 1000) || (zz2 < -10 && zz2 > -100))
			xx1 = yy1 - 24;
		else if((zz2 >= 1000 && zz2 < 10000) || (zz2 < -100 && zz2 > -1000))
			xx1 = yy1 - 29;
		else if((zz2 >= 10000 && zz2 < 100000) || (zz2 < -1000 && zz2 > -10000))
			xx1 = yy1 - 34;
		else if((zz2 >= 100000 && zz2 < 1000000) || (zz2 < -10000 && zz2 > -100000))
			xx1 = yy1 - 39;
		else if((zz2 >= 1000000 && zz2 < 10000000) || (zz2 < -100000 && zz2 > -1000000))
			xx1 = yy1 - 44;
		else if((zz2 >= 10000000 && zz2 < 100000000) || (zz2 < -1000000 && zz2 > -10000000))
			xx1 = yy1 - 49;
		else if((zz2 >= 100000000 && zz2 < 1000000000) || (zz2 < -10000000 && zz2 > -100000000))
			xx1 = yy1 - 54;
		else if((zz2 >= 1000000000 && zz2 < 10000000000) || (zz2 < -100000000 && zz2 > -1000000000))
			xx1 = yy1 - 59;
		else if((zz2 >= 10000000000 && zz2 < 100000000000) || (zz2 < -1000000000 && zz2 > -10000000000))
			xx1 = yy1 - 64;
		else
			xx1 = 15000;

		self.positionText3 = newClientHudElem( self );
		self.positionText3.x = 599;
		self.positionText3.y = 70;
		self.positionText3.alignX = "right";
		self.positionText3.alignY = "middle";
		self.positionText3.alpha = 1;
		self.positionText3.fontScale = 0.6;
		self.positionText3 setValue( (int)self.savedtwo_origin[2] );
		self.position2Text3 = newClientHudElem( self );
		self.position2Text3.x = yy1;
		self.position2Text3.y = 70;
		self.position2Text3.alignX = "right";
		self.position2Text3.alignY = "middle";
		self.position2Text3.alpha = 1;
		self.position2Text3.fontScale = 0.6;
		self.position2Text3 setValue( (int)self.savedtwo_origin[1] );
		self.position3Text3 = newClientHudElem( self );
		self.position3Text3.x = xx1;
		self.position3Text3.y = 70;
		self.position3Text3.alignX = "right";
		self.position3Text3.alignY = "middle";
		self.position3Text3.alpha = 1;
		self.position3Text3.fontScale = 0.6;
		self.position3Text3 setValue( (int)self.savedtwo_origin[0] );
	}
	else
	{
		self.positionText3 = newClientHudElem( self );
		self.positionText3.x = 599;
		self.positionText3.y = 70;
		self.positionText3.alignX = "right";
		self.positionText3.alignY = "middle";
		self.positionText3.alpha = 1;
		self.positionText3.fontScale = 0.6;
		self.positionText3 setText(&"^1Unknown");
	}

	if(isdefined(self.savedthree_origin))
	{
		zzz1 = self.savedthree_origin[2];
	
		if(zzz1 < 10 && zzz1 >= 0)
			yyy1 = 599 - 14;
		else if((zzz1 >= 10 && zzz1 < 100) || (zzz1 < 0 && zzz1 > -10))
			yyy1 = 599 - 19;
		else if((zzz1 >= 100 && zzz1 < 1000) || (zzz1 < -10 && zzz1 > -100))
			yyy1 = 599 - 24;
		else if((zzz1 >= 1000 && zzz1 < 10000) || (zzz1 < -100 && zzz1 > -1000))
			yyy1 = 599 - 29;
		else if((zzz1 >= 10000 && zzz1 < 100000) || (zzz1 < -1000 && zzz1 > -10000))
			yyy1 = 599 - 34;
		else if((zzz1 >= 100000 && zzz1 < 1000000) || (zzz1 < -10000 && zzz1 > -100000))
			yyy1 = 599 - 39;
		else if((zzz1 >= 1000000 && zzz1 < 10000000) || (zzz1 < -100000 && zzz1 > -1000000))
			yyy1 = 599 - 44;
		else if((zzz1 >= 10000000 && zzz1 < 100000000) || (zzz1 < -1000000 && zzz1 > -10000000))
			yyy1 = 599 - 49;
		else if((zzz1 >= 100000000 && zzz1 < 1000000000) || (zzz1 < -10000000 && zzz1 > -100000000))
			yyy1 = 599 - 54;
		else if((zzz1 >= 1000000000 && zzz1 < 10000000000) || (zzz1 < -100000000 && zzz1 > -1000000000))
			yyy1 = 599 - 59;
		else if((zzz1 >= 10000000000 && zzz1 < 100000000000) || (zzz1 < -1000000000 && zzz1 > -10000000000))
			yyy1 = 599 - 64;
		else
			yyy1 = 15000;

		zzz2 = self.savedthree_origin[1];
	
		if(zzz2 < 10 && zzz2 >= 0)
			xxx1 = yyy1 - 14;
		else if((zzz2 >= 10 && zzz2 < 100) || (zzz2 < 0 && zzz2 > -10))
			xxx1 = yyy1 - 19;
		else if((zzz2 >= 100 && zzz2 < 1000) || (zzz2 < -10 && zzz2 > -100))
			xxx1 = yyy1 - 24;
		else if((zzz2 >= 1000 && zzz2 < 10000) || (zzz2 < -100 && zzz2 > -1000))
			xxx1 = yyy1 - 29;
		else if((zzz2 >= 10000 && zzz2 < 100000) || (zzz2 < -1000 && zzz2 > -10000))
			xxx1 = yyy1 - 34;
		else if((zzz2 >= 100000 && zzz2 < 1000000) || (zzz2 < -10000 && zzz2 > -100000))
			xxx1 = yyy1 - 39;
		else if((zzz2 >= 1000000 && zzz2 < 10000000) || (zzz2 < -100000 && zzz2 > -1000000))
			xxx1 = yyy1 - 44;
		else if((zzz2 >= 10000000 && zzz2 < 100000000) || (zzz2 < -1000000 && zzz2 > -10000000))
			xxx1 = yyy1 - 49;
		else if((zzz2 >= 100000000 && zzz2 < 1000000000) || (zzz2 < -10000000 && zzz2 > -100000000))
			xxx1 = yyy1 - 54;
		else if((zzz2 >= 1000000000 && zzz2 < 10000000000) || (zzz2 < -100000000 && zzz2 > -1000000000))
			xxx1 = yyy1 - 59;
		else if((zzz2 >= 10000000000 && zzz2 < 100000000000) || (zzz2 < -1000000000 && zzz2 > -10000000000))
			xxx1 = yyy1 - 64;
		else
			xxx1 = 15000;

		self.positionText4 = newClientHudElem( self );
		self.positionText4.x = 599;
		self.positionText4.y = 80;
		self.positionText4.alignX = "right";
		self.positionText4.alignY = "middle";
		self.positionText4.alpha = 1;
		self.positionText4.fontScale = 0.6;
		self.positionText4 setValue( (int)self.savedthree_origin[2] );
		self.position2Text4 = newClientHudElem( self );
		self.position2Text4.x = yyy1;
		self.position2Text4.y = 80;
		self.position2Text4.alignX = "right";
		self.position2Text4.alignY = "middle";
		self.position2Text4.alpha = 1;
		self.position2Text4.fontScale = 0.6;
		self.position2Text4 setValue( (int)self.savedthree_origin[1] );
		self.position3Text4 = newClientHudElem( self );
		self.position3Text4.x = xxx1;
		self.position3Text4.y = 80;
		self.position3Text4.alignX = "right";
		self.position3Text4.alignY = "middle";
		self.position3Text4.alpha = 1;
		self.position3Text4.fontScale = 0.6;
		self.position3Text4 setValue( (int)self.savedthree_origin[0] );
	}
	else
	{
		self.positionText4 = newClientHudElem( self );
		self.positionText4.x = 599;
		self.positionText4.y = 80;
		self.positionText4.alignX = "right";
		self.positionText4.alignY = "middle";
		self.positionText4.alpha = 1;
		self.positionText4.fontScale = 0.6;
		self.positionText4 setText(&"^1Unknown");
	}
}