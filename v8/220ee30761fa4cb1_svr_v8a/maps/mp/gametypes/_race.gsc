raceMain()
{
	level._effect["powerup"] = loadFx( "fx/boux/powerup.efx" );
	level._effect["takepower"] = loadFx( "fx/boux/takepower.efx" );
	level._effect["race_fireball"] = loadFx( "fx/boux/race_fireball.efx" );
}

Callback_StartGameType()
{
	level.nbLaps = 3;
	level.position = 0;
	level.nbRaces = 0;
	level.tehbuilder = 0;
	level.allowPowerups = 0;
	level.oldbuilder = undefined;
	level.nbObjects = 0;
	level.raceInactive = 120;
	level.gameStarted = false;
	level.ready = false;
	level.object = getentarray("mp_deathmatch_spawn", "classname");
	level.nbObjects = 0;
	for(i = 0; i < level.object.size; i++)
	{
		level.object[i].type = "none";
	}

	precacheString(&"^2Special^0-^2Power");
	precacheString(&"^1None");
	precacheString(&"^1Blind^0-^1Target");
	precacheString(&"^1Freeze^0-^1Target");
	precacheString(&"^1Teleport^0-^1To^0-^1Target");
	precacheString(&"^1Teleport^0-^1Target^0-^1To^0-^1Self");
	precacheString(&"^1Random^0-^1Angle");

	precachemodel("xmodel/o_ctf_flag_r"); // checkpoint
	precachemodel("xmodel/zomgah"); // invis

	precacheShader("gfx/hud/objective.tga");
	precacheShader("gfx/hud/objective_up.tga");
	precacheShader("gfx/hud/objective_down.tga");

	setCvar("scr_race_timelimit", "0");

	precacheItem("race_pistol_mp");

	setCvar("ufa_infammo", "2");

	thread waitBuilder();
}

Callback_PlayerConnect()
{
	self.builder = false;
	self.finished = false;
	self.specialPower = "none";
	self.spawnOrigin = self.origin;
	self.spawnAngles = self.angles;
}

Callback_PlayerDisconnect()
{
	if(self.builder)
	{
		level.oldbuilder = undefined;
		self.builder = false;
		level thread newBuilder();
	}
}

spawnPlayer()
{
	self.specialPower = "none";
	self.takingPower = false;
	if(level.gameStarted)
		self powerText();

	if(isDefined(self.blindedBlack))
		self.blindedBlack destroy();

	if(level.gameStarted)
	{
		self iprintln("Shoot people to use powerups.");
		self setOrigin(self.spawnOrigin);
		self setplayerangles(self.spawnAngles);
		self.specialPower = "none";
		self setweaponslotweapon("primary", "race_pistol_mp");
		self setweaponslotclipammo("primary", "999");
		self setweaponslotammo("primary", "999");
		self switchToWeapon("race_pistol_mp");
	}
}

powerText()
{
	if(isDefined(self.powerText))
	{
		self.powerText destroy();
	}
	if(isDefined(self.powerTypeText))
	{
		self.powerTypeText destroy();
	}

	self.powerText = newClientHudElem( self );
	self.powerText.x = 599;
	self.powerText.y = 350;
	self.powerText.alignX = "right";
	self.powerText.alignY = "middle";
	self.powerText.alpha = 1;
	self.powerText.fontScale = 0.6;
	self.powerText setText(&"^2Special^0-^2Power");
	self.powerTypeText = newClientHudElem( self );
	self.powerTypeText.x = 599;
	self.powerTypeText.y = 360;
	self.powerTypeText.alignX = "right";
	self.powerTypeText.alignY = "middle";
	self.powerTypeText.alpha = 1;
	self.powerTypeText.fontScale = 0.6;
	if(self.specialPower == "none")
		self.powerTypeText setText(&"^1None");
	else if(self.specialPower == "warpto")
		self.powerTypeText setText(&"^1Teleport^0-^1To^0-^1Target");
	else if(self.specialPower == "warptarget")
		self.powerTypeText setText(&"^1Teleport^0-^1Target^0-^1To^0-^1Self");
	else if(self.specialPower == "freeze")
		self.powerTypeText setText(&"^1Freeze^0-^1Target");
	else if(self.specialPower == "blind")
		self.powerTypeText setText(&"^1Blind^0-^1Target");
	else if(self.specialPower == "angle")
		self.powerTypeText setText(&"^1Random^0-^1Angle");

}

usePower(target)
{
	if(self.specialPower == "warpto")
		self thread powerWarpTo(target);
	else if(self.specialPower == "warptarget")
		self thread powerWarpTarget(target);
	else if(self.specialPower == "freeze")
		self thread powerFreeze(target);
	else if(self.specialPower == "blind")
		self thread powerBlind(target);
	else if(self.specialPower == "angle")
		self thread powerAngle(target);

	self.specialPower = "none";
	self powerText();
}

powerWarpTo(target)
{
	self iprintlnbold("^2Teleported To Target.");
	self setOrigin(target.origin);
}
powerWarpTarget(target)
{
	self iprintlnbold("^2Teleported Target To Self.");
	target setOrigin(self.origin);
}

powerFreeze(target)
{
	self iprintlnbold("^2Freezing Target.");
	target thread frozen(3, self.name);
}

powerBlind(target)
{
	self iprintlnbold("^2Blinding Target.");
	target thread blinded(3, self.name);
}
powerAngle(target)
{
	self iprintlnbold("^2Lollerskates.");
	target iprintlnbold("^2LOOK OVER THERE.");
	target setplayerangles(vectortoangles((target.origin[0] - 10 + randomfloat(20), target.origin[1] - 10 + randomfloat(20), target.origin[2] - 10 + randomfloat(20)) - target.origin));
}
frozen( time, enemyname )
{
	self iprintlnbold("^2Frozen for ^7" + time + "^2 seconds by ^7" + enemyname + "^2.");
	wait 0.5;
	self.frozenSpot = self getOrigin();
	linker = spawn("script_origin", self.origin);
	self linkto(linker);
	for( i = ( time * 2); i > 0; i-- )
	{
		self setOrigin( self.frozenSpot );
		if(!isAlive(self))
		{
			self unlink();
			self setOrigin( self.frozenSpot );
			i = 0;
		}
		wait 0.5;
	}
	linker delete();
	self iprintlnbold("^2Not frozen anymore.");
}

blinded( time, enemyname )
{
	self iprintlnbold("^2Blinded for ^7" + time + "^2 seconds by ^7" + enemyname + "^2.");
	wait 0.5;
	if(!isDefined(self.blindedBlack))
	{
		self.blindedBlack = newClientHudElem(self);
		self.blindedBlack.archived = false;
		self.blindedBlack.x = 0;
		self.blindedBlack.y = 0;
		self.blindedBlack.sort = -100;
		self.blindedBlack setShader("black", 640, 800);
	}
	wait time;
	if(isDefined(self.blindedBlack))
		self.blindedBlack destroy();
	self iprintlnbold("^2Not blind anymore.");
}

spawnPowerUp()
{
	level.object[level.nbObjects].exist = true;
	level.object[level.nbObjects].type = "powerup";
	level.object[level.nbObjects] thread placePowerUp(self.origin);
}

placePowerUp(powerOrigin)
{
	players = getEntArray( "player", "classname" );
	self.origin = powerOrigin;
	level.nbObjects++;
	iprintlnbold("Placed powerup " + level.nbObjects + " (" + self.origin[0] + ", " + self.origin[1] + ", " + self.origin[2] + ").");
	wait 0.1;
	iprintlnbold((level.object.size-level.nbObjects) + " ^2Objects left to place.");
	while(self.exist)
	{
		for(i = 0; i < players.size; i++)
		{
			if(distance(players[i].origin, self.origin + (0,0,25)) <= 100 && players[i].pers["team"] != "spectator" && !players[i].takingPower && level.gameStarted && !players[i].finished)
			{
				playFx( level._effect["takepower"], self.origin + (0,0,25));
				players[i].takingPower = true;
				players[i] thread givePower();
				wait 5; // respawn powerup after 5 seconds
			}
		}
		playFx( level._effect["powerup"], self.origin + (0,0,25));
		wait 0.5;
	}
}

givePower()
{
	self.newPower = randomint(250) + 1;
	if(self.newPower <= 50)
	{
		self.specialPower = "blind";
		self iprintlnbold("^2New Power : ^7Blind Target^2.");
	}
	else if(self.newPower > 50 && self.newPower <= 100)
	{
		self.specialPower = "warpto";
		self iprintlnbold("^2New Power : ^7Teleport To Target^2.");
	}
	else if(self.newPower > 100 && self.newPower <= 150)
	{
		self.specialPower = "warptarget";
		self iprintlnbold("^2New Power : ^7Teleport Target To Self^2.");
	}
	else if(self.newPower > 150 && self.newPower <= 200)
	{
		self.specialPower = "freeze";
		self iprintlnbold("^2New Power : ^7Freeze Target^2.");
	}
	else if(self.newPower > 200 && self.newPower <= 250)
	{
		self.specialPower = "angle";
		self iprintlnbold("^2New Power : ^7Random Target Angles^2.");
	}

	self powerText();
	wait 5;
	self.takingPower = false;
}

waitBuilder()
{
//	for(j = 15; j > 0; j--)
	for(j = 60; j > 0; j--)
	{
		iprintln("Waiting for random builder... " + j);
		wait 1;
	}
	level newBuilder();
}

newBuilder()
{
	iprintlnbold("^1Choosing new builder...");
	wait 1.5;
	players = getEntArray( "player", "classname" );
	while(players.size <= 0)
	{
		iprintlnbold("Not enough players to start the game.");
		wait 3;
	}
	endSelect = false;
	while(!endSelect)
	{
		if(isdefined(level.oldbuilder))
		{
			for(i = 0; i < players.size; i++)
			{
				players[i].builder = false;
			}
		}
		level.tehbuilder = randomInt(players.size);
		wait 2;
		for(der = 0.05; der < 0.65 && players.size > 1; der += 0.05) // lottery
		{
			level.tehbuilder = randomInt(players.size);
			if(isdefined(level.oldbuilder) && players.size > 1)
			{
				while(level.tehbuilder == level.oldbuilder || !isdefined(players[level.tehbuilder]))
				{
					level.tehbuilder = randomInt(players.size);
				}
			}
			wait der;
			iprintlnbold(players[level.tehbuilder].name);
		}
/*		if(isdefined(level.oldbuilder))
		{
			while(level.tehbuilder == level.oldbuilder || !isdefined(players[level.tehbuilder]))
			{
				level.tehbuilder = randomInt(players.size);
			}
		}*/
		if(isdefined(level.oldbuilder))
		{
			if(players[level.oldbuilder] == players[level.tehbuilder] && players.size > 1)
			{
				iprintlnbold(players[level.oldbuilder].name + " ^2was the last builder, choosing someone else...");
				wait 3;
			}
			else
			{
				endSelect = true;
			}
		}
		else
		{
			endSelect = true;
		}
	}
	iprintlnbold(players[level.tehbuilder].name + " ^2is the new race builder.");
	players[level.tehbuilder] checkActive();
	if(players[level.tehbuilder].builder)
	{
		players[level.tehbuilder] iprintlnbold("^2Press ^3v ^2+ ^3R ^2to open the race builder menu.");
		level.oldbuilder = level.tehbuilder;
	}
}

checkActive()
{
	self iprintlnbold("^2You have 10 seconds to press and hold USE (F) to confirm you want to be a builder.");
	activeBuilder = false;
	for(i = 0; i < 100 && !activeBuilder; i++)
	{
		if(self useButtonPressed())
		{
			activeBuilder = true;
			self.builder = true;
			break;
		}
		wait 0.1;
	}
	if(!activeBuilder)
	{
		iprintlnbold(self.name + " is inactive, choosing another builder.");
		level thread newBuilder();
	}
}

builderMenu(response)
{
	if(!level.gameStarted && self.builder)
	{
		switch(response)
		{
		case "place_checkpoint":
			if(level.nbObjects < level.object.size)
				self spawnCheckpoint();
			else
				iprintlnbold("^2Reached max number of checkpoints for this map.");
			break;
		case "place_powerup":
			if(level.nbObjects < level.object.size)
				self spawnPowerUp();
			else
				iprintlnbold("^2Reached max number of powerups for this map.");
			break;
		case "remove_object":
			destroyObject();
			break;
		case "place_start":
			placeStart();
			break;
		case "reset":
			resetRace();
			break;
		case "start":
			self thread setLapNumber();
			break;
		case "switch_builder":
			self.builder = false;
			level thread newBuilder();
			break;
		case "end_race":
			self iprintlnbold("^1Race not started yet.");
			break;
		}
	}
	else if(level.gameStarted && self.builder)
	{
		switch(response)
		{
		case "end_race":
			thread endGame();
			break;
		}
	}
	else
	{
		self iprintlnbold("^1EAT POOP.");
	}
}

setLapNumber()
{
	iprintlnbold("^2Race Starting Soon...");
	self iprintlnbold("^2Press MELEE (SHIFT) to confirm the number of laps ^3[" + level.nbLaps + "]^2. After 20 seconds, the race will start automatically.");
	self iprintlnbold("^2Press USE (F) change the number of laps ^3[" + level.nbLaps + "]^2.");
	chooseLapTime = 0;
	while(!self meleeButtonPressed() && chooseLapTime < 66)
	{
		if(self useButtonPressed())
		{
			switch(level.nbLaps)
			{
			case 1 :
				level.nbLaps = 2;
				break;
			case 2 :
				level.nbLaps = 3;
				break;
			case 3 :
				level.nbLaps = 5;
				break;
			case 5 :
				level.nbLaps = 10;
				break;
			case 10 :
				level.nbLaps = 15;
				break;
			case 15 :
				level.nbLaps = 20;
				break;
			case 20 :
				level.nbLaps = 30;
				break;
			case 30 :
				level.nbLaps = 50;
				break;
			case 50 :
				level.nbLaps = 75;
				break;
			case 75 :
				level.nbLaps = 100;
				break;
			case 100 :
				level.nbLaps = 150;
				break;
			case 150 :
				level.nbLaps = 200;
				break;
			case 200 :
				level.nbLaps = 1;
				break;
			}
			self iprintlnbold("^2Press USE (F) change the number of laps ^3[" + level.nbLaps + "]^2.");
		}
		chooseLapTime++;
		wait 0.3;
	}
	iprintlnbold("^3[" + level.nbLaps + "]^2 Laps.");
	level startGame();
}

resetRace()
{
	iprintlnbold("^1Reset.");
	for(i = 0; i < level.object.size; i++)
	{
		if(level.object[i].type == "checkpoint")
		{
			level.objectModel[i] setmodel("xmodel/zomgah");
			level.objectModel[i] delete();
			level.fireball[i].exist = false;
			level.object[i].exist = false;
		}
		else if(level.object[i].type == "powerup")
		{
			level.object[i].exist = false;
		}
		level.object[i].type = "none";
	}
	level.nbObjects = 0;
	level.ready = false;
	level.startOrigin = undefined;
}

spawnCheckpoint()
{
	level.object[level.nbObjects].exist = true;
	level.object[level.nbObjects].origin = self.origin;
	level.object[level.nbObjects].type = "checkpoint";
	iprintlnbold("Placed checkpoint " + (level.nbObjects+1) + " (" + level.object[level.nbObjects].origin[0] + ", " + level.object[level.nbObjects].origin[1] + ", " + level.object[level.nbObjects].origin[2] + ").");
	if(level.nbObjects < 1)
		objective_add(level.nbObjects, "current", level.object[level.nbObjects].origin, "gfx/hud/objective.tga");
	level.objectModel[level.nbObjects] = spawn("script_model", level.object[level.nbObjects].origin);
	level.objectModel[level.nbObjects] setmodel("xmodel/o_ctf_flag_r");
	level.fireball[level.nbObjects] = spawn("script_origin", self.origin);
	level.fireball[level.nbObjects] thread moveFireBall(level.nbObjects);
	level.nbObjects++;
	wait 0.1;
	iprintlnbold((level.object.size-level.nbObjects) + " ^2Objects left to place.");
}

moveFireBall(num)
{
	self.origin = self.origin + (0,0,28);
	self.firstOrigin = self.origin;
	num++;
	self.exist = true;
	while(self.exist)
	{
		while(level.object[num].type != "checkpoint" && num < level.nbObjects)
		{
			num++;
			wait 0.5;
		}
		if(num >= level.nbObjects)
			j = 0;
		else
			j = num;
		self moveTo(level.object[j].origin + (0,0,28), 2.5);
		for(i = 0; i < 50 && self.exist; i++)
		{
			playFx(level._effect["race_fireball"], self.origin);
			wait 0.05;
		}
		self.origin = self.firstOrigin;
		wait 0.05;
	}	
}

destroyObject()
{
	if(level.nbObjects > 0)
	{
		level.nbObjects--;
		if(level.object[level.nbObjects].type == "checkpoint")
		{
			level.objectModel[level.nbObjects] setmodel("xmodel/zomgah");
			level.objectModel[level.nbObjects] delete();
			level.fireball[level.nbObjects].exist = false;
			level.object[level.nbObjects].exist = false;
			iprintlnbold("Removed object " + (level.nbObjects+1) + "(checkpoint) (" + level.object[level.nbObjects].origin[0] + ", " + level.object[level.nbObjects].origin[1] + ", " + level.object[level.nbObjects].origin[2] + ").");
		}
		else if(level.object[level.nbObjects].type == "powerup")
		{
			level.object[level.nbObjects].exist = false;
			iprintlnbold("Removed object " + (level.nbObjects+1) + "(powerup) (" + level.object[level.nbObjects].origin[0] + ", " + level.object[level.nbObjects].origin[1] + ", " + level.object[level.nbObjects].origin[2] + ").");
		}
		level.object[level.nbObjects].type = "none";
	}
	else
	{
		iprintlnbold("No object to remove");
	}
}

placeStart()
{
	iprintlnbold("Placed start point (" + self.origin[0] + ", " + self.origin[1] + ", " + self.origin[2] + ").");
	level.startOrigin = self.origin;
	level.ready = true;
	players = getEntArray( "player", "classname" );
	for(i = 0; i < players.size; i++)
	{
		players[i].spawnOrigin = level.startOrigin;
	}
}

startGame()
{
	iprintlnbold("^1Game starting if you're spec, you have 5 seconds to join...");
	wait 5;
	level.raceInactive = 126 + (level.nbObjects*4);
	if(level.ready && level.nbObjects >= 2)
	{
		players = getEntArray( "player", "classname" );	
		for(i = 0; i < players.size; i++)
		{
			if(isdefined(players[i]) && players[i].pers["team"] != "spectator") // if someone disconnects in the middle of this.
			{
				players[i] thread readyPlayer();
			}
		}
		checkRaceInactivity();
	}
	else
	{
		iprintlnbold("^1Place a start point and at least two checkpoints before starting the game");
	}

}

readyPlayer()
{
	if(level.ready && level.nbObjects >= 2)
	{
		self.randomorigin = (-40 + randomInt(81), -40 + randomInt(81), -40 + randomInt(81));
		self.linker = spawn("script_origin", level.startOrigin + self.randomorigin);
		self powerText();
		self setOrigin(level.startOrigin + self.randomorigin);
		self linkto(self.linker);
		self setClientCvar( "cg_thirdperson", "1" );
		wait 4;
		for(j = 0; j < level.nbObjects; j++)
		{
			if(level.object[j].type == "checkpoint")
			{
				self.randomorigin = (-40 + randomInt(81), -40 + randomInt(81), -40 + randomInt(81));
				self.linker moveTo(level.object[j].origin + self.randomorigin, 2);
				wait 4;
			}
		}
		self.linker moveTo(level.startOrigin, 2);
		wait 2;
		self setClientCvar( "cg_thirdperson", "0" );
		wait 3;
		self iprintlnbold("^1READY...");
		wait 1;
		self iprintlnbold("^3SET...");
		wait 1;
		self iprintlnbold("^2GO!");
		level.gameStarted = true; // GOOOO!!!
		self.linker delete();
		self racing();
	}
	else
	{
		iprintlnbold("^1Place a start point and at least two checkpoints before starting the game");
	}
}

racing()
{
	self.currentCheckpoint = 0;
	self.specialPower = "none";
	self.finished = false;
	self.score = -100;
	self.currentLap = 0;
	self.totalTime = 0;
	self.lapTime = 0;
	self setweaponslotweapon("primary", "race_pistol_mp");
	self setweaponslotclipammo("primary", "999");
	self setweaponslotammo("primary", "999");
	self switchToWeapon("race_pistol_mp");
	
	while(level.gameStarted)
	{
		while(level.object[self.currentCheckpoint].type != "checkpoint" && self.currentCheckpoint < level.nbObjects)
		{
			self.currentCheckpoint++;
		}
		if(level.nbLaps > 1)
		{
			self.lapTime++;
			if( distance(self.origin, level.object[self.currentCheckpoint].origin) <= 45 )
			{
				if(self.currentLap <= 0)
				{
					level.raceInactive = 120;
					self.currentLap++;
					self iprintln("^2Lap ^7" + self.currentLap + "^2/^7" + level.nbLaps + "^2.");
				}
				else if(self.currentLap < level.nbLaps && self.currentCheckpoint == 0)
				{
					level.raceInactive = 120;
					self.currentLap++;
					self.totalTime += self.lapTime;
					self iprintln("^2Lap ^7" + self.currentLap + "^2/^7" + level.nbLaps + " ^2Lap Time : ^7" + self.lapTime + "^2.");
					self.lapTime = 0;
				}
				else if(self.currentLap >= level.nbLaps && self.currentCheckpoint == 0)
				{
					level.raceInactive = 120;
					level.position++;
					self.totalTime += self.lapTime;
					iprintln("^2" + level.position + "- ^7" + self.name + " ^2Lap Time : ^7" + self.lapTime + " ^3Total Time : ^7" + self.totalTime + "2.");
					self.lapTime = 0;
					self.score = 0;
					self.deaths = 0;
					self.score -= level.position;
					self.finished = true;
					self.specialPower = "none";
					self.totalTime = 0;
					self checkFinish();
					if(level.position == 1 && level.gameStarted)
						thread timedEnd();
					break;
				}
				else
				{
					level.raceInactive = 120;
					self iprintln("^1Checkpoint! Current Lap Time : ^7" + self.lapTime + "^1.");
				}
				self.spawnOrigin = self.origin;
				self.spawnAngles = self.angles;
				self.currentCheckpoint++;
			}
			if(self.currentCheckpoint >= level.nbObjects)
			{
				self.currentCheckpoint = 0;
			}
		}
		else
		{
			self.totalTime++;
			if( distance(self.origin, level.object[self.currentCheckpoint].origin) <= 45 )
			{
				level.raceInactive = 120;
				self.currentLap++;
				self iprintln("^2Time : ^7" + self.totalTime + "^2.");
				self.currentCheckpoint++;
			}
			if(self.currentCheckpoint >= level.nbObjects)
			{
				level.raceInactive = 120;
				level.position++;
				self.currentCheckpoint = 0;
				self.score = 0;
				self.deaths = 0;
				self.score -= level.position;
				self.totalTime = 0;
				self.finished = true;
				self checkFinish();
				if(level.position == 1 && level.gameStarted)
					self thread timedEnd();
				break;
			}
		}
		wait 0.1;
	}
	self.specialPower = "none";
	self takeWeapon("race_pistol_mp");
}

timedEnd()
{
	iprintlnbold("^160 seconds until game over.");
	for(i = 0; i < 60; i++)
	{	
		level.raceInactive = 120;
		wait 1;
		if(!level.gameStarted)
			return;
	}
	if(level.gameStarted)
		self endGame();
}

checkRaceInactivity()
{
	while(level.raceInactive > 0)
	{
		level.raceInactive--;
		wait 1;
	}
	if(level.gameStarted)
	{
		iprintlnbold("^1Race Inactive");
		endGame();
	}
}

checkFinish()
{
	players = getEntArray( "player", "classname" );
	check = false;
	for(i = 0; i < players.size; i++)
	{
		if(!players[i].finished)
			check = true;
	}
	if(!check)
	{
		if(level.gameStarted)
			endGame();
	}
}

endGame()
{
	level.gameStarted = false;
	level.raceInactive = 0;
	level.position = 0;
	iprintlnbold("^1RACE OVER.");
	level.nbRaces++;
	wait 5;
	if(getCvarInt("scr_race_nbraces") > level.nbRaces)
	{
		dertotalraces = getCvarInt("scr_race_nbraces");
		iprintlnbold((dertotalraces - level.nbRaces) + " ^2 more races to go...");
		wait 3;
		resetRace();
		iprintlnbold("^3New builder in 10 seconds...");
		wait 10;
		level thread newBuilder();
	}
	else
	{
		iprintlnbold("^1GAME OVER.");
		wait 1;
		iprintlnbold("^3Map Ending in 30 seconds.");
		setCvar("scr_race_timelimit", "0.5");
	}
}