powerMain()
{
	level._effect["powerup"] = loadFx( "fx/boux/powerup.efx" );
	level._effect["takepower"] = loadFx( "fx/boux/takepower.efx" );
	level._effect["ghost"] = loadFx( "fx/boux/ghost.efx" );
	level._effect["poweruppassive"] = loadFx( "fx/boux/poweruppassive.efx" );
	level._effect["takepowerpassive"] = loadFx( "fx/boux/takepowerpassive.efx" );
	level._effect["powerupweapon"] = loadFx( "fx/boux/powerupweapon.efx" );
	level._effect["takepowerweapon"] = loadFx( "fx/boux/takepowerweapon.efx" );
	level._effect["freeze"] = loadFx( "fx/boux/freeze.efx" );
	level._effect["blind"] = loadFx( "fx/boux/blind.efx" );
	level._effect["boom"] = loadFx( "fx/boux/hehe.efx" );
	level._effect["teleport_in"] = loadFx( "fx/boux/teleport_in.efx" );
	level._effect["teleport_out"] = loadFx( "fx/boux/teleport_out.efx" );
	level._effect["invis"] = loadFx( "fx/boux/invis.efx" );
}

Callback_StartGameType()
{
	level.nbPowerUps = 0;
	level.startSpawn = false;
	level.totalPowerUps = 0;
	thread spawnPowerUp();
	precachemodel("xmodel/mp_crate_misc_red1");
	precacheModel("xmodel/mp_crate_misc1_stalingrad");
	precachemodel("xmodel/barrel_tan_snowy");
	precachemodel("xmodel/barrel_black1_stalingrad");
	precachemodel("xmodel/barrel_benzin");
	precachemodel("xmodel/zomgah");
	precacheString(&"^2Special^0-^2Power");
	precacheString(&"^1None");
	precacheString(&"^1Random^0-^1Teleport");
	precacheString(&"^1Freeze^0-^1Enemies^0-^1In^0-^1Radius");
	precacheString(&"^1Blind^0-^1Enemies^0-^1In^0-^1Radius");
	precacheString(&"^1Ghost");
	precacheString(&"^1Temporary^0-^1Invisibility");
	precacheString(&"^1Drop^0-^1Enemy^0-^1Weapons");
	precacheString(&"^1Explosive^0-^1Ammo");
	precacheString(&"^1Unlimited^0-^1Ammo");
	precacheString(&"^1Radar");
	precacheString(&"^2Health");
	precacheString(&"^2|");
	precacheString(&"^4Passive^0-^4PowerUp");
	precacheString(&"^1X2^0-^1Damage");
	precacheString(&"^1Armor^0-^1Low^0-^1Quality");
	precacheString(&"^1Armor^0-^1High^0-^1Quality");
	precacheString(&"^1One^0-^1Shot^0-^1Kill");
	precacheString(&"^1Temporary^0-^1Invulnerability");
	precacheString(&"^1Great^0-^1Insight");
	precacheString(&"^1Aimbot");
	precacheString(&"^1Reflect^0-^1Damage");
	precacheShader("gfx/hud/objective.tga");
	precacheShader("gfx/hud/objective_up.tga");
	precacheShader("gfx/hud/objective_down.tga");

	precacheItem("ghost_mp");
	precacheItem("plasma_mp");
	precacheItem("nuke_mp");
	precacheItem("precision_pdm_mp");

	setCvar("ufa_infammo", "0");
}

Callback_PlayerConnect()
{
	self.passiveTime = 0;
	self.god = false;
	self.isGhost = false;
	self.specialPower = "none";
	self.passivePower = "none";
	self thread doPassive();
	self thread usePower();
	self thread pickUpPower();
}

spawnPlayer()
{
	if(!isDefined(self.pers["savedmodel"]))
		self maps\mp\gametypes\_teams::model();
	else
		self maps\mp\_utility::loadModel(self.pers["savedmodel"]);

	if(!level.startSpawn)
		level.startSpawn = true;

	self.superGun = 0;
	self.god = false;
	self.isGhost = false;
	self.passiveTime = 0;
	self.specialPower = "none";
	self.passivePower = "none";
	self.maxhealth = 100;
	self powerText();
	self passiveText();
	self healthText();
	self setClientCvar( "r_xdebug", "0" );
	self setClientCvar( "developer", "0" );

	if(isDefined(self.blindedBlack))
		self.blindedBlack destroy();

	self iprintln("Double press use button to use powerups.");
}

powerText()
{
	if(isDefined(self.powerText))
	{
		self.powerText destroy();
	}
	if(isDefined(self.powerTypeText))
	{
		self.powerTypeText destroy();
	}

	self.powerText = newClientHudElem( self );
	self.powerText.x = 599;
	self.powerText.y = 340;
	self.powerText.alignX = "right";
	self.powerText.alignY = "middle";
	self.powerText.alpha = 1;
	self.powerText.fontScale = 0.6;
	self.powerText setText(&"^2Special^0-^2Power");
	self.powerTypeText = newClientHudElem( self );
	self.powerTypeText.x = 599;
	self.powerTypeText.y = 350;
	self.powerTypeText.alignX = "right";
	self.powerTypeText.alignY = "middle";
	self.powerTypeText.alpha = 1;
	self.powerTypeText.fontScale = 0.6;
	if(self.specialPower == "none")
		self.powerTypeText setText(&"^1None");
	else if(self.specialPower == "warp")
		self.powerTypeText setText(&"^1Random^0-^1Teleport");
	else if(self.specialPower == "transform")
		self.powerTypeText setText(&"^1Transform");
	else if(self.specialPower == "freeze")
		self.powerTypeText setText(&"^1Freeze^0-^1Enemies^0-^1In^0-^1Radius");
	else if(self.specialPower == "smoke")
		self.powerTypeText setText(&"^1Smoke^0-^1Screen");
	else if(self.specialPower == "blind")
		self.powerTypeText setText(&"^1Blind^0-^1Enemies^0-^1In^0-^1Radius");
	else if(self.specialPower == "invis")
		self.powerTypeText setText(&"^1Ghost");
	else if(self.specialPower == "speed")
		self.powerTypeText setText(&"^1Temporary^0-^1Super^0-^1Speed");
	else if(self.specialPower == "radar")
		self.powerTypeText setText(&"^1Radar");
	else if(self.specialPower == "god")
		self.powerTypeText setText(&"^1Temporary^0-^1Invulnerability");
	else if(self.specialPower == "drop")
		self.powerTypeText setText(&"^1Drop^0-^1Enemy^0-^1Weapons");
}

passiveText()
{
	if(isDefined(self.powerPassiveText))
	{
		self.powerPassiveText destroy();
	}
	if(isDefined(self.powerPassiveTypeText))
	{
		self.powerPassiveTypeText destroy();
	}

	self.powerPassiveText = newClientHudElem( self );
	self.powerPassiveText.x = 599;
	self.powerPassiveText.y = 370;
	self.powerPassiveText.alignX = "right";
	self.powerPassiveText.alignY = "middle";
	self.powerPassiveText.alpha = 1;
	self.powerPassiveText.fontScale = 0.6;
	self.powerPassiveText setText(&"^4Passive^0-^4PowerUp");
	self.powerPassiveTypeText = newClientHudElem( self );
	self.powerPassiveTypeText.x = 599;
	self.powerPassiveTypeText.y = 380;
	self.powerPassiveTypeText.alignX = "right";
	self.powerPassiveTypeText.alignY = "middle";
	self.powerPassiveTypeText.alpha = 1;
	self.powerPassiveTypeText.fontScale = 0.6;
	if(self.passivePower == "none")
		self.powerPassiveTypeText setText(&"^1None");
	else if(self.passivePower == "one_shot")
		self.powerPassiveTypeText setText(&"^1One^0-^1Shot^0-^1Kill");
	else if(self.passivePower == "x2")
		self.powerPassiveTypeText setText(&"^1X2^0-^1Damage");
	else if(self.passivePower == "armor_low")
		self.powerPassiveTypeText setText(&"^1Armor^0-^1Low^0-^1Quality");
	else if(self.passivePower == "armor_high")
		self.powerPassiveTypeText setText(&"^1Armor^0-^1High^0-^1Quality");
	else if(self.passivePower == "insight")
		self.powerPassiveTypeText setText(&"^1Great^0-^1Insight");
	else if(self.passivePower == "explosive_ammo")
		self.powerPassiveTypeText setText(&"^1Explosive^0-^1Ammo");
	else if(self.passivePower == "infammo")
		self.powerPassiveTypeText setText(&"^1Unlimited^0-^1Ammo");
	else if(self.passivePower == "aimbot")
		self.powerPassiveTypeText setText(&"^1Aimbot");
	else if(self.passivePower == "reflect")
		self.powerPassiveTypeText setText(&"^1Reflect^0-^1Damage");
}

healthText()
{
	if(self.health >= 2000)
		self.health = 2000;
	if(self.maxhealth >= 2000)
		self.maxhealth = 2000;
	if(isDefined(self.healthText))
	{
		self.healthText destroy();
	}
	if(isDefined(self.healthCurrentText))
	{
		self.healthCurrentText destroy();
	}
	if(isDefined(self.healthMaxText))
	{
		self.healthMaxText destroy();
	}
	if(isDefined(self.healthBarText))
	{
		self.healthBarText destroy();
	}

	self.healthText = newClientHudElem( self );
	self.healthText.x = 599;
	self.healthText.y = 400;
	self.healthText.alignX = "right";
	self.healthText.alignY = "middle";
	self.healthText.alpha = 1;
	self.healthText.fontScale = 0.6;
	self.healthText setText(&"^2Health");
	self.healthCurrentText = newClientHudElem( self );
	self.healthCurrentText.x = 585;
	self.healthCurrentText.y = 410;
	self.healthCurrentText.alignX = "right";
	self.healthCurrentText.alignY = "middle";
	self.healthCurrentText.alpha = 1;
	self.healthCurrentText.fontScale = 0.6;
	self.healthCurrentText setValue((int)(self.health));
	self.healthBarText = newClientHudElem( self );
	self.healthBarText.x = 588;
	self.healthBarText.y = 410;
	self.healthBarText.alignX = "center";
	self.healthBarText.alignY = "middle";
	self.healthBarText.alpha = 1;
	self.healthBarText.fontScale = 0.6;
	self.healthBarText setText(&"^2|");
	self.healthMaxText = newClientHudElem( self );
	self.healthMaxText.x = 591;
	self.healthMaxText.y = 410;
	self.healthMaxText.alignX = "left";
	self.healthMaxText.alignY = "middle";
	self.healthMaxText.alpha = 1;
	self.healthMaxText.fontScale = 0.6;
	self.healthMaxText setValue((int)(self.maxhealth));
}

usePower()
{
	for(;;)
	{
		if(self useButtonPressed())
		{
			catch_next = false;
			for(i=0; i<=0.25; i+=0.01)
			{
				if(catch_next && self useButtonPressed() && self.sessionstate == "playing")
				{
					if(self.specialPower == "warp")
					{
						self thread powerWarp();
					}
					else if(self.specialPower == "freeze")
					{
						self thread powerFreeze();
					}
					else if(self.specialPower == "blind")
					{
						self thread powerBlind();
					}
					else if(self.specialPower == "radar")
					{
						self thread powerRadar();
					}
					else if(self.specialPower == "invis")
					{
						self thread powerInvis();
					}
					else if(self.specialPower == "god")
					{
						self thread powerGod();
					}
					else if(self.specialPower == "drop")
					{
						self thread powerDrop();
					}
					else
					{
						self iprintlnbold("You currently have no special powers");
					}
					self.specialPower = "none";
					catch_next = false;
					self powerText();
				}
				else if(!(self useButtonPressed()))
				{
					catch_next = true;
				}
				wait 0.01;
			}
		}
		self healthText();
		wait 0.05;
	}
}

powerWarp()
{
	playFx( level._effect["teleport_in"], self.origin + (0,0,50) );
	self iprintlnbold("^2Randomly Teleported.");
	warptoplace = "mp_deathmatch_spawn";
	place = getentarray(warptoplace, "classname");
	self.where = randomint(place.size);
	self setOrigin(place[self.where].origin);
	self setPlayerAngles(place[self.where].angles);
	playFx( level._effect["teleport_out"], self.origin + (0,0,50) );
}

powerRadar()
{
	self iprintlnbold("^2Radar.");
	players = getEntArray( "player", "classname" );
	for(x = 0; x < (60 + randomint(30)); x++)
	{
		for(i = 0; i < players.size; i++)
		{
			if(players[i].pers["team"] != self.pers["team"] && isAlive(players[i]))
			{
				objective_add(i, "current", players[i].origin, "gfx/hud/objective.tga");
				objective_icon(i,"gfx/hud/objective.tga");
				if(self.pers["team"] == "allies")
				{
					objective_team(i,"allies");
				}
				else if(self.pers["team"] == "axis")
				{
					objective_team(i,"axis");
				}
			}
		}
		wait 0.5;
		for(i = 0; i < players.size; i++)
		{
			if(players[i].pers["team"] != self.pers["team"])
			{
				objective_delete(i);
			}
		}
	}
}

powerFreeze()
{
	self iprintlnbold("^2Freezing enemies near you.");
	playFx( level._effect["freeze"], self.origin + (0,0,50) );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( self != players[i] && distance(self.origin, players[i].origin) <= 400 )
		{
			players[i] thread frozen(10 + randomint(6), self.name);
		}
	}
}

frozen( time, enemyname )
{
	self iprintlnbold("^2Frozen for ^7" + time + "^2 seconds by ^7" + enemyname + "^2.");
	wait 0.5;
	self.frozenSpot = self getOrigin();
	linker = spawn("script_origin", self.origin);
	self linkto(linker);
	for( i = ( time * 2); i > 0; i-- )
	{
		self setOrigin( self.frozenSpot );
		if(!isAlive(self))
		{
			self unlink();
			self setOrigin( self.frozenSpot );
			i = 0;
		}
		wait 0.5;
	}
	linker delete();
	self iprintlnbold("^2Not frozen anymore.");
}

powerDrop()
{
	self iprintlnbold("^2Making Enemies around you drop their weapons.");
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( self != players[i] && distance(self.origin, players[i].origin) <= 400 )
		{
			players[i] thread drop();
		}
	}
}

drop()
{
	playFx( level._effect["powerup"], self.origin + (0,0,150) );
	self iprintlnbold("^2Dropped all weapons.");
	self setweaponslotammo("primary", "0");
	self setweaponslotclipammo("primary", "0");
	self setweaponslotammo("primaryb", "0");
	self setweaponslotclipammo("primaryb", "0");
	self setweaponslotammo("pistol", "0");
	self setweaponslotclipammo("pistol", "0");
	self setweaponslotammo("grenade", "0");
	self setweaponslotclipammo("grenade", "0");
	self setweaponslotammo("smokegrenade", "0");
	self setweaponslotclipammo("smokegrenade", "0");
	self setweaponslotammo("satchel", "0");
	self setweaponslotclipammo("satchel", "0");
	self setweaponslotammo("binocular", "0");
	self setweaponslotclipammo("binocular", "0");
	self dropItem( self getWeaponSlotWeapon( "primary" ) );
	self dropItem( self getWeaponSlotWeapon( "primaryb" ) );
	self dropItem( self getWeaponSlotWeapon( "pistol" ) );
	self dropItem( self getWeaponSlotWeapon( "grenade" ) );
	self dropItem( self getWeaponSlotWeapon( "smokegrenade" ) );
	self dropItem( self getWeaponSlotWeapon( "satchel" ) );
	self dropItem( self getWeaponSlotWeapon( "binocular" ) );
}

powerBlind()
{
	self iprintlnbold("^2Blinding enemies near you.");
	playFx( level._effect["blind"], self.origin + (0,0,50) );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( self != players[i] && distance(self.origin, players[i].origin) <= 450 )
		{
			players[i] thread blinded(10 + randomint(6), self.name);
		}
	}
}

blinded( time, enemyname )
{
	self iprintlnbold("^2Blinded for ^7" + time + "^2 seconds by ^7" + enemyname + "^2.");
	wait 0.5;
	if(!isDefined(self.blindedBlack))
	{
		self.blindedBlack = newClientHudElem(self);
		self.blindedBlack.archived = false;
		self.blindedBlack.x = 0;
		self.blindedBlack.y = 0;
		self.blindedBlack.sort = -100;
		self.blindedBlack setShader("black", 640, 800);
	}
	wait time;
	if(isDefined(self.blindedBlack))
		self.blindedBlack destroy();
	self iprintlnbold("^2Not blind anymore.");
}

powerGod()
{
	self.god = true;
	time = 15 + randomint(13);
	self iprintlnbold("^2Invulnerable for ^7" + time + "^2 seconds.");
	wait time;
	self iprintlnbold("^2Not Invulnerable Anymore.");
	self.god = false;	
}

powerInvis()
{
	playFx( level._effect["invis"], self.origin + (0,0,50) );
	self.transTime = 15 + randomint(11);
	self.isGhost = true;
	self iprintlnbold("^2Become a Ghost for ^7" + self.transTime + "^2 seconds.");
	self detachAll();
	self setModel("xmodel/zomgah");
	self setweaponslotweapon("primary", "ghost_mp");
	self switchToWeapon("ghost_mp");
	for(i = self.transTime*4; i > 0; i--)
	{
		playFx( level._effect["ghost"], self.origin + (0,0,20) );
		wait 0.25;
	}
	self setweaponslotweapon("primary", self.pers["spawnweapon"]);
	self switchToWeapon(self.pers["spawnweapon"]);
	self iprintlnbold("^2Not a Ghost anymore.");
	if(!isDefined(self.pers["savedmodel"]))
		self maps\mp\gametypes\_teams::model();
	else
		self maps\mp\_utility::loadModel(self.pers["savedmodel"]);
	self.isGhost = false;
}

spawnPowerUp()
{
	level.newPowerUp = false;
	powerplace = "mp_deathmatch_spawn";
	level.powerUp = getentarray(powerplace, "classname");
	for(i = 0; i < level.powerUp.size; i++)
	{
		level.powerUp[i].exist = false;
		level.powerUp[i].type = "none";
	}
	for(;;)
	{
		wait 10;
		while(!level.newPowerUp && level.nbPowerUps < level.powerUp.size)
		{
			num = randomint(level.PowerUp.size);
			if(!level.powerUp[num].exist)
			{
				level.powerUp[num].exist = true;
				level.newPowerUp = true;
				level.nbPowerUps++;
				level.totalPowerUps++;
				wait 0.5;
				if((level.totalPowerUps % 5) == 0 || (level.totalPowerUps % 5) == 2) 
					level.powerUp[num] thread placePowerUp();
				else if((level.totalPowerUps % 5) == 1 || (level.totalPowerUps % 5) == 3) 
					level.powerUp[num] thread placePowerUpPassive();
				else if((level.totalPowerUps % 5) == 4)
					level.powerUp[num] thread placePowerUpWeapon();
				else
					level.powerUp[num] thread placePowerUp();
			}
		}

		level.newPowerUp = false;
	}
}

pickUpPower()
{
	while(1)
	{
		for(i = 0; i < level.powerUp.size; i++)
		{
			if(distance(self.origin, level.powerUp[i].origin + (0,0,25)) <= 100 && self.pers["team"] != "spectator" && level.powerUp[i].exist)
			{
				if(level.powerUp[i].type == "blue")
				{
					if(self.specialPower != "none")
					{
						self iprintlnbold("^2You already have a power.");
					}
					else
					{
						playFx( level._effect["takepower"], level.powerUp[i].origin + (0,0,25));
						level.powerUp[i].exist = false;
						level.powerUp[i].type = "none";
						level.nbPowerUps--;
						self thread givePower();
					//	level.powerUp[i] movePowerUp();
					}
				}
				else if(level.powerUp[i].type == "red")
				{
					playFx( level._effect["takepowerpassive"], level.powerUp[i].origin + (0,0,25));
					level.powerUp[i].exist = false;
					level.powerUp[i].type = "none";
					level.nbPowerUps--;
					self thread givePowerPassive();
				//	level.powerUp[i] movePowerUp();
				}
				else if(level.powerUp[i].type == "green")
				{
					playFx( level._effect["takepowerweapon"], level.powerUp[i].origin + (0,0,25));
					level.powerUp[i].exist = false;
					level.powerUp[i].type = "none";
					level.nbPowerUps--;
					self thread givePowerWeapon();
				//	level.powerUp[i] movePowerUp();
				}
			}
		}
		wait 0.5;
	}
}

placePowerUp()
{
	players = getEntArray( "player", "classname" );
	self.type = "blue";
	while(self.exist)
	{
		playFx( level._effect["powerup"], self.origin + (0,0,25));
		wait 0.5;
	}
}

placePowerUpPassive()
{
	players = getEntArray( "player", "classname" );
	self.type = "red";
	while(self.exist)
	{
		playFx( level._effect["poweruppassive"], self.origin + (0,0,25));
		wait 0.5;
	}
}

placePowerUpWeapon()
{
	players = getEntArray( "player", "classname" );
	self.type = "green";
	while(self.exist)
	{
		playFx( level._effect["powerupweapon"], self.origin + (0,0,25));
		wait 0.5;
	}
}


movePowerUp() // Disabled for now
{
/*
	players = getEntArray( "player", "classname" );
	trynumber = 0;
	n = randomInt(players.size);
	while(players[n].pers["team"] == "spectator" && trynumber < 10)
	{
		trynumber++;
		n = randomInt(players.size);
		wait 0.1;
	}
	self setOrigin(players[n].origin);
*/
}


givePower()
{
	self.newPower = randomint(300) + 1;
	if(self.newPower <= 50)
	{
		self.specialPower = "blind";
		self iprintlnbold("^2New Power : ^7Blind Nearby Enemies^2.");
	}
	else if(self.newPower > 50 && self.newPower <= 100)
	{
		self.specialPower = "warp";
		self iprintlnbold("^2New Power : ^7Random Teleportation^2.");
	}
	else if(self.newPower > 100 && self.newPower <= 150)
	{
		self.specialPower = "freeze";
		self iprintlnbold("^2New Power : ^7Freeze Nearby Enemies^2.");
	}
	else if(self.newPower > 150 && self.newPower <= 200)
	{
		self.specialPower = "invis";
		self iprintlnbold("^2New Power : ^7Ghost^2.");
	}
	else if(self.newPower > 200 && self.newPower <= 250)
	{
		self.specialPower = "drop";
		self iprintlnbold("^2New Power : ^7Drop Nearby Enemies' Weapons^2.");
	}
	else if(self.newPower > 250 && self.newPower <= 300)
	{
		self.specialPower = "god";
		self iprintlnbold("^2New Power : ^7Invincibility^2.");
	}
	self powerText();
}

givePowerPassive()
{
/*	self.newPower = randomint(290) + 1;
	if(self.newPower <= 25)
	{
		addhealth = randomint(150) + 250;
		self iprintlnbold("^2Add to health : ^7" + addhealth + "^2.");
		self.health += addhealth;
		self.maxhealth += addhealth;
	}
	else if(self.newPower > 25 && self.newPower <= 40)
	{
		self iprintlnbold("^2Health ^7X2^2.");
		self.health *= 2;
		self.maxhealth *= 2;
	}
	else if(self.newPower > 40 && self.newPower <= 50)
	{
		self iprintlnbold("^2Health ^7X4^2.");
		self.health *= 4;
		self.maxhealth *= 4;
	}
	else if(self.newPower > 50 && self.newPower <= 55)
	{
		self iprintlnbold("^2Health ^7X8^2.");
		self.health *= 8;
		self.maxhealth *= 8;
	}
	else if(self.newPower > 55 && self.newPower <= 95)
	{	
		self checkCurrentPassive();
		self.passiveTime = 20 + randomint(30);
		self.passivePower = "x2";
		self iprintlnbold("^2X2 Damage : ^7" + self.passiveTime + "^2 seconds.");
	}
	else if(self.newPower > 95 && self.newPower <= 115)
	{
		self checkCurrentPassive();
		self.passiveTime = 20 + randomint(30);
		self.passivePower = "one_shot";
		self iprintlnbold("^2One Shot Kill : ^7" + self.passiveTime + "^2 seconds.");
	}
	else if(self.newPower > 115 && self.newPower <= 155)
	{
		self checkCurrentPassive();
		self.passiveTime = 20 + randomint(30);
		self.passivePower = "armor_low";
		self iprintlnbold("^2Armor : Low - ^7" + self.passiveTime + "^2 seconds.");
	}
	else if(self.newPower > 155 && self.newPower <= 180)
	{
		self checkCurrentPassive();
		self.passiveTime = 20 + randomint(30);
		self.passivePower = "armor_high";
		self iprintlnbold("^2Armor : High - ^7" + self.passiveTime + "^2 seconds.");
	}
	else if(self.newPower > 180 && self.newPower <= 195)
	{
		self checkCurrentPassive();
		self.passiveTime = 20 + randomint(30);
		self.passivePower = "insight";
		self iprintlnbold("^2See Through Walls : ^7" + self.passiveTime + "^2 seconds.");
		self setClientCvar( "r_xdebug", "boxes" );
	}
	else if(self.newPower > 195 && self.newPower <= 210) // to test
	{
		self checkCurrentPassive();
		self.passiveTime = 20 + randomint(16);
		self.passivePower = "explosive_ammo";
		self iprintlnbold("^2Temporary Explosive Ammo : ^7" + self.passiveTime + "^2 seconds.");
	}
	else if(self.newPower > 210 && self.newPower <= 235) // to test
	{
		self checkCurrentPassive();
		self.passiveTime = 20 + randomint(16);
		self.passivePower = "infammo";
		self iprintlnbold("^2Temporary Unlimited Ammo : ^7" + self.passiveTime + "^2 seconds.");
		self thread passiveInfAmmo();
	}
	else if(self.newPower > 235 && self.newPower <= 260) // to test
	{*/
		self checkCurrentPassive();
		self.passiveTime = 1000 + randomint(16);
		self.passivePower = "aimbot";
		self iprintlnbold("^2Temporary Aimbot : ^7" + self.passiveTime + "^2 seconds.");
		self thread passiveAimbot();
/*	}
	else if(self.newPower > 260 && self.newPower <= 290) // to test
	{
		self checkCurrentPassive();
		self.passiveTime = 20 + randomint(26);
		self.passivePower = "reflect";
		self iprintlnbold("^2Temporary Partial Damage Reflection : ^7" + self.passiveTime + "^2 seconds.");
	}*/
	self healthText();
	self passiveText();
}

givePowerWeapon()
{
	self.newPower = randomint(300) + 1;
	self.superGun = 1;
	if(self.newPower <= 100)
	{
		self iprintlnbold("^2Special Weapon : ^7Precision Rifle^2.");
		ammocount = getfullclipammo("precision_pdm_mp") * 2;
		self setWeaponSlotWeapon("primaryb", "precision_pdm_mp");
		self setweaponslotclipammo("primaryb", "999");
		self setweaponslotammo("primaryb", ammocount);
		self switchToWeapon("precision_pdm_mp");
	}
	else if(self.newPower > 100 && self.newPower <= 200)
	{
		self iprintlnbold("^2Special Weapon : ^0P^4l^5a^7sma ^0P^4i^5s^7tol^2.");
		ammocount = getfullclipammo("plasma_mp") * 3;
		self setWeaponSlotWeapon("primaryb", "plasma_mp");
		self setweaponslotclipammo("primaryb", "999");
		self setweaponslotammo("primaryb", ammocount);
		self switchToWeapon("plasma_mp");
	}
	else if(self.newPower > 200 && self.newPower <= 300)
	{
		self iprintlnbold("^2Special Weapon : ^0N^1uke^2.");
		self setWeaponSlotWeapon("primaryb", "nuke_mp");
		self setweaponslotclipammo("primaryb", "1");
		self setweaponslotammo("primaryb", 0);
		self switchToWeapon("nuke_mp");
	}
}

checkCurrentPassive()
{
	if(self.passivePower != "none")
		self.passivePower = "none";
	self setClientCvar( "r_xdebug", "0" );
}

passiveInfAmmo()
{
	while(self.passivePower == "infammo" && isAlive(self))
	{
		if(self.superGun != 1)
			self setweaponslotammo("primaryb", "999");
		self setweaponslotammo("pistol", "999");
		self setweaponslotammo("primary", "999");
		self setweaponslotammo("grenade", "999");
		if(self.superGun != 1)
			self setweaponslotclipammo("primaryb", "999");
		self setweaponslotclipammo("primary", "999");
		self setweaponslotclipammo("pistol", "999");
		wait 0.5;
	}
}

passiveAimbot()
{
	aimHere = spawn ("script_origin", (0,0,0));
	aimFromHere = spawn ("script_origin", (0,0,0));
	while(self.passivePower == "aimbot")
	{
		num = 0;
		players = getEntArray( "player", "classname" );
		for(i = 0; i < players.size; i++)
		{
			aimbotDistance[i] = 30000;
			if(players[i] != self && isAlive(players[i]) && !players[i].isGhost)
				aimbotDistance[i] = distance(players[i].origin, self.origin);
		}
		aimbotAgain = true;
		for(i = 0; i < players.size && aimbotAgain; i++)
		{
			aimbotAgain = false;
			for(j = i; j < players.size; j++)
			{
				if(aimbotDistance[i] > aimbotDistance[j])
				{
					aimbotAgain = true;
				}
			}
			num = i;
		}
		aimHere linkto (players[num], "Bip01 Neck", (0,0,0), (0,0,0));
		if(!self.isGhost) // to test
			aimFromHere linkto (self, "Bip01 Neck", (0,0,0), (0,0,0));
		while(self useButtonPressed() && self.passivePower == "aimbot" && isAlive(self) && isdefined(players[num]) && isAlive(players[num]) && !players[num].isGhost && !self.isGhost)
		{
//			self setplayerangles(vectortoangles((aimHere.origin + (-2 + randomfloat(4),-2 + randomfloat(4),-2 + randomfloat(4)))  - aimFromHere.origin));
			self setplayerangles(vectortoangles(aimHere.origin - aimFromHere.origin));
			wait 0.05;
		}
		wait 0.2;
	}
}

doPassive()
{
	for(;;)
	{
		while(self.passiveTime > 0)
		{
			self.passiveTime -= 0.5;
			wait 0.5;
		}
		if(self.passivePower == "insight")
			self setClientCvar( "r_xdebug", "0" );
		if(self.passivePower != "none")
			self.passivePower = "none";
		self passiveText();
		wait 1;
	}
}

/*
########################################################################
	BONE 0 -1 "TAG_ORIGIN"
	BONE 1 0 "Bip01 Pelvis"
	BONE 2 1 "Bip01 L Thigh"
	BONE 3 1 "Bip01 R Thigh"
	BONE 4 1 "Bip01 Spine"
	BONE 5 2 "Bip01 L Calf"
	BONE 6 3 "Bip01 R Calf"
	BONE 7 4 "Bip01 Spine1"
	BONE 8 5 "Bip01 L Foot"
	BONE 9 6 "Bip01 R Foot"
	BONE 10 7 "Bip01 Spine2"
	BONE 11 10 "Bip01 L Clavicle"
	BONE 12 8 "Bip01 L Toe0"
	BONE 13 10 "Bip01 Neck"
	BONE 14 10 "Bip01 R Clavicle"
	BONE 15 9 "Bip01 R Toe0"
	BONE 16 13 "Bip01 Head"
	BONE 17 11 "Bip01 L UpperArm"
	BONE 18 14 "Bip01 R UpperArm"
	BONE 19 17 "Bip01 L Forearm"
	BONE 20 18 "Bip01 R Forearm"
	BONE 21 16 "TAG_HELMET"
	BONE 22 19 "Bip01 L Hand"
	BONE 23 20 "Bip01 R Hand"
	BONE 24 22 "Bip01 L Finger0"
	BONE 25 22 "Bip01 L Finger1"
	BONE 26 22 "Bip01 L Finger2"
	BONE 27 22 "Bip01 L Finger3"
	BONE 28 22 "Bip01 L Finger4"
	BONE 29 23 "Bip01 R Finger0"
	BONE 30 23 "Bip01 R Finger1"
	BONE 31 23 "Bip01 R Finger2"
	BONE 32 23 "Bip01 R Finger3"
	BONE 33 23 "Bip01 R Finger4"
	BONE 34 22 "TAG_WEAPON_LEFT"
	BONE 35 23 "TAG_WEAPON_RIGHT"
	BONE 36 24 "Bip01 L Finger01"
	BONE 37 25 "Bip01 L Finger11"
	BONE 38 26 "Bip01 L Finger21"
	BONE 39 27 "Bip01 L Finger31"
	BONE 40 28 "Bip01 L Finger41"
	BONE 41 29 "Bip01 R Finger01"
	BONE 42 30 "Bip01 R Finger11"
	BONE 43 31 "Bip01 R Finger21"
	BONE 44 32 "Bip01 R Finger31"
	BONE 45 33 "Bip01 R Finger41"
	BONE 46 36 "Bip01 L Finger02"
	BONE 47 37 "Bip01 L Finger12"
	BONE 48 38 "Bip01 L Finger22"
	BONE 49 39 "Bip01 L Finger32"
	BONE 50 40 "Bip01 L Finger42"
	BONE 51 41 "Bip01 R Finger02"
	BONE 52 42 "Bip01 R Finger12"
	BONE 53 43 "Bip01 R Finger22"
	BONE 54 44 "Bip01 R Finger32"
	BONE 55 45 "Bip01 R Finger42"
########################################################################
*/