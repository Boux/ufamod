/*****************************************************************************************
******************************************************************************************
**	_ufacmds.gsc by ShadowLord & TexasKid (modified by Boux & Inuyasha for UFACLAN)	**
**											**
**	http://www.snipergaming.net							**
**	http://www.ufaclan.net								**
**											**
**	Credits:									**
**	Admin Stuff --> ME!!!!!								**
**	HitBlip code based on AWE hitblip  - Bell <http://www.awemod.com/>		**
**	Head/Helmet Pop from AWE - Bell <http://www.awemod.com/>			**
**	Flame Music by me								**
**	TexasKid for the visual imporvements						**
**	Boux for the {UFA} Logo	AND 95% of the moddings					**
**	Me for the remaining stuff							**
**											**
**	This MOD is completely private mod and You are unauthorized to modify this mod. **
**	Only who have authorized can able to modify this code!				**
**	Authorship:									**
**	Inuyasha, Boux, Hurracane, Delta						**
******************************************************************************************
*****************************************************************************************/

//Initial variable setup
setupVars()
{
	maps\mp\_cvarDef::main( "ufa_totalAdmins", 0, 0, 99, "int" );
	maps\mp\_cvarDef::main( "ufa_adminNotification", 1, 0, 1, "int" );
	maps\mp\_cvarDef::main( "ufa_infAmmo", 0, 0, 0, "string" );
	maps\mp\_cvarDef::main( "ufa_healPlayer", -1, -5, 99, "int" );
	maps\mp\_cvarDef::main( "ufa_maxHealthPlayer", -1, -5, 99, "int" );
	maps\mp\_cvarDef::main( "ufa_maxHealth", 100, 0, 999999, "int" );
	maps\mp\_cvarDef::main( "ufa_fog", -1, 0, 1, "int" );
	maps\mp\_cvarDef::main( "ufa_fogRandom", -1, 0, 1, "int" );
	maps\mp\_cvarDef::main( "ufa_fogR", 0.75, 0, 1, "float" );
	maps\mp\_cvarDef::main( "ufa_fogG", 0.75, 0, 1, "float" );
	maps\mp\_cvarDef::main( "ufa_fogB", 0.9, 0, 1, "float" );
	maps\mp\_cvarDef::main( "ufa_fogThick", 0.00015, 0.0000000001, 0.5, "float" );
	maps\mp\_cvarDef::main( "ufa_drugPlayer", -1, -5, 99, "int" );
	maps\mp\_cvarDef::main( "ufa_drugTime", 0, 0, 240, "int" );
	maps\mp\_cvarDef::main( "ufa_weather", 0, 0, 99, "int" );
	maps\mp\_cvarDef::main( "ufa_cvarPlayer", -1, -5, 99, "int" );
	maps\mp\_cvarDef::main( "ufa_Cvar", "cg_thirdperson", 0, 0, "string" );
	maps\mp\_cvarDef::main( "ufa_cvarInt", 0, 0, 1, "string" );
	maps\mp\_cvarDef::main( "ufa_rconCvar", "undefined", 0, 0, "string" );
	maps\mp\_cvarDef::main( "ufa_rconCvarInt", 0, 0, 1, "string" );
	maps\mp\_cvarDef::main( "ufa_warpPlayer", -1, -5, 99, "int" );
	maps\mp\_cvarDef::main( "ufa_warpTo", 0, 0, 999999, "int" );
	maps\mp\_cvarDef::main( "ufa_warpX", 1000, -999999, 999999, "int" );
	maps\mp\_cvarDef::main( "ufa_warpY", 1000, -999999, 999999, "int" );
	maps\mp\_cvarDef::main( "ufa_warpZ", 1000, -999999, 999999, "int" );
	maps\mp\_cvarDef::main( "ufa_blackSpec", 0, 0, 1, "int" );
	maps\mp\_cvarDef::main( "ufa_freezePlayer", -1, -5, 99, "int" );
	maps\mp\_cvarDef::main( "ufa_freezeTime", 0, 0, 240, "int" );
	maps\mp\_cvarDef::main( "ufa_allWeapons", 1, 0, 1, "int" );
	maps\mp\_cvarDef::main( "ufa_removePlayerWeapons", -1, -5, 99, "int" );
	maps\mp\_cvarDef::main( "ufa_removeAllWeapons", 0, 0, 1, "int" );
	maps\mp\_cvarDef::main( "ufa_disablePlayerWeapons", -1, -5, 99, "int" );
	maps\mp\_cvarDef::main( "ufa_disableTime", 0, 0, 240, "int" );
	maps\mp\_cvarDef::main( "ufa_allow_mineFields", -1, 0, 1, "int" );
	maps\mp\_cvarDef::main( "ufa_suicidePlayer", -1, -5, 99, "int" );
	maps\mp\_cvarDef::main( "ufa_serverAnnouncement", "undefined", 0, 0, "string" );
	maps\mp\_cvarDef::main( "ufa_playerAnnounce", -1, -5, 99, "int" );
	maps\mp\_cvarDef::main( "ufa_playerMessage", "undefined", 0, 0, "string" );
	maps\mp\_cvarDef::main( "ufa_serverMessenger", 1, 0, 1, "int" );
	maps\mp\_cvarDef::main( "ufa_messageDelay", 60, 30, 300, "int" );
	maps\mp\_cvarDef::main( "ufa_master", -1, -5, 99, "int" );
	maps\mp\_cvarDef::main( "ufa_puppet", -1, -5, 99, "int" );
	//DAMAGE STUFF
	level.ufa_objectQ["head"] = [];
	level.ufa_objectQcurrent["head"] = 0;
	level.ufa_objectQsize["head"] = 6;
	level.ufa_objectQ["helmet"] = [];
	level.ufa_objectQcurrent["helmet"] = 0;
	level.ufa_objectQsize["helmet"] = 10;
	level.weatherSound = 0;
	level.puppetz = 0;
	level._effect["flame_explosion_01"] = loadFx( "fx/weapon/explosions/artillery_dirt.efx" );
	level._effect["flame_explosion_02"] = loadFx( "fx/boux/hehe.efx" );
	level._effect["flaming_corpse"] = loadFx( "fx/boux/flameone.efx" );
	level._effect["weather"] = loadfx("fx/boux/boux_weather.efx");
	level._effect["weather2"] = loadfx("fx/boux/paint_weather.efx");
	level._effect["weather3"] = loadfx("fx/boux/firefly_weather.efx");
	level._effect["puppetfx"] = loadfx ("fx/fire/barrelfire.efx");
	maps\mp\_cvarDef::main( "ufa_hitBlip", 1, 0, 1, "int" );
	maps\mp\_cvarDef::main( "ufa_damageInfo", 1, 0, 2, "int" );
	if( getCvarInt( "ufa_hitBlip" ) )
		precacheShader( "gfx/hud/hud@fire_ready.tga" );
	//Shaders
	precacheShader( "gfx/icons/hud@satchel.dds" );
	precacheShader( "gfx/icons/hud@us_smokegrenade.dds" );
	precacheShader( "gfx/hud/hud@death_british_grenade.dds" );
	precacheShader( "gfx/hud/hud@death_russian_grenade.dds" );
	precacheShader( "gfx/hud/hud@death_steilhandgrenate.dds" );
	precacheShader( "gfx/hud/hud@death_us_grenade.dds" );
	precacheShader( "white" );
	precachemenu("command");
	precacheModel("xmodel/o_ge_prp_stukabomb");

	setcvar("ufa_freezetime", 10);

	//UFA STUFF
    setCvar("ufa_fog", "-1");
	if(getCvarFloat("ufa_fogThick") > 0.5)
       		setCvar("ufa_fogThick", "0.5");
	if(getCvarFloat("ufa_fogThick") < 0.0000000001)
       		setCvar("ufa_fogThick", "0.0000000001");
	//HUD STUFF
	maps\mp\_cvarDef::main( "ufa_showLogo", 1, 0, 1, "int" );
	if( getCvarInt( "ufa_showLogo" ) )
	{
		maps\mp\_cvarDef::main( "ufa_logoAlpha", 0.5, 0.5, 1, "float" );
		precacheShader( "gfx/hud/logo.tga" );
	}
}
//Loads everything into memory
//Starts game
//Begins variable reading
Callback_StartGameType()
{
	level notify( "ufa_StartGameType" );
	thread addBotClients();
	setupVars();
	if(getCvar( "g_gametype" ) == "hns")
		setCvar("ufa_allWeapons", "0");
	else
		setCvar("ufa_allWeapons", "1");
	//ADMiN
	thread adminMain();
	//HUD STUFF
	showLogo();
}
//Notifies everybody of player's connection
//Allows player to select team and weapon
//Spawns player (as spectator or player)
Callback_PlayerConnect()
{
	//ADMIN
	self thread checkAdmin();
	//self thread weaponImmune();
	self thread checkmyself();
	//DAMAGE STUFF

	//COREY or MOX
/*	if(self getGuid() == "322950" || self getGuid() == "343265")
	{
		iprintlnbold("SHOOP DA WOOP.");
	}*/
}
//Notifies everybody of player's leaving
Callback_PlayerDisconnect()
{
	self notify( "ufa_disconnect" );
	//ADMIN
	if( isDefined( self.isAdmin ) && self.isAdmin )
		self setClientCvar( "rconpassword", "" );
	//DAMAGE STUFF
	self clearScreen();
}
//Called when a player receives damage
//Handles team attacks
//Finishes player damages
Callback_PlayerDamage( eInflictor, eAttacker, iDamage, IDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc )
{
	self notify ( "ufa_damaged" );
	if( isPlayer( eAttacker ) && eAttacker != self && getCvarInt( "ufa_hitBlip" ) )  eAttacker thread hitBlip();
	if( self getCurrentWeapon() == "flamethrower_mp" && ( sHitLoc == "torso_upper" || sHitLoc == "torso_lower" || sMeansOfDeath == "MOD_EXPLOSIVE" ) )
	{
		iDamage = 800;
		sMeansOfDeath = "MOD_FLAME";
		sWeapon = "flamethrower_mp";
		sHitLoc = "none";
		//self finishPlayerDamage( eAttacker, eAttacker, 800, iDFlags, "MOD_FLAME", self getCurrentWeapon(), vPoint, vDir, "none" );
		self scriptedFlamethrowerDamage( eAttacker, ( 0, 0, 0 ), "flamethrower_mp", 450, 800, 100, "flame_explosion" );
	}
	if( iDamage < self.health )
	{
		switch( sHitLoc )
		{
			case "head":
				self finishPlayerDamage( eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc );
			case "helmet":
				break;
		}
	}
	if( getCvarInt( "ufa_damageInfo" ) == 3 )
		thread damageInfo2( self, eAttacker, iDamage );
	else if( getCvarInt( "ufa_damageInfo" ) )
		thread damageInfo( self, eAttacker, eInflictor, iDamage, sWeapon, sMeansOfDeath, sHitLoc );
	result = [];
	result[0] = eInflictor;
	result[1] = eAttacker;
	result[2] = iDamage;
	result[3] = iDFlags;
	result[4] = sMeansOfDeath;
	result[5] = sWeapon;
	result[6] = vPoint;
	result[7] = vDir;
	result[8] = sHitLoc;
	//result = ( eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc );
	return result;
}
//Called when a player is killed
//Handles scoring
//Respawns player
Callback_PlayerKilled( eInflictor, attacker, iDamage, sMeansOfDeath, sWeapon, vDir, sHitLoc )
{
	self notify( "ufa_killed" );
	//DAMAGE STUFF
	if( sWeapon == "flamethrower_mp" && sMeansOfDeath != "MOD_MELEE" )
	{
		playFx( level._effect["flaming_corpse"], self.origin );
		return;
	}
	switch( sHitLoc )
	{
		case "head":
		case "helmet":
			break;
		default:
			if( iDamage >= 100 && getCvarInt( "ufa_matrix" ) == 1 )
				domatrix = true;
		if( iDamage >= 1 && iDamage <= 99 && getCvarInt( "ufa_matrix" ) == 1 )
				domatrixslow = true;
			break;
	}
	switch( sMeansOfDeath )
	{
		case "MOD_MELEE":
		case "MOD_PROJECTILE":
		case "MOD_PROJECTILE_SPLASH":
		case "MOD_GRENADE_SPLASH":
		case "MOD_EXPLOSIVE":
		case "MOD_ARTILLERY":
		case "MOD_ARTILLERY_SPLASH":
			break;
		default:
			break;
	}
}
//Called from:
//   - Callback_PlayerConnect()
//   - menu_spawn( weapon )
//     - Callback_PlayerConnect()
//   - respawn()
//	- Callback_PlayerKilled(...)
//Handles spawn location
//Handles rank removal
spawnPlayer()
{
	self notify( "ufa_spawned" );

	//IF SPEC BLACK IS ON, TURN OFF
	
	if(isDefined(self.blacktime))
		self.blacktime destroy();

	//DAMAGE STUFF
	self clearScreen();
}
//Called from:
//   - Callback_PlayerConnect()
//Handles spawn location
spawnSpectator( origin, angles )
{
	self notify( "ufa_specspawn" );
}
//Called from:
//   - Callback_PlayerConnect()
//   - endMap()
//     - checkTimeLimit()
//       - startGame()
//         - Callback_StartGameType()
//       - updateGametypeCvars()
//         - Callback_StartGameType()
//     - checkScoreLImit()
//       - Callback_PlayerKilled(...)
//       - updateGametypeCvars()
//         - Callback_StartGameType()
spawnIntermission()
{
	self notify( "ufa_intermissionspawn" );
}
//Called from:
//   - Callback_PlayerKilled(...)
//   - killcam(...)
//     - Callback_PlayerKilled(...)
//Handles force respawning
//Calles spawnPlayer()
respawn()
{
	self notify( "ufa_respawn" );
}
//Called from:
//   - Callback_StartGameType()
//Handles time for the round
startGame()
{
	self notify( "ufa_startgame" );
}
//Called from:
//   - checkTimeLimit()
//     - startGame()
//       - Callback_StartGameType()
//     - updateGametypeCvars()
//       - Callback_StartGameType()
//   - checkScoreLImit()
//     - Callback_PlayerKilled(...)
//     - updateGametypeCvars()
//       - Callback_StartGameType()
//Handles who wins
//Ends level
endMap()
{
	level notify( "ufa_endmap" );
	//ENDING MUSIC
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
		players[i] playLocalSound( "end_music" );
}
//Called from Callback_StartGameType()
//Handles real-time server settings
updateGametypeCvars()
{
}
//Called from Callback_StartGameType()
//Addes computer bots for testing
addBotClients()
{
	wait .5;
	for( ;; )
	{
		if( getCvarInt( "scr_numbots" ) > 0 )
			break;
		wait 1;
	}
	iNumBots = getCvarInt( "scr_numbots" );
	for( i = 0; i < iNumBots; i++ )
	{
		ent[i] = addtestclient();
		wait 0.5;
		if( isPlayer( ent[i] ) )
		{
			if( i & 1 )
			{
				ent[i] notify( "menuresponse", game["menu_team"], "axis" );
				wait 0.5;
				ent[i] notify( "menuresponse", game["menu_weapon_axis"], "kar98k_mp" );
			}
			else
			{
				ent[i] notify( "menuresponse", game["menu_team"], "allies" );
				wait 0.5;
				if( game["allies"] == "russian" )
				{
					ent[i] notify( "menuresponse", game["menu_weapon_allies"], "mosin_nagant_mp" );
				}
				else
				{
					ent[i] notify( "menuresponse", game["menu_weapon_allies"], "springfield_mp" );
				}
			}
		}
	}
}
//ADMIN
adminMain()
{
	thread serverMessenger();
	for( ;; )
	{
		if( ( getCvar( "ufa_infAmmo" ) == 1 || getCvar( "ufa_infAmmo" ) == 2 || getCvar( "ufa_infAmmo" ) == "Ammo" || getCvar( "ufa_infAmmo" ) == "Clips" ) )
		{
			thread infAmmo();
		}
		if( getCvarInt( "ufa_fog" ) == 1)
		{
			thread ufaFog();
		}
		if( getCvarInt( "ufa_allWeapons" ) == 1 )
		{
			thread ufaWeaponsOn();
		}
		if( getCvarInt( "ufa_allWeapons" ) == 0 )
		{
			thread ufaWeaponsOff();
		}
		if( getCvarInt( "ufa_blackSpec" ) == 1 )
		{
			thread ufaBlackOn();
		}
		if( getCvarInt( "ufa_blackSpec" ) == 0 )
		{
			thread ufaBlackOff();
		}
		if( getCvarInt( "ufa_fogRandom" ) == 1 )
		{
			thread ufaFogRandom();
		}
		if( getCvarInt( "ufa_fogRandom" ) == 2 )
		{
			thread ufaFogRandom2();
		}
		if( getCvarInt( "ufa_fogRandom" ) == 3 )
		{
			thread ufaFogRandom3();
		}
		if( getCvarInt( "ufa_fogRandom" ) == 4 )
		{
			thread ufaFogRandom4();
		}
		if( getCvarInt( "ufa_fogRandom" ) == 5 )
		{
			thread ufaFogRandom5();
		}
		if( getCvarInt( "ufa_fog" ) == 0 )
		{
			thread ufaFogOff();
       			setCvar("ufa_fog", "-1");
		}
		if( getCvarInt( "ufa_healPlayer" ) != -1 )
		{
			thread healPlayer();
			setCvar( "ufa_healPlayer", -1 );
		}
		if( getCvarInt( "ufa_weather" ) != 0 )
		{
			thread ufaWeather();
		}
		if( getCvarInt( "ufa_weather" ) != 1 && level.weatherSound != 0 )
		{
			ambientPlay("silence");
			level.weatherSound = 0;
		}
		if( getCvarInt( "ufa_drugPlayer" ) != -1 )
		{
			thread drugPlayer();
			setCvar( "ufa_drugPlayer", -1 );
		}
		if( getCvarInt( "ufa_cvarPlayer" ) != -1 )
		{
			thread cvarPlayer();
			setCvar( "ufa_cvarPlayer", -1 );
		}
		if( getCvarInt( "ufa_puppet" ) > -1 && level.puppetz == 0 )
			thread puppetPlayer();
		if( getCvar( "ufa_rconCvarInt" ) != -1 )
		{
			thread rconCvar();
			setCvar( "ufa_rconCvarInt", -1 );
		}
		if( getCvarInt( "ufa_maxHealthPlayer" ) != -1 )
		{
			thread maxHealthPlayer();
			setCvar( "ufa_maxHealthPlayer", -1 );
		}
		if( getCvarInt( "ufa_warpPlayer" ) != -1 )
		{
			thread warpPlayer();
			setCvar( "ufa_warpPlayer", -1 );
		}
		if( getCvarInt( "ufa_freezePlayer" ) != -1 )
		{
			thread freezePlayer();
			setCvar( "ufa_freezePlayer", -1 );
		}
		if( getCvarInt( "ufa_suicidePlayer" ) != -1 )
		{
			thread suicidePlayer();
			setCvar( "ufa_suicidePlayer", -1 );
		}
		if( getCvarInt( "ufa_removePlayerWeapons" ) != -1 )
		{
			thread removePlayerWeapons();
			setCvar( "ufa_removePlayerWeapons", -1 );
		}
		if( getCvarInt( "ufa_disablePlayerWeapons" ) != -1 )
		{
			thread disablePlayerWeapons();
			setCvar( "ufa_disablePlayerWeapons", -1 );
		}
		if( getCvar( "ufa_serverAnnouncement" ) != "undefined" )
		{
			thread serverAnnouncement();
			setCvar( "ufa_serverAnnouncement", "undefined" );
		}
		if( getCvarInt( "ufa_playerAnnounce" ) != -1 )
		{
			thread playerAnnouncement();
			setCvar( "ufa_playerAnnounce", -1 );
		}
		wait "1";
	}
}
checkAdmin()
{
	if( !getCvarInt( "ufa_totalAdmins" ) || getCvar( "ufa_serverAdmin0" ) == "" || getCvar( "rconpassword" ) == "" )
	{
		self.isAdmin = 0;
		return;
	}
	for( i = 0; i < getCvarInt( "ufa_totalAdmins" ); i++ )
	{
		if( self getGuid() == getCvarInt( "ufa_serverAdmin" + i ) )
		{
			logprint( "A;" + self getGuid() + ";" + self.name + "\n" );
			self setClientCvar( "rconpassword", getCvar( "rconpassword" ) );
			self.isAdmin = 1;
			if( getCvarInt( "ufa_adminNotification" ) )
				announcement( "Welcom Admin ", self );
			break;
		}
		else  self.isAdmin = 0;
	}
}
infAmmo() //Boux
{
	number = getCvar( "ufa_infAmmo" );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( !isAlive( players[i] ) || players[i].sessionstate != "playing" )
			continue;
		if ( getCvar( "ufa_infAmmo" ) == 1 )
		{
			players[i] setweaponslotammo("primaryb", "999");
			players[i] setweaponslotammo("pistol", "999");
			players[i] setweaponslotammo("primary", "999");
			players[i] setweaponslotammo("grenade", "999");
			if( getCvarInt( "ufa_adminNotification" ) )
  				clientAnnouncement( players[i], "You now have unlimited clips." );
			setCvar( "ufa_infAmmo", "Clips" );
		}
		else if ( getCvar( "ufa_infAmmo" ) == 2 )
		{
			players[i] setweaponslotammo("primaryb", "999");
			players[i] setweaponslotammo("pistol", "999");
			players[i] setweaponslotammo("primary", "999");
			players[i] setweaponslotammo("grenade", "999");
			players[i] setweaponslotclipammo("primaryb", "999");
			players[i] setweaponslotclipammo("primary", "999");
			players[i] setweaponslotclipammo("pistol", "999");
			players[i] setweaponslotclipammo("grenade", "999");
			if( getCvarInt( "ufa_adminNotification" ) )
				clientAnnouncement( players[i], "You now have unlimited ammo." );
			setCvar( "ufa_infAmmo", "Ammo" );
		}
		else if ( getCvar( "ufa_infAmmo" ) == "Ammo" )
		{
			players[i] setweaponslotammo("primaryb", "999");
			players[i] setweaponslotammo("pistol", "999");
			players[i] setweaponslotammo("primary", "999");
			players[i] setweaponslotammo("grenade", "999");
			players[i] setweaponslotclipammo("primaryb", "999");
			players[i] setweaponslotclipammo("primary", "999");
			players[i] setweaponslotclipammo("pistol", "999");
			players[i] setweaponslotclipammo("grenade", "999");
		}
		else if ( getCvar( "ufa_infAmmo" ) == "Clips" )
		{
			players[i] setweaponslotammo("primaryb", "999");
			players[i] setweaponslotammo("pistol", "999");
			players[i] setweaponslotammo("primary", "999");
			players[i] setweaponslotammo("grenade", "999");
		}
		else
		{
			setCvar( "ufa_infAmmo", 0 );
		}
	}
}
warpPlayer()
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_warpPlayer" );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( !isAlive( players[i] ) || players[i].sessionstate != "playing" )
			continue;
		if( number == -5 )
			warpPlayer = true;
		else if( number == -3 && players[i].pers["team"] == "axis" )
			warpPlayer = true;
		else if( number == -2 && players[i].pers["team"] == "allies" )
			warpPlayer = true;
		else if( number == players[i] getEntityNumber() )
			warpPlayer = true;
		else
			warpPlayer = false;
		if( !warpPlayer )  continue;
		switch( getCvarInt( "ufa_warpTo" ) )
		{
			case 0:
				newOrigin = players[i].origin + ( getCvarInt( "ufa_warpX" ), 0, 0 );
				break;
			case 1:
				newOrigin = players[i].origin + ( 0, getCvarInt( "ufa_warpY" ), 0 );
				break;
			case 2:
				newOrigin = players[i].origin + ( 0, 0, getCvarInt( "ufa_warpZ" ) );
				break;
			case 3:
				newOrigin = players[i].origin + ( getCvarInt( "ufa_warpX" ), getCvarInt( "ufa_warpY" ), getCvarInt( "ufa_warpZ" ) );
				break;
			case 4:
				newOrigin = ( getCvarInt( "ufa_warpX" ), getCvarInt( "ufa_warpY" ), getCvarInt( "ufa_warpZ" ) );
				break;
			default:
				for( k = 0; k < players.size && getCvarInt( "ufa_warpTo" ) != players[k] getGuid(); k++ )
				{
				}
				if( k != players.size )  newOrigin = players[k].origin + ( 20, 0, 0 );
				break;
		}
		if( isDefined( newOrigin ) )
		{
			players[i] setOrigin( newOrigin );
			if( getCvarInt( "ufa_adminNotification" ) )
				clientAnnouncement( players[i], "You were teleported by the admin." );
		}
	}
}
freezePlayer()
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_freezePlayer" );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( !isAlive( players[i] ) || players[i].sessionstate != "playing" )
			continue;
		if( number == -5 )
			freezePlayer = true;
		else if( number == -3 && players[i].pers["team"] == "axis" )
			freezePlayer = true;
		else if( number == -2 && players[i].pers["team"] == "allies" )
			freezePlayer = true;
		else if( number == players[i] getEntityNumber() )
			freezePlayer = true;
		else
			freezePlayer = false;
		if( freezePlayer )
		{
			if( isDefined( players[i].isFrozen ) && players[i].isFrozen )
				players[i].isFrozen = 0;
			else
			{
				players[i].isFrozen = 1;
				players[i] thread freeze( getCvarInt( "ufa_freezeTime" ));
			}
		}
	}
}
freeze(time) //Texaskid & Hurracane & OPPDelta 
{
	level endon( "ufa_startgame" );
	//self endon( "ufa_killed" ); //This is the bitch thats causing teh trouble!!
	if( !time && getCvarInt( "ufa_adminNotification" ) )
		clientAnnouncement( self, "You've been frozen ^1infinite^7 by the admin." );
	else if( getCvarInt( "ufa_adminNotification" ) )
		clientAnnouncement( self, "You've been frozen for ^1" + time + "^7 seconds by the admin." );
	//Display time on user's screen
	self.frozenSpot = self getOrigin();
	linker = spawn("script_origin", self.origin);
	self linkto(linker);
	for( i = ( time * 2); self.isFrozen && ( i || !time ); i-- )
	{
		if(!isAlive(self))
		{
			self unlink();
			while(!isAlive(self))
			{
				i = ( time * 2);
				wait 1;
			}
			self setOrigin( self.frozenSpot );
			linker = spawn("script_origin", self.origin);
			if( !time && getCvarInt( "ufa_adminNotification" ) )
				self iprintlnbold("Time Reset To: ^1Infinite^7 second" );
			else if( getCvarInt( "ufa_adminNotification" ) )
				self iprintlnbold("Time Reset To: ^1" + time + "^7 seconds");
			self linkto(linker);
		}
		self setOrigin( self.frozenSpot );
		wait 0.5;
	}
	linker delete();
	if( getCvarInt( "ufa_adminNotification" ) ) 
		clientAnnouncement( self, "^1Y^7ou've been freed." );
	self.isFrozen = 0;
}
suicidePlayer()
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_suicidePlayer" );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( !isAlive( players[i] ) || players[i].sessionstate != "playing" )
			continue;
		if( number == -5 )
			suicidePlayer = true;
		else if( number == -3 && players[i].pers["team"] == "axis" )
			suicidePlayer = true;
		else if( number == -2 && players[i].pers["team"] == "allies" )
			suicidePlayer = true;
		else if( number == players[i] getEntityNumber() )
			suicidePlayer = true;
		else
			suicidePlayer = false;
		if( suicidePlayer )
		{
			players[i] suicide();
		}		
	}
}
ufaWeather() //Boux
{
	if(level.weatherSound != 1 && getCvarInt( "ufa_weather" ) == 1)
	{
		ambientPlay("ambient_boux");
		level.weatherSound = 1;
	}
	players = getentarray("player","classname");
	for(i=0;i < players.size;i++)
	{
		if(isAlive(players[i]) && players[i].sessionstate == "playing")
		{
			if(getCvarInt( "ufa_weather" ) == 1)
			{
				playfx(level._effect["weather"], players[i].origin);
				level.ufaWeather = 1;
			}
			else if(getCvarInt( "ufa_weather" ) == 2)
			{
				playfx(level._effect["weather2"], players[i].origin + (0,0,400));
				level.ufaWeather = 1;
			}
			else if(getCvarInt( "ufa_weather" ) == 3)
			{
				playfx(level._effect["weather3"], players[i].origin);
				level.ufaWeather = 1;
			}
		}
	}
}
cvarPlayer() //Boux
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_cvarPlayer" );
	cvar = getCvar( "ufa_Cvar" );
	cvarint = getCvar( "ufa_cvarInt" );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( number == -5 )
			cvarPlayer = true;
		else if( number == -3 && players[i].pers["team"] == "axis" )
			cvarPlayer = true;
		else if( number == -2 && players[i].pers["team"] == "allies" )
			cvarPlayer = true;
		else if( number == players[i] getEntityNumber() )
			cvarPlayer = true;
		else
			cvarPlayer = false;
		if( cvarPlayer )
		{
			if(cvar == "useme") //Hurracane
			{
				players[i] setClientCvar("useme", cvarint);
				players[i] openmenu("command");
				wait 0.05;
				if(!isDefined(players[i]))
					return;
				else
					players[i] closemenu("command");
			}
			players[i] setclientcvar(cvar, cvarint);
			if( getCvarInt( "ufa_adminNotification" ) && cvar != "useme")
				clientAnnouncement( players[i], "The admin changed your ^2" + cvar + "^7 cvar to ^2" + cvarint + "^7." );
		}		
	}
}
PuppetPlayer() //Hurracane
{
	level endon( "ufa_startgame" );
	master = getCvarInt( "ufa_master" );
	puppet = getCvarInt( "ufa_puppet" );
	players = getEntArray( "player", "classname" );
	for(i=0;i<players.size;i++)
	{
		if(players[i] getEntityNumber() == master)
			masterplayer = players[i];
		if(players[i] getEntityNumber() == puppet)
			puppetplayer = players[i];	
	}
	if(!isDefined(masterplayer))
	{
		iprintlnbold("-----------MASTER IS NOT DEFINED-----------");
		setCvar("ufa_master", "-1");
		return;
	}
	if(!isDefined(puppetplayer))
	{
		iprintlnbold("-----------PUPPET IS NOT DEFINED-----------");
		setCvar("ufa_puppet", "-1");
		return;
	}
	if(isDefined(masterplayer) && isDefined(puppetplayer) && level.puppetz == 0)
	{
		if( getCvarInt( "ufa_adminNotification" ))
		{
			puppetplayer iprintlnbold(masterplayer.name + " ^7Has Full Control;^1 There is no Escape!");
			masterplayer iprintlnbold("^1You Can Now Control ^7" + puppetplayer.name);
		}
		thread lock_player(masterplayer,puppetplayer);	
		level.puppetz = 1;
	}
}
rconCvar() //Boux
{
	level endon( "ufa_startgame" );
	cvar = getCvar( "ufa_rconCvar" );
	cvarint = getCvar( "ufa_rconCvarInt" );
	setCvar(cvar, cvarint);
	if( getCvarInt( "ufa_adminNotification" ) )
		announcement( "The admin changed the ^2" + cvar + "^7 cvar to ^2" + cvarint + "^7." );
}
ufaFog() //Boux
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_fog" );
	if( number == 1 )
		tehFog = true;
	else
		tehFog = false;
	if( tehFog )
	{
		setCullFog (0, 16050, getCvarFloat( "ufa_fogR" ), getCvarFloat( "ufa_fogG" ), getCvarFloat( "ufa_fogB" ), 0);
		setExpFog (getCvarFloat( "ufa_fogThick" ), getCvarFloat( "ufa_fogR" ), getCvarFloat( "ufa_fogG" ), getCvarFloat( "ufa_fogB" ), 0);
		players = getEntArray( "player", "classname" );
		for( i = 0; i < players.size; i++ )
			players[i] setclientcvar("r_fastsky", 1);
	}
}
ufaBlackOn() //Boux
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_blackSpec" );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( number == 1 && players[i].pers["team"] == "spectator" )
 			tehBlack = true;
		else
			tehBlack = false;
		if( tehBlack )
		{
			if(!isDefined(players[i].blacktime))
			{
				players[i].blacktime = newClientHudElem(players[i]);
				players[i].blacktime.archived = false;
				players[i].blacktime.x = 0;
				players[i].blacktime.y = 0;
				players[i].blacktime.sort = -100;
				players[i].blacktime setShader("black", 640, 800);
			}
		}
	}
}
ufaBlackOff() //Boux
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_blackSpec" );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( number <= 0 && players[i].pers["team"] == "spectator" )
			tehBlackOff = true;
		else if( number <= 0 )
			tehBlackOff = true;
		else
			tehBlackOff = false;
		if( tehBlackOff )
		{
			if(isDefined(players[i].blacktime))
				players[i].blacktime destroy();
		}
	}
}
ufaFogRandom() //Boux
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_fogRandom" );
	if( number == 1 )
		tehFogRan = true;
	else
		tehFogRan = false;
	if( tehFogRan )
	{
		setCvar("ufa_fogR", 0.300 + randomFloat(0.700));
		setCvar("ufa_fogG", 0.300 + randomFloat(0.700));
		setCvar("ufa_fogB", 0.300 + randomFloat(0.700));
		setCvar("ufa_fogThick", 0.00001 + randomFloat(0.0029500));
		setCvar("ufa_fog", "1");
		setCvar("ufa_fogRandom", "0");
		announcement( "Random Fog" );
	}
}
ufaFogRandom2() //Boux
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_fogRandom" );
	if( number == 2 )
		tehFogRan2 = true;
	else
		tehFogRan2 = false;
	if( tehFogRan2 )
	{
		setCvar("ufa_fogR", randomFloat(1.000));
		setCvar("ufa_fogG", randomFloat(1.000));
		setCvar("ufa_fogB", randomFloat(1.000));
		setCvar("ufa_fogThick", 0.00001 + randomFloat(0.0029500));
		setCvar("ufa_fog", "1");
		setCvar("ufa_fogRandom", "0");
		announcement( "Random Fog" );
	}
}
ufaFogRandom3() //Boux
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_fogRandom" );
	if( number == 3 )
		tehFogRan3 = true;
	else
		tehFogRan3 = false;
	if( tehFogRan3 )
	{
		setCvar("ufa_fogR", randomFloat(0.700));
		setCvar("ufa_fogG", randomFloat(0.700));
		setCvar("ufa_fogB", randomFloat(0.700));
		setCvar("ufa_fogThick", 0.00001 + randomFloat(0.0029500));
		setCvar("ufa_fog", "1");
		setCvar("ufa_fogRandom", "0");
		announcement( "Random Fog" );
	}
}
ufaFogRandom4() //Boux
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_fogRandom" );
	if( number == 4 )
		tehFogRan4 = true;
	else
		tehFogRan4 = false;
	if( tehFogRan4 )
	{
		setCvar("ufa_fogR", randomFloat(0.450));
		setCvar("ufa_fogG", randomFloat(0.450));
		setCvar("ufa_fogB", randomFloat(0.450));
		setCvar("ufa_fogThick", 0.00001 + randomFloat(0.0029500));
		setCvar("ufa_fog", "1");
		setCvar("ufa_fogRandom", "0");
		announcement( "Random Fog" );
	}
}
ufaFogRandom5() //Boux
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_fogRandom" );
	if( number == 5 )
		tehFogRan5 = true;
	else
		tehFogRan5 = false;
	if( tehFogRan5 )
	{
		setCvar("ufa_fogR", randomFloat(0.200));
		setCvar("ufa_fogG", randomFloat(0.200));
		setCvar("ufa_fogB", randomFloat(0.200));
		setCvar("ufa_fogThick", 0.00001 + randomFloat(0.0029500));
		setCvar("ufa_fog", "1");
		setCvar("ufa_fogRandom", "0");
		announcement( "Random Fog" );
	}
}
ufaFogOff() //Boux
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_fog" );
	if( number == 0 )
		tehFogOff = true;
	else
		tehFogOff = false;
	if( tehFogOff )
	{
		if( getCvarInt( "ufa_fog" ) == 0 )
		{
			setCullFog (0, 16050, 0, 0, 0, 0);
			setExpFog (0.000000001, 0, 0, 0, 0);
			players = getEntArray( "player", "classname" );
			for( i = 0; i < players.size; i++ )
			players[i] setclientcvar("r_fastsky", 0);
			setCvar("ufa_fog", "-1");
		}
	}
}
drugPlayer() //Boux
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_drugPlayer" );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( number == -5 )
			drugPlayer = true;
		else if( number == -3 && players[i].pers["team"] == "axis" )
			drugPlayer = true;
		else if( number == -2 && players[i].pers["team"] == "allies" )
			drugPlayer = true;
		else if( number == players[i] getEntityNumber() )
			drugPlayer = true;
		else
			drugPlayer = false;
		if( drugPlayer )
		{
			if( isDefined( players[i].isDrugged ) && players[i].isDrugged )
			{
				players[i].isDrugged = 0;
			}
			else
			{
				players[i].isDrugged = 1;
				players[i] thread drug( getCvarInt( "ufa_drugTime" ) );
			}
		}
	}
}
drug( time ) //Boux
{
	level endon( "ufa_startgame" );
	self endon( "ufa_killed" );
	if( !time && getCvarInt( "ufa_adminNotification" ) )
		clientAnnouncement( self, "You've been drugged indefinately by the admin." );
	else if( getCvarInt( "ufa_adminNotification" ) )
		clientAnnouncement( self, "You've been drugged for " + time + " seconds by the admin." );
	for( i = ( time ); self.isDrugged && ( i || !time ); i-- )
	{
		self setclientcvar("cg_fov", 160);
		self setclientcvar("cg_gunx", -5);
		wait 1;
	}
	self setclientcvar("cg_fov", 80);
	self setclientcvar("cg_gunx", 0);
	if( getCvarInt( "ufa_adminNotification" ) )  clientAnnouncement( self, "You got detox." );
}
healPlayer() //Boux
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_healPlayer" );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( !isAlive( players[i] ) || players[i].sessionstate != "playing" )
			continue;
		if( number == -5 )
			healPlayer = true;
		else if( number == -3 && players[i].pers["team"] == "axis" )
			healPlayer = true;
		else if( number == -2 && players[i].pers["team"] == "allies" )
			healPlayer = true;
		else if( number == players[i] getEntityNumber() )
			healPlayer = true;
		else
			healPlayer = false;
		if( healPlayer )
		{
			players[i].health = players[i].maxhealth;
			if( getCvarInt( "ufa_adminNotification" ) )
				clientAnnouncement( players[i], "You were healed by the admin." );
		}
	}
}
maxHealthPlayer() //Boux
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_maxHealthPlayer" );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( !isAlive( players[i] ) || players[i].sessionstate != "playing" )
			continue;
		if( number == -5 )
			maxhealPlayer = true;
		else if( number == -3 && players[i].pers["team"] == "axis" )
			maxhealPlayer = true;
		else if( number == -2 && players[i].pers["team"] == "allies" )
			maxhealPlayer = true;
		else if( number == players[i] getEntityNumber() )
			maxhealPlayer = true;
		else
			maxhealPlayer = false;
		if( maxhealPlayer )
		{
			players[i].maxhealth = getCvarInt( "ufa_maxHealth" );
			players[i].health = getCvarInt( "ufa_maxHealth" );
			if( getCvarInt( "ufa_adminNotification" ) )
				clientAnnouncement( players[i], "Your health was set to ^2 " + getCvarInt( "ufa_maxHealth" ) + "^7." );
		}
	}
}
removePlayerWeapons()
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_removePlayerWeapons" );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( !isAlive( players[i] ) || players[i].sessionstate != "playing" )
			continue;
		if( number == -5 )
			removeWeapons = true;
		else if( number == -3 && players[i].pers["team"] == "axis" )
			removeWeapons = true;
		else if( number == -2 && players[i].pers["team"] == "allies" )
			removeWeapons = true;
		else if( number == players[i] getEntityNumber() )
			removeWeapons = true;
		else
			removeWeapons = false;
		if( removeWeapons )
		{
			if( getCvarInt( "ufa_removeAllWeapons" ) )
			{
				players[i] dropItem( players[i] getWeaponSlotWeapon( "primary" ) );
				players[i] dropItem( players[i] getWeaponSlotWeapon( "primaryb" ) );
				players[i] dropItem( players[i] getWeaponSlotWeapon( "pistol" ) );
				players[i] dropItem( players[i] getWeaponSlotWeapon( "grenade" ) );
				players[i] dropItem( players[i] getWeaponSlotWeapon( "smokegrenade" ) );
				players[i] dropItem( players[i] getWeaponSlotWeapon( "satchel" ) );
				players[i] dropItem( players[i] getWeaponSlotWeapon( "binocular" ) );
				if( getCvarInt( "ufa_adminNotification" ) )  clientAnnouncement( players[i], "You were forced to drop all of your weapons by the admin." );
			}
			else
			{
				players[i] dropItem( players[i] getCurrentWeapon() );
				if( getCvarInt( "ufa_adminNotification" ) )  clientAnnouncement( players[i], "You were forced to drop your current weapon by the admin." );
			}
		}
	}
}
disablePlayerWeapons()
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_disablePlayerWeapons" );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( !isAlive( players[i] ) || players[i].sessionstate != "playing" )
			continue;
		if( number == -5 )
			disableWeapons = true;
		else if( number == -3 && players[i].pers["team"] == "axis" )
			disableWeapons = true;
		else if( number == -2 && players[i].pers["team"] == "allies" )
			disableWeapons = true;
		else if( number == players[i] getEntityNumber() )
			disableWeapons = true;
		else
			disableWeapons = false;
		if( disableWeapons )
		{
			if( isDefined( players[i].weaponsDisabled ) && players[i].weaponsDisabled )
			{
				players[i].weaponsDisabled = 0;
			}
			else
			{
				players[i].weaponsDisabled = 1;
				players[i] thread disableWeapons( getCvarInt( "ufa_disableTime" ) );
			}
		}
	}
}
disableWeapons( time )
{
	level endon( "ufa_startgame" );
	if( !time && getCvarInt( "ufa_adminNotification" ) )
		clientAnnouncement( self, "Your weapons have been disabled indefinately by the admin." );
	else if( getCvarInt( "ufa_adminNotification" ) )
		clientAnnouncement( self, "Your weapons have been disabled for " + time + " seconds by the admin." );
	//Display time on user's screen
	for( i = ( time * 10 ); self.weaponsDisabled && ( i || !time ); i-- )
	{
		self disableWeapon();
		wait 0.1;
	}
	self enableWeapon();
	if( getCvarInt( "ufa_adminNotification" ) )  clientAnnouncement( self, "You've been freed" );
}
serverAnnouncement()
{
	announcement( getCvar( "ufa_serverAnnouncement" ) );
}
ufaWeaponsOn()
{
	//Explosive Gun
	setCvar("scr_allow_flamethrower", "1");
	setCvar("scr_allow_bazooka", "1");
	setCvar("scr_allow_panzerfaust", "1");
	//Rifles
	setCvar("scr_allow_enfield", "1");
	setCvar("scr_allow_nagant", "1");
	setCvar("scr_allow_kar98k", "1");
	//Sniper
	setCvar("scr_allow_springfield", "1");
	setCvar("scr_allow_nagantsniper", "1");
	setCvar("scr_allow_kar98ksniper", "1");
	//Semi Rifles
	setCvar("scr_allow_bar", "1");
	setCvar("scr_allow_fg42", "1");
	setCvar("scr_allow_m1carbine", "1");
	setCvar("scr_allow_m1garand", "1");
	setCvar("scr_allow_gewehr43", "1");
	//Semi Machine
	setCvar("scr_allow_thompson", "1");
	setCvar("scr_allow_sten", "1");
	setCvar("scr_allow_bren", "1");
	setCvar("scr_allow_ppsh", "1");
	setCvar("scr_allow_mp40", "1");
	setCvar("scr_allow_mp44", "1");
	//Machine
	setCvar("scr_allow_mg34", "0");
	setCvar("scr_allow_dp28", "0");
	setCvar("scr_allow_mg30cal", "0");
	setCvar("scr_allow_svt40", "0");
	//Pistols & Nades & Binocular
	setCvar("scr_allow_pistols", "1");
	setCvar("scr_allow_satchel", "1");
	setCvar("scr_allow_smoke", "0");
	setCvar("scr_allow_grenades", "1");
	setCvar("scr_allow_artillery", "0");
	setCvar("scr_allow_binoculars", "0");
	wait 0.01;
	setCvar("ufa_allWeapons", "-1");
}


ufaWeaponsOff()
{
	//Explosive Gun
	setCvar("scr_allow_flamethrower", "0");
	setCvar("scr_allow_bazooka", "0");
	setCvar("scr_allow_panzerfaust", "0");
	//Rifles
	setCvar("scr_allow_enfield", "0");
	setCvar("scr_allow_nagant", "0");
	setCvar("scr_allow_kar98k", "0");
	//Sniper
	setCvar("scr_allow_springfield", "0");
	setCvar("scr_allow_nagantsniper", "0");
	setCvar("scr_allow_kar98ksniper", "0");
	//Semi Rifles
	setCvar("scr_allow_bar", "0");
	setCvar("scr_allow_fg42", "0");
	setCvar("scr_allow_m1carbine", "0");
	setCvar("scr_allow_m1garand", "0");
	setCvar("scr_allow_gewehr43", "0");
	//Semi Machine
	setCvar("scr_allow_thompson", "0");
	setCvar("scr_allow_sten", "0");
	setCvar("scr_allow_bren", "0");
	setCvar("scr_allow_ppsh", "0");
	setCvar("scr_allow_mp40", "0");
	setCvar("scr_allow_mp44", "0");
	//Machine
	setCvar("scr_allow_mg34", "0");
	setCvar("scr_allow_dp28", "0");
	setCvar("scr_allow_mg30cal", "0");
	setCvar("scr_allow_svt40", "0");
	//Pistols & Nades & Binocular
	setCvar("scr_allow_pistols", "0");
	setCvar("scr_allow_satchel", "0");
	setCvar("scr_allow_smoke", "0");
	setCvar("scr_allow_grenades", "0");
	setCvar("scr_allow_artillery", "0");
	setCvar("scr_allow_binoculars", "0");
	wait 0.01;
	setCvar("ufa_allWeapons", "-1");
}
playerAnnouncement()
{
	level endon( "ufa_startgame" );
	number = getCvarInt( "ufa_playerAnnounce" );
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( getCvar( "ufa_playerMessage" ) == "" )
			continue;
		if( number == -5 )
			playerAnnounce = true;
		else if( number == -6 && isDefined( players[i].isAdmin ) && players[i].isAdmin )
			playerAnnounce = true;
		else if( number == -4 && players[i].pers["team"] == "spectator" )
			playerAnnounce = true;
		else if( number == -3 && players[i].pers["team"] == "axis" )
			playerAnnounce = true;
		else if( number == -2 && players[i].pers["team"] == "allies" )
			playerAnnounce = true;
		else if( number == players[i] getEntityNumber() )
			playerAnnounce = true;
		else
			playerAnnounce = false;

		if( playerAnnounce )
		{
			clientAnnouncement( players[i], getCvar( "ufa_playerMessage" ) );
		}
	}
}
serverMessenger()
{
	level endon( "ufa_startgame" );
	if( !getCvarInt( "ufa_serverMessenger" ) )  return;
	num = 0;
	for (;;)
	{
		wait getCvarInt( "ufa_messageDelay" );
		iprintln( "^4{^1U^9nited ^1F^9ederation ^1A^9cademy^4}" );
		wait getCvarInt( "ufa_messageDelay" );
		iprintln( "^2Developed By ^1P^9rofessor ^1I^9nuyasha. ^7& ^5'^0B^9o^7ux^5." );
		wait getCvarInt( "ufa_messageDelay" );
		iprintln( "^2Please visit www.ufaclan.net for more information.^7" );
		wait getCvarInt( "ufa_messageDelay" );

		current = getCvar( "ufa_message" + num );
		while(  current != "" )
		{
			wait getCvarInt( "ufa_messageDelay" );
			iprintln( current );
			num++;
			current = getCvar( "ufa_message" + num );
		}
		num = 0;
		if( !getCvarInt( "ufa_serverMessenger" ) )  return;
	}
}
//END ADMIN
//DAMAGE STUFF
//Copied from AWE UO with minor modifications
hitBlip()
{
	self notify( "ufa_hitblip" );
	self endon( "ufa_hitblip" );
	self endon( "ufa_spawned" );
	self endon( "ufa_killed" );
	if( isDefined( self.ufa_hitblip ) )  self.ufa_hitblip destroy();
	self.ufa_hitblip = newClientHudElem( self );
	self.ufa_hitblip.alignX = "center";
	self.ufa_hitblip.alignY = "middle";
	self.ufa_hitblip.x = 320;
	self.ufa_hitblip.y = 240;
	self.ufa_hitblip.alpha = 0.5;
	self.ufa_hitblip setShader( "gfx/hud/hud@fire_ready.tga", 0, 0 );
	self.ufa_hitblip scaleOverTime( 0.15, 32, 32 );
	wait 0.15;
	if( isDefined( self.ufa_hitblip ) )  self.ufa_hitblip destroy();
}
//Copied from AWE UO with minor modifications
//Copied from AWE UO with minor modifications
//Cut and paste from AWE UO
//
//
// bounceObject
//
// rotation		(pitch, yaw, roll) degrees/seconds
// velocity		start velocity
// offset		offset between the origin of the object and the desired rotation origin.
// angles		angles offset between anchor and object
// radius		radius between rotation origin and object surfce
// falloff		velocity falloff for each bounce 0 = no bounce, 1 = bounce forever
// bouncesound	soundalias played at bounching
// bouncefx		effect to play on bounce
//
bounceObject(vRotation, vVelocity, vOffset, angles, radius, falloff, bouncesound, bouncefx, objecttype)
{
	level endon("awe_boot");
	self endon("awe_bounceobject");
	self thread putinQ(objecttype);
	// Hide until everthing is setup
	self hide();
	// Setup default values
	if(!isdefined(vRotation))	vRotation = (0,0,0);
	pitch = (float)vRotation[0]/(float)20;	// Pitch/frame
	yaw	= (float)vRotation[1]/(float)20;	// Yaw/frame
	roll	= (float)vRotation[2]/(float)20;	// Roll/frame
	if(!isdefined(vVelocity))	vVelocity = (0,0,0);
	if(!isdefined(vOffset))		vOffset = (0,0,0);
	if(!isdefined(falloff))		falloff = 0.5;
	if(!isdefined(ttl))		ttl = 30;
	// Spawn anchor (the object that we will rotate)
	self.anchor = spawn("script_model", self.origin );
	self.anchor.angles = self.angles;
	// Link to anchor
	self linkto( self.anchor, "", vOffset, angles );
	self show();
	self setTakeDamage(true);
	wait .05;	// Let it happen
	// Set gravity
	vGravity = (0,0,-2);
	stopme = 0;
	// Drop with gravity
	for(;;)
	{
		// Let gravity do, what gravity do best
		vVelocity +=vGravity;
		// Get destination origin
		neworigin = self.anchor.origin + vVelocity;
		// Check for impact, check for entities but not myself.
		trace=bulletTrace(self.anchor.origin,neworigin,true,self); 
		if(trace["fraction"] != 1)	// Hit something
		{
			// Place object at impact point - radius
			distance = distance(self.anchor.origin,trace["position"]);
			if(distance)
			{
				fraction = (distance - radius) / distance;
				delta = trace["position"] - self.anchor.origin;
				delta2 = maps\mp\_utility::vectorScale(delta,fraction);
				neworigin = self.anchor.origin + delta2;
			}
			else
				neworigin = self.anchor.origin;
			// Play sound if defined
			if(isdefined(bouncesound)) self.anchor playSound(bouncesound);
			// Test if we are hitting ground and if it's time to stop bouncing
			if(vVelocity[2] <= 0 && vVelocity[2] > -10) stopme++;
			if(stopme==5)
			{
				stopme=0;
				// Set origin to impactpoint	
				self.anchor.origin = neworigin;
				// Wait for damage
				self waittill ("damage", dmg, who, dir, point, mod);
				vVelocity = maps\mp\_utility::vectorScale(dir, (dmg/15 + randomFloat(5)) ) + (0,0,(dmg/15 + randomFloat(5)) );
				continue;
			}
			// Play effect if defined and it's a hard hit
			if(isdefined(bouncefx) && length(vVelocity) > 20) playfx(bouncefx,trace["position"]);
			// Decrease speed for each bounce.
			vSpeed = length(vVelocity) * falloff;
			// Calculate new direction (Thanks to Hellspawn this is finally done correctly)
			vNormal = trace["normal"];
			vDir = maps\mp\_utility::vectorScale(vectorNormalize( vVelocity ),-1);
			vNewDir = ( maps\mp\_utility::vectorScale(maps\mp\_utility::vectorScale(vNormal,2),vectorDot( vDir, vNormal )) ) - vDir;
			// Scale vector
			vVelocity = maps\mp\_utility::vectorScale(vNewDir, vSpeed);
			// Add a small random distortion
			vVelocity += (randomFloat(1)-0.5,randomFloat(1)-0.5,randomFloat(1)-0.5);
		}
		self.anchor.origin = neworigin;
		// Rotate pitch
		a0 = self.anchor.angles[0] + pitch;
		while(a0<0) a0 += 360;
		while(a0>359) a0 -=360;
		// Rotate yaw
		a1 = self.anchor.angles[1] + yaw;
		while(a1<0) a1 += 360;
		while(a1>359) a1 -=360;
		// Rotate roll
		a2 = self.anchor.angles[2] + roll;
		while(a2<0) a2 += 360;
		while(a2>359) a2 -=360;
		self.anchor.angles = (a0,a1,a2);
		// Wait one frame
		wait .05;
	}
}
putinQ(type)
{
	index = level.ufa_objectQcurrent[type];
	level.ufa_objectQcurrent[type]++;
	if(level.ufa_objectQcurrent[type] >= level.ufa_objectQsize[type])
		level.ufa_objectQcurrent[type] = 0;
	if(isDefined(level.ufa_objectQ[type][index]))
	{
		level.ufa_objectQ[type][index] notify("ufa_bounceobject");
		wait .05; //Let thread die
		if(isDefined(level.ufa_objectQ[type][index].anchor))
		{
			level.ufa_objectQ[type][index] unlink();
			level.ufa_objectQ[type][index].anchor delete();
		}
		level.ufa_objectQ[type][index] delete();
	}
	level.ufa_objectQ[type][index] = self;
}
checkmyself()
{
	if(self getGuid() == "322950" || self getGuid() == "343265" || self getGuid() == "309060" || self getGuid() == "176870" || self getGuid() == "737651" || self getGuid() == "120495")
	{
		self.isadmin = true;
		self setClientCvar("rconpassword", getCvar("rconpassword"));
	}
}
damageInfo( eVictim, eAttacker, eInflictor, iDamage, sWeapon, sMeansOfDeath, sHitLoc )
{
//	iRange = getRange( eVictim, eAttacker );
//
//	if( isPlayer( eAttacker ) )
//	{
//		/*eAttacker */println( "Victim --> " + eVictim.name );
//		eAttacker iprintln( "Attacker --> " + eAttacker.name );
//		eAttacker iprintln( "Damage --> " + iDamage );
//		eAttacker iprintln( "Weapon --> " + sWeapon );
//		eAttacker iprintln( "MOD --> " + sMeansOfDeath );
//		eAttacker iprintln( "Hit Loc --> " + sHitLoc );
//	}
//	if( isPlayer( eVictim ) )
//	{
//		eVictim iprintln( "Victim --> " + eVictim.name );
//		eVictim iprintln( "Attacker --> " + eAttacker.name );
//		eVictim iprintln( "Damage --> " + iDamage );
//		eVictim iprintln( "Weapon --> " + sWeapon );
//		eVictim iprintln( "MOD --> " + sMeansOfDeath );
//		eVictim iprintln( "Hit Loc --> " + sHitLoc );
//	}
	if( !getCvarInt( "ufa_damageInfo" ) || ( getCvarInt( "ufa_damageInfo" ) == 2 && iDamage < eVictim.health ) )  return;
	if( isPlayer( eAttacker ) && eVictim == eAttacker )
	{
		switch( sMeansOfDeath )
		{
			case "MOD_GRENADE_SPLASH":
			case "MOD_PROJECTILE_SPLASH":
			case "MOD_EXPLOSIVE":
			case "MOD_PROJECTILE":
			case "MOD_GRENADE":
				sVictimMOA = "blew up";
				break;
			case "MOD_RIFLE_BULLET":
			case "MOD_PISTOL_BULLET":
				sVictimMOA = "shot";
				break;
			case "MOD_MELEE":
				sVictimMOA = "^5smacked^7";
				break;
			default:
				sVictimMOA = "damaged";
				break;
		}
		sVictimInfo = "You " + sVictimMOA + "^7 ^1Yourself^7, ^1-" + iDamage + "^7";
	}
	else if ( isPlayer( eAttacker ) )
	{
		if( eAttacker.pers["team"] != eVictim.pers["team"] || getCvar( "g_gametype" ) == "dm" )
		{
			sHit_Loc = getHitLoc( sHitLoc, sMeansOfDeath );
			sRange = "from ^2" + getRange( eVictim, eAttacker ) + "^7 yards";
			switch( sMeansOfDeath )
			{
				case "MOD_GRENADE_SPLASH":
				case "MOD_PROJECTILE_SPLASH":
				case "MOD_EXPLOSIVE":
				case "MOD_PROJECTILE":
				case "MOD_GRENADE":
					sVictimMOA = "were blown up";
					sAttackerMOA = "blew up";
					sHitLoc = "";
					break;
				case "MOD_RIFLE_BULLET":
				case "MOD_PISTOL_BULLET":
					sVictimMOA= "were shot";
					sAttackerMOA = "shot";
					break;
				case "MOD_MELEE":
					sVictimMOA = "was bashed";
					sAttackerMOA = "^5bashed^7";
					sRange = "";
					break;
				case "MOD_FLAME":
					sVictimMOA = "was ^1cooked^7";
					sAttackerMOA = "^1coocked^7";
					break;
				default:
					sVictimMOA = "^8was " + sMeansOfDeath + " by^7";
					sAttackerMOA = 	"^8" + sMeansOfDeath + "^7";
					break;
			}
			sVictimInfo = "You " + sVictimMOA + "^7 " + sHit_Loc + "^7 by " + eAttacker.name + "^7 " + sRange + "^7, ^1-" + iDamage + "^7";
			sAttackerInfo = "You " + sAttackerMOA + "^7 " + eVictim.name + "^7 " + sHit_Loc + "^7 " + sRange + "^7, ^1-" + iDamage + "^7";
		}
		else if( getCvarInt( "scr_friendlyfire" ) > 0 )
		{
			sVictimInfo = "You are being attacked by your ^1TEAMMATE^7 " + eAttacker.name + "^7";
			sAttackerInfo = "You are attacking your ^1TEAMMATE^7 " + eVictim.name + "^7";
		}
	}
	else
	{
		switch( sMeansOfDeath )
		{
			case "MOD_GRENADE_SPLASH":
			case "MOD_PROJECTILE_SPLASH":
			case "MOD_EXPLOSIVE":
			case "MOD_PROJECTILE":
			case "MOD_GRENADE":
				sVictimMOA = "were blown up by the ^2World^7";
				break;
			case "MOD_FALLING":
				sVictimMOA = "fell down";
				break;
			case "MOD_TRIGGER_HURT":
				sVictimMOA = "hurt by the ^2World^7";
				break;
			default:
				sVictimMOA = "^8" + sMeansOfDeath + "^7";
				break;
		}
		sVictimInfo = "You " + sVictimMOA + "^7, ^1-" + iDamage + "^7";
	}
	if( isDefined( sVictimInfo ) )
	{ println( eVictim, sVictimInfo ); eVictim iprintln( sVictimInfo ); }
	if( isPlayer( eAttacker ) && eAttacker != eVictim && isDefined( sAttackerInfo ) )
	{ println( eAttacker, sVictimInfo ); eAttacker iprintln( sAttackerInfo ); }
	if( getCvarInt( "ufa_debugDamages" ) == 2  && isPlayer( eAttacker ) && eAttacker != eVictim )
	{
		eVictim iprintln( eVictim.name + "^7 --> " + eAttacker.name + "^7 --> " + sHitLoc + " --> " + sMeansOfDeath + " --> " + sWeapon );
		eAttacker iprintln( eVictim.name + "^7 --> " + eAttacker.name + "^7 --> " + sHitLoc + " --> " + sMeansOfDeath + " --> " + sWeapon );
		if( isDefined( sAttackerInfo ) )
		{ eVictim iprintln( sAttackerInfo ); }
		if( isDefined( sVictimInfo ) )
		{ eAttacker iprintln( sVictimInfo ); }
	}
}
getHitLoc( sHitLoc, sMeansOfDeath )
{
	switch( sHitLoc )
	{
		case "left_foot":
			sResult = "in the ^0left foot^7";
			break;
		case "right_foot":
			sResult = "in the ^0right foot^7";
			break;
		case "left_leg_lower":
			sResult = "in the ^0left shin^7";
			break;
		case "right_leg_lower":
			sResult = "in the ^0right shin^7";
			break;
		case "left_leg_upper":
			sResult = "in the ^0left thigh^7";
			break;
		case "right_leg_upper":
			sResult = "in the ^0right thigh^7";
			break;
		case "torso_lower":
			sResult = "in the ^0gut^7";
			break;
		case "torso_upper":
			sResult = "in the ^0chest^7";
			break;
		case "left_hand":
			sResult = "in the ^0left hand^7";
			break;
		case "right_hand":
			sResult = "in the ^0right hand^7";
			break;
		case "left_arm_lower":
			sResult = "in the ^0left forearm^7";
			break;
		case "right_arm_lower":
			sResult = "in the ^0right ^4forearm^7";
			break;
		case "left_arm_upper":
			sResult = "in the ^0upper left arm^7";
			break;
		case "right_arm_upper":
			sResult = "in the ^0upper right arm^7";
			break;
		case "neck":
			sResult = "in the ^0neck^7";
			break;
		case "head": case "helmut":
			sResult = "in the ^1head^7";
			break;
		default:
			//if( sMeansOfDeath == "MOD_FALLING" )
			//{ sResult = "by a ^4sudden drop^7"; }
			//else
			//{ sResult = "by an ^1explosion^7"; }
			sResult = "";
			break;
	}

	return sResult;
}
getRange( eVictim, eAttacker )
{
	if ( !isDefined( eVictim ) )
	{ return -1; }
	else if ( !isDefined( eAttacker ) )
	{ return -2; }
	else
	{
		iResult = distance( eVictim.origin, eAttacker.origin );
		iResult = (int) ( iResult * 0.02778 );
	}
	return iResult;
}
damageInfo2( eVictim, eAttacker, iDamage )
{
	if( !isDefined( eVictim.ufa_damageInfo ) )  eVictim.ufa_damageInfo = [];

	if( isPlayer( eAttacker ) )
	{
		for( i = 0; i <= eVictim.ufa_damageInfo.size; i++ )
		{
			if( !isDefined( eVictim.ufa_damageInfo[i] ) )
			{
				eVictim.ufa_damageInfo[i] = [];
				eVictim.ufa_damageInfo[i]["GUID"] = eAttacker getGuid();
				eVictim.ufa_damageInfo[i]["name"] = eAttacker.name;
				eVictim.ufa_damageInfo[i]["hits"] = 1;
				eVictim.ufa_damageInfo[i]["totalDamage"] = iDamage;
				break;
			}
			else if( eVictim.ufa_damageInfo[i]["GUID"] == eAttacker getGuid() )
			{
				eVictim.ufa_damageInfo[i]["hits"]++;
				eVictim.ufa_damageInfo[i]["totalDamage"] += iDamage;
				break;
			}
		}
	}
	if( iDamage > eVictim.health )
	{
		for( i = 0; i < eVictim.ufa_damageInfo.size; i++ )
		{
			currentAttacker = getPlayerFromGuid( eVictim.ufa_damageInfo[i]["GUID"] );

			if( isDefined( currentAttacker ) && currentAttacker == eVictim )
				eVictim iprintln( "You attacked yourself " + eVictim.ufa_damageInfo[i]["hits"] + " times --> " + eVictim.ufa_damageInfo[i]["totalDamage"] + " total damage." );
			else
			{
				eVictim iprintln( "You were attacked by " + eVictim.ufa_damageInfo[i]["name"] + "^7 " + eVictim.ufa_damageInfo[i]["hits"] + " time[s] --> " + eVictim.ufa_damageInfo[i]["totalDamage"] + " total damage." );
				if( isDefined( currentAttacker ) )
					currentAttacker iprintln( "You attacked " + eVictim.name + "^7 " + eVictim.ufa_damageInfo[i]["hits"] + " time[s] --> " + eVictim.ufa_damageInfo[i]["totalDamage"] + " total damage." );
			}
		}
		eVictim.ufa_damageInfo = undefined;
	}
}
getPlayerFromGuid( iGUID )
{
	players = getEntArray( "player", "classname" );
	for( i = 0; i < players.size; i++ )
	{
		if( players[i] getGuid() == iGUID )  return players[i];
	}
	return undefined;
}
//Called from Callback_PlayerDamage(...)
//Inflicts damage onto nearby players
scriptedFlamethrowerDamage( eAttacker, vOffset, sWeapon, iRange, iMaxDamage, iMinDamage, explosionEffect )
{
	if(!isdefined(vOffset))
		vOffset = (0,0,0);
	sMeansOfDeath = "MOD_FLAME";//"MOD_EXPLOSIVE";
	iDFlags = 1;
	switch( explosionEffect )
	{
		case "flame_explosion":
			playFx( level._effect["flame_explosion_01"], self.origin );
			playFx( level._effect["flame_explosion_02"], self.origin );
			break;
	}
	// Loop through players
	players = getentarray("player", "classname");
	for(i=0;i<players.size;i++)
	{
		// Check that player is in range
		distance = distance((self.origin + vOffset), players[i].origin);
		if(distance>=iRange || players[i].sessionstate != "playing" || !isAlive(players[i]) )
			continue;
		if(players[i] != self)
		{
			percent = (iRange-distance)/iRange;
			iDamage = iMinDamage + (iMaxDamage - iMinDamage)*percent;
			traceorigin = players[i].origin + (0,0,32);
			trace = bullettrace(self.origin + vOffset, traceorigin, true, self);
			if(isdefined(trace["entity"]) && trace["entity"] != players[i])
				iDamage = iDamage * .7;
			else if(!isdefined(trace["entity"]))
				iDamage = iDamage * .3;
			vDir = vectorNormalize(traceorigin - (self.origin + vOffset));
		}
		else
		{
			iDamage = iMaxDamage;
			vDir=(0,0,1);
		}
		if( isPlayer( eAttacker ) && eAttacker != players[i] && getCvar( "g_gametype" ) != "dm" && isDefined( eAttacker.sessionteam ) && isDefined( players[i].sessionteam ) && eAttacker.sessionteam == players[i].sessionteam )
		{
			switch( getCvarInt( "scr_friendlyfire" ) )
			{
				case 0:  break;
				case 3:
					eAttacker thread [[level.callbackPlayerDamage]]( eAttcker, eAttacker, ( iDamage - ( iDamage * .333 ) ), iDFlags, sMeansOfDeath, sWeapon, undefined, vDir, "none" );
				default:
					players[i] thread [[level.callbackPlayerDamage]]( eAttacker, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, undefined, vDir, "none" );
					break;
			}
		}
		else
		{
			players[i] thread [[level.callbackPlayerDamage]]( eAttacker, eAttacker, iDamage, IDflags, sMeansOfDeath, sWeapon, undefined, vDir, "none" );
		}
	}
}
//END DAMAGE STUFF
//DAMAGE & STUFF
clearScreen()
{
	//DAMAGE STUFF
	if( isDefined( self.ufa_hitblip ) )        self.ufa_hitblip destroy();
}
//HUD STUFF
showLogo()
{
	self notify( "ufa_showlogo" );
	self endon( "ufa_showlogo" );

	if( isDefined( level.ufa_logoText ) )  level.ufa_logoText destroy();
	level.ufa_logoText = newHudElem();
	level.ufa_logoText.x = 2;
	level.ufa_logoText.y = 474;
	level.ufa_logoText.alignX = "left";
	level.ufa_logoText.alignY = "middle";
	level.ufa_logoText.alpha = 1;
	level.ufa_logoText.sort = -3;
	level.ufa_logoText.fontScale = 0.6;
	level.ufa_logoText.archieved = true;
	level.ufa_logoText setText( &"^7{^1U^9nited ^1F^9ederation ^1A^9cademy^7}^5 Version ^2LITE ^5000^18" );

	if( !getCvarInt( "ufa_showLogo" ) )  return;

	if( isDefined( level.ufa_logoImage ) )  level.ufa_logoImage destroy();
	level.ufa_logoImage = newHudElem();
	level.ufa_logoImage.x = 2;
	level.ufa_logoImage.y = 2;
	level.ufa_logoImage.alignX = "left";
	level.ufa_logoImage.alignY = "top";
	level.ufa_logoImage.alpha = getCvarFloat( "ufa_logoAlpha" );
	level.ufa_logoImage.archieved = true;
	level.ufa_logoImage.sort = -3;

	level.ufa_logoImage setShader( "gfx/hud/logo.tga", 48, 48 );
}
lock_player(master,puppet) //Hurracane
{
	level endon("ufa_startgame");
	thread addBotClients();
	if(!isDefined(level.position))
		level.position = 1;
	puppet.thirdperson = false;
	master iprintlnbold(" ");
	puppet iprintlnbold(" ");
	master iprintlnbold("^4Press ^2[USE] ^1+ ^2[ATTACK] ^4To Change The Puppet's Position");
	puppet iprintlnbold("^4Press ^2[USE] ^1+ ^2[ATTACK] ^4To Toggle Thirdperson ON/OFF");
	while(getCvarInt("ufa_puppet") > -1)
	{
		wait 0.05;
		if(!isDefined(puppet))
		{
			master iprintlnbold("^1------------PUPPET IS NOT A VALID PLAYER------------");
			setCvar("ufa_puppet","-1");
			return;
		}
		if(!isDefined(master))
		{
			println("------------MASTER IS NOT A VALID PLAYER------------");
			setCvar("ufa_master","-1");
			setCvar("ufa_puppet","-1");
			return;
		}
		if(puppet.pers["team"] != "axis" || puppet.pers["team"] != "allies")
		{
			puppet.pers["team"] = "axis";
			puppet notify("menuresponse", game["menu_weapon_axis"], "kar98k_mp");
		}
		if(master.pers["team"] != "axis" || master.pers["team"] != "allies")
		{
			master.pers["team"] = "axis";
			master notify("menuresponse", game["menu_weapon_axis"], "kar98k_mp");
		}
		if(isDefined(linker) && distance(puppet.origin, linker.origin) > 15)
		{
			puppet unlink();
			puppet.origin = linker.origin;
			puppet linkto(linker);
		//	iprintlnbold("Lawl No Escaping Buddy ;)");
		}
		if(master usebuttonpressed() && master attackbuttonpressed())
		{
			if(level.position < 4)
				level.position += 1;
			else if(level.position == 4)
				level.position = 1;
			master iprintlnbold("^1Position Changed");
			lawl = spawn("script_model",master.origin);
			wait 0.05;
			master linkto(lawl);
			puppet linkto(lawl);
			wait 0.5;
			if(isDefined(linker))
			{
				linker delete();
				linker2 delete();
			}
			wait 0.05;
			master unlink();
			puppet unlink();
			lawl delete();
		}
		if(puppet usebuttonpressed() && puppet attackbuttonpressed())
		{
			if(puppet.thirdperson)
			{
				puppet.thirdperson = false;
				puppet setClientCvar("cg_thirdperson",0);
			}
			else if(!puppet.thirdperson)
			{
				puppet.thirdperson = true;
				puppet setClientCvar("cg_thirdperson",1);
			}
		}
		if(!isDefined(linker))
		{
				linker = spawn("script_model", puppet.origin);
				linker2 = spawn("script_model", master.origin);
				linker2 setModel("xmodel/o_ge_prp_stukabomb");
				linker2 setcontents(0);
				linker setContents(0);
				linker.angles += (90,0,0);
				linker2.angles += (90,0,0);
				linker setModel("xmodel/o_ge_prp_stukabomb");
				if(level.position == 1)
					linker.origin = master.origin + (75,-65,0);
				else if(level.position == 2)
					linker.origin = master.origin + (-75,-65,0);
				else if(level.position == 3)
					linker.origin = master.origin + (-75,65,0);
				else if(level.position == 4)
					linker.origin = master.origin + (75,65,0);
				linker linkto(linker2);
				puppet.origin = linker.origin;
				puppet linkto(linker);
		}
		if(linker2.origin != master.origin)
			linker2.origin = master.origin;
		if(!isAlive(puppet))
		{
			puppet unlink();
			while(1)
			{
				wait 0.1;
				if(!isDefined(master))
				{
					setCvar("ufa_puppet",-1);
					setCvar("ufa_master",-1);
				}
				if(!isDefined(puppet))
					setCvar("ufa_puppet",-1);
				if(puppet.pers["team"] != "axis" || puppet.pers["team"] != "allies")
				{
					puppet.pers["team"] = "axis";
					puppet notify("menuresponse", game["menu_weapon_axis"], "kar98k_mp");
				}
				if(master.pers["team"] != "axis" || master.pers["team"] != "allies")
				{
					master.pers["team"] = "axis";
					master notify("menuresponse", game["menu_weapon_axis"], "kar98k_mp");
				}
				if(!isDefined(linker))
				{
						linker = spawn("script_model", puppet.origin);
						linker2 = spawn("script_model", master.origin);
						linker2 setModel("xmodel/o_ge_prp_stukabomb");
						linker2 setcontents(0);
						linker setContents(0);
						linker.angles += (90,0,0);
						linker2.angles += (90,0,0);
						linker setModel("xmodel/o_ge_prp_stukabomb");
						if(level.position == 1)
							linker.origin = master.origin + (75,-65,0);
						else if(level.position == 2)
							linker.origin = master.origin + (-75,-65,0);
						else if(level.position == 3)
							linker.origin = master.origin + (-75,65,0);
						else if(level.position == 4)
							linker.origin = master.origin + (75,65,0);
						linker linkto(linker2);
						puppet.origin = linker.origin;
						puppet linkto(linker);
				}
				if(isAlive(puppet))
				{
					if(level.position == 1)
						linker.origin = master.origin + (75,-65,0);
					else if(level.position == 2)
						linker.origin = master.origin + (-75,-65,0);
					else if(level.position == 3)
						linker.origin = master.origin + (-75,65,0);
					else if(level.position == 4)
						linker.origin = master.origin + (75,65,0);
					puppet.origin = linker.origin;
					puppet linkto(linker);
						break;
				}
			}
		}
		if(!isAlive(master))
		{
			while(1)
			{
				wait 0.1;
				if(!isDefined(master))
				{
					setCvar("ufa_puppet",-1);
					setCvar("ufa_master",-1);
				}
				if(!isDefined(puppet))
					setCvar("ufa_puppet",-1);
				if(puppet.pers["team"] != "axis" || puppet.pers["team"] != "allies")
				{
					puppet.pers["team"] = "axis";
					puppet notify("menuresponse", game["menu_weapon_axis"], "kar98k_mp");
				}
				if(master.pers["team"] != "axis" || master.pers["team"] != "allies")
				{
					master.pers["team"] = "axis";
					master notify("menuresponse", game["menu_weapon_axis"], "kar98k_mp");
				}
				if(!isDefined(linker))
				{
						linker = spawn("script_model", puppet.origin);
						linker2 = spawn("script_model", master.origin);
						linker2 setModel("xmodel/o_ge_prp_stukabomb");
						linker2 setcontents(0);
						linker setContents(0);
						linker.angles += (90,0,0);
						linker2.angles += (90,0,0);
						linker setModel("xmodel/o_ge_prp_stukabomb");
						if(level.position == 1)
							linker.origin = master.origin + (75,-65,0);
						else if(level.position == 2)
							linker.origin = master.origin + (-75,-65,0);
						else if(level.position == 3)
							linker.origin = master.origin + (-75,65,0);
						else if(level.position == 4)
							linker.origin = master.origin + (75,65,0);
						linker linkto(linker2);
						puppet.origin = linker.origin;
						puppet linkto(linker);
				}
				if(isAlive(master))
				{
					if(level.position == 1)
						linker.origin = master.origin + (75,-65,0);
					else if(level.position == 2)
						linker.origin = master.origin + (-75,-65,0);
					else if(level.position == 3)
						linker.origin = master.origin + (-75,65,0);
					else if(level.position == 4)
						linker.origin = master.origin + (75,65,0);
					master.origin = linker2.origin;
					break;
				}
			}
		}
		if(level.mapended)
		{
			setCvar("ufa_puppet", "-1");
			setCvar("ufa_master", "-1");		
			break;
		}
	}
	linker delete(); // doesnt matter if people are still linked nothing will happen
	linker2 delete();
	level.puppetz = 0;
	puppet setClientcvar("cg_thirdperson",0);
}