/*
  _isWeaponType.gsc by ShadowLord
    (method from AWE UO Mod by Bell )
*/
main(type,weapon)
{
	switch(type)
	{
		case "turret":
			switch(weapon)
			{
				case "mg42_bipod_duck_mp":
				case "mg42_bipod_prone_mp":
				case "mg42_bipod_stand_mp":
				case "mg42_tank_mp":
				case "mg42_turret_mp":
				case "30cal_tank_mp":
				case "50cal_tank_mp":
				case "mg34_tank_mp":
				case "mg50cal_tripod_stand_mp":
				case "mg_sg43_stand_mp":
				case "sg43_tank_mp":
				case "sg43_turret_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		case "rocket":
			switch(weapon)
			{
				case "panzerfaust_mp":
				case "panzerschreck_mp":
				case "bazooka_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;
			
		case "common":
			switch(weapon)
			{
				case "fg42_mp":
				case "panzerfaust_mp":
				case "panzerschreck_mp":
				case "flamethrower_mp":
				case "bazooka_mp":
				case "smokegrenade_mp":
				case "flashgrenade_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		// Check if weapon is a grenade
		case "grenade":
			switch(weapon)
			{
				case "fraggrenade_mp":
				case "mk1britishfrag_mp":
				case "rgd-33russianfrag_mp":
				case "stielhandgranate_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		case "satchel":
			switch( weapon )
			{
				case "satchelcharge_mp":
					return true;
					break;
				default:
					return false;
					break;
			}

		// Check if weapon is smoke/flash grenade
		case "smokegrenade":
			switch(weapon)
			{
				case "smokegrenade_mp":
				case "flashgrenade_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		//Check if weapon is bolt-action
		case "bolt-action":
			switch( weapon )
			{
				case "mosin_nagant_mp":
				case "kar98k_mp":
				case "enfield_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		// Check if weapon is a rifle
		case "rifle":
			switch(weapon)
			{
				case "m1carbine_mp":
				case "m1garand_mp":
				case "mosin_nagant_mp":
				case "svt40_mp":
				case "kar98k_mp":
				case "gewehr43_mp":
				case "enfield_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		// Check if weapon is smg
		case "smg":
			switch(weapon)
			{
				case "mp40_mp":
				case "sten_mp":
				case "thompson_mp":
				case "ppsh_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		// Check if weapon is assault
		case "assault":
			switch(weapon)
			{
				case "mp44_mp":
				case "bar_mp":
				case "bren_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		// Check if weapon is sniper
		case "sniper":
			switch(weapon)
			{
				case "mosin_nagant_sniper_mp":
				case "springfield_mp":
				case "kar98k_sniper_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		// Check if weapon is lmg
		case "lmg":
			switch(weapon)
			{
				case "dp28_mp":
				case "mg34_mp":
				case "mg30Cal_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		// Check if weapon is pistol
		case "pistol":
			switch(weapon)
			{
				case "colt_mp":
				case "luger_mp":
				case "tt33_mp":
				case "webley_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		// Check if weapon is american
		case "american":
			switch(weapon)
			{
				case "fraggrenade_mp":
				case "colt_mp":
				case "m1carbine_mp":
				case "m1garand_mp":
				case "thompson_mp":
				case "bar_mp":
				case "springfield_mp":
				case "mg30Cal_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		// Check if weapon is british
		case "british":
			switch(weapon)
			{
				case "mk1britishfrag_mp":
				case "webley_mp":
				case "enfield_mp":
				case "sten_mp":
				case "bren_mp":
				case "springfield_mp":
				case "mg30Cal_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		// Check if weapon is russian
		case "russian":
			switch(weapon)
			{
				case "rgd-33russianfrag_mp":
				case "tt33_mp":
				case "mosin_nagant_mp":
				case "svt40_mp":
				case "ppsh_mp":
				case "mosin_nagant_sniper_mp":
				case "dp28_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		// Check if weapon is german
		case "german":
			switch(weapon)
			{
				case "stielhandgranate_mp":
				case "luger_mp":
				case "kar98k_mp":
				case "gewehr43_mp":
				case "mp40_mp":
				case "mp44_mp":
				case "mg34_mp":
				case "kar98k_sniper_mp":
					return true;
					break;
				default:
					return false;
					break;
			}
			break;

		default:
			return false;
			break;
	}
}
