Callback_StartGameType()
{
	// Set up variables
	setupVariables();



	// Show lev logo
	showlogo();


	// Start threads
	startThreads();
}

startThreads()
{
	level notify("jump_boot");

	// Start thread for updating variables from cvars
	thread updateGametypeCvars(false);
}

setupVariables()
{
	// defaults if not defined in level script
	if(!isDefined(game["allies"]))
		game["allies"] = "american";
	if(!isDefined(game["axis"]))
		game["axis"] = "german";

	// Set up the number of available punishments
	level.jump_punishments = 3;

	// Set up number of voices
	level.jump_voices["german"] = 3;
	level.jump_voices["american"] = 7;
	level.jump_voices["russian"] = 6;
	level.jump_voices["british"] = 6;


	// Set up grenade voices
	level.jump_grenadevoices["german"][0]="german_grenade";
	level.jump_grenadevoices["german"][1]="generic_grenadeattack_german_1";
	level.jump_grenadevoices["german"][2]="generic_grenadeattack_german_2";
	level.jump_grenadevoices["german"][3]="generic_grenadeattack_german_3";	

	level.jump_grenadevoices["american"][0]="american_grenade";
	level.jump_grenadevoices["american"][1]="generic_grenadeattack_american_1";
	level.jump_grenadevoices["american"][2]="generic_grenadeattack_american_2";
	level.jump_grenadevoices["american"][3]="generic_grenadeattack_american_3";
	level.jump_grenadevoices["american"][4]="generic_grenadeattack_american_4";
	level.jump_grenadevoices["american"][5]="generic_grenadeattack_american_5";
	level.jump_grenadevoices["american"][6]="generic_grenadeattack_american_6";	

	level.jump_grenadevoices["russian"][0]="russian_grenade";
	level.jump_grenadevoices["russian"][1]="generic_grenadeattack_russian_3";
	level.jump_grenadevoices["russian"][2]="generic_grenadeattack_russian_4";
	level.jump_grenadevoices["russian"][3]="generic_grenadeattack_russian_5";
	level.jump_grenadevoices["russian"][4]="generic_grenadeattack_russian_6";	

	level.jump_grenadevoices["british"][0]="british_grenade";
	level.jump_grenadevoices["british"][1]="generic_grenadeattack_british_1";
	level.jump_grenadevoices["british"][2]="generic_grenadeattack_british_2";
	level.jump_grenadevoices["british"][3]="generic_grenadeattack_british_4";
	level.jump_grenadevoices["british"][4]="generic_grenadeattack_british_5";
	level.jump_grenadevoices["british"][5]="generic_grenadeattack_british_6";	


	// Initialize variables from cvars
	updateGametypeCvars(true);	


	if(isdefined(game["german_soldiervariation"]) && game["german_soldiervariation"] == "winter")
		level.jump_wintermap = true;
	overrideteams();


	// Load effect for bomb explosion (used by antiteamkill,)
	level._effect["bombexplosion"]= loadfx("fx/explosions/pathfinder_explosion.efx");


	if(
		level.jump_secondaryweapon["default"] == "select"	|| level.jump_secondaryweapon["default"] == "selectother"
	  )
	{
		level.jump_secondaryweapontext = &"Select your secondary weapon";
	}

	// Disable minefields?
	if(level.jump_disableminefields)
	{
		minefields = getentarray( "minefield", "targetname" );
		if(minefields.size)
			for(i=0;i< minefields.size;i++)
				if(isdefined(minefields[i]))
					minefields[i] delete();
	}

}
	
showWelcomeMessages()
{
	self endon("jump_spawned");
	self endon("jump_died");

	if(isdefined(self.pers["jump_welcomed"])) return;
	self.pers["jump_welcomed"] = true;

	wait 2;

	count = 0;
	message = cvardef("scr_jump_welcome" + count, "", "", "", "string");
	while(message != "")
	{
		self iprintlnbold(message);
		count++;
		message = cvardef("scr_jump_welcome" + count, "", "", "", "string");
		wait level.jump_welcomedelay;
	}
}

updateGametypeCvars(init)
{
	level endon("jump_boot");

	// Debug
	level.jump_debug = cvardef("scr_jump_debug", 0, 0, 1, "int");

	// Disable minefields
	level.jump_disableminefields = cvardef("scr_jump_disable_minefields", 0, 0, 1, "int");

	// Weapon options
	level.jump_secondaryweapon["default"]= "panzerfaust_mp";


	for(;;)
	{
		// team overriding
		level.jump_teamallies = cvardef("scr_jump_team_allies","","","","string");

		// welcome message
		level.jump_welcomedelay	= cvardef("scr/_welcome_delay", 1, 0.05, 30, "float");


		//save positions
		level.savepositions = cvardef("scr_allow_jumps", 0, 0, 1, "int");

		// Grenade options
		level.jump_grenadewarning = cvardef("scr_jump_grenade_warning", 0, 0, 99, "int");
		level.jump_grenadewarningrange = 500;
		level.jump_grenadecount = cvardef("scr_jump_grenade_count", 0, 0, 999, "int");
		level.jump_panzercount = cvardef("scr_jump_panzer_count", 0, 0, 999, "int");
		
		// Ammo limiting
		level.jump_ammomin = cvardef("scr_jump_ammo_min",100,0,100,"int");
		level.jump_ammomax = cvardef("scr_jump_ammo_max",100,level.jump_ammomin,100,"int");

		// Hud
		level.jump_showlogo = cvardef("scr_jump_show_logo", 0, 0, 1, "int");	

		// If we are initializing variables, break here
		if(init) break;

		wait 2;
	}
}

/*
USAGE OF "cvardef"
cvardef replaces the multiple lines of code used repeatedly in the setup areas of the script.
The function requires 5 parameters, and returns the set value of the specified cvar
Parameters:
	varname - The name of the variable, i.e. "scr_teambalance", or "scr_dem_respawn"
		This function will automatically find map-sensitive overrides, i.e. "src_dem_respawn_mp_brecourt"

	vardefault - The default value for the variable.  
		Numbers do not require quotes, but strings do.  i.e.   10, "10", or "wave"

	min - The minimum value if the variable is an "int" or "float" type
		If there is no minimum, use "" as the parameter in the function call

	max - The maximum value if the variable is an "int" or "float" type
		If there is no maximum, use "" as the parameter in the function call

	type - The type of data to be contained in the vairable.
		"int" - integer value: 1, 2, 3, etc.
		"float" - floating point value: 1.0, 2.5, 10.384, etc.
		"string" - a character string: "wave", "player", "none", etc.
*/
cvardef(varname, vardefault, min, max, type)
{
	mapname = getcvar("mapname");		// "mp_dawnville", "mp_rocket", etc.
	gametype = getcvar("g_gametype");	// "tdm", "bel", etc.

//	if(getcvar(varname) == "")		// if the cvar is blank
//		setcvar(varname, vardefault); // set the default

	tempvar = varname + "_" + gametype;	// i.e., scr_teambalance becomes scr_teambalance_tdm
	if(getcvar(tempvar) != "") 		// if the gametype override is being used
		varname = tempvar; 		// use the gametype override instead of the standard variable

	tempvar = varname + "_" + mapname;	// i.e., scr_teambalance becomes scr_teambalance_mp_dawnville
	if(getcvar(tempvar) != "")		// if the map override is being used
		varname = tempvar;		// use the map override instead of the standard variable


	// get the variable's definition
	switch(type)
	{
		case "int":
			if(getcvar(varname) == "")		// if the cvar is blank
				definition = vardefault;	// set the default
			else
				definition = getcvarint(varname);
			break;
		case "float":
			if(getcvar(varname) == "")	// if the cvar is blank
				definition = vardefault;	// set the default
			else
				definition = getcvarfloat(varname);
			break;
		case "string":
		default:
			if(getcvar(varname) == "")		// if the cvar is blank
				definition = vardefault;	// set the default
			else
				definition = getcvar(varname);
			break;
	}

	// if it's a number, with a minimum, that violates the parameter
	if((type == "int" || type == "float") && min != "" && definition < min)
		definition = min;

	// if it's a number, with a maximum, that violates the parameter
	if((type == "int" || type == "float") && max != "" && definition > max)
		definition = max;

	return definition;
}

// sort a list of entities with ".origin" properties in ascending order by their distance from the "startpoint"
// "points" is the array to be sorted
// "startpoint" (or the closest point to it) is the first entity in the returned list
// "maxdist" is the farthest distance allowed in the returned list
// "mindist" is the nearest distance to be allowed in the returned list
sortByDist(points, startpoint, maxdist, mindist)
{
	if(!isdefined(points))
		return undefined;
	if(!isdefineD(startpoint))
		return undefined;

	if(!isdefined(mindist))
		mindist = -1000000;
	if(!isdefined(maxdist))
		maxdist = 1000000; // almost 16 miles, should cover everything.

	sortedpoints = [];

	max = points.size-1;
	for(i = 0; i < max; i++)
	{
		nextdist = 1000000;
		next = undefined;

		for(j = 0; j < points.size; j++)
		{
			thisdist = distance(startpoint.origin, points[j].origin);
			if(thisdist <= nextdist && thisdist <= maxdist && thisdist >= mindist)
			{
				next = j;
				nextdist = thisdist;
			}
		}

		if(!isdefined(next))
			break; // didn't find one that fit the range, stop trying

		sortedpoints[i] = points[next];

		// shorten the list, fewer compares
		points[next] = points[points.size-1]; // replace the closest point with the end of the list
		points[points.size-1] = undefined; // cut off the end of the list
	}

	sortedpoints[sortedpoints.size] = points[0]; // the last point in the list

	return sortedpoints;
}

PlayerKilled(eInflictor, attacker, iDamage, sMeansOfDeath, sWeapon, vDir, sHitLoc)
{
	self notify("jump_died");

	self cleanupPlayer1();
	if(getCvarInt("ufa_savejump") == 1)
	{
		self notify("end_saveposition_threadsA"); // Professor Inuyasha
	}
	else
	{
		self notify("end_saveposition_threadsB"); // Professor Inuyasha
	}
}

// Done on death/spawn and disconnect
cleanupPlayer1()
{
	// Destroy hud elements
	if(isdefined(self.jump_weaponselectmsg))	self.jump_weaponselectmsg destroy();
	if(isdefined(self.jump_punishtimer))		self.jump_punishtimer destroy();

	// Remove compass objective if present
	if(isdefined(self.jump_objnum))
	{
		objective_delete(self.jump_objnum);
		self.jump_objnum = undefined;
	}

	if(isdefined(self.jump_anchor))
		self.jump_anchor delete();

	// Remove spine marker if present
	if(isdefined(self.jump_spinemarker))
	{
		self.jump_spinemarker unlink();
		self.jump_spinemarker delete();
	}
}

spawnPlayer()
{
	self notify("jump_spawned");
	if(getCvarInt("ufa_savejump") == 1)
	{
		self notify("end_saveposition_threadsA"); // Professor Inuyasha
	}
	else
	{
		self notify("end_saveposition_threadsB"); // Professor Inuyasha
	}

	if(level.savepositions)

	if(!isdefined(self.jump_pace))
		self.jump_pace = 0;

	// Reset flags
	self.jump_disableprimaryb = undefined;
	self.jump_invulnerable = undefined;
	self.jump_warnnade = undefined;
	self cleanupPlayer1();

	// Force weapons
	if(!isdefined(level.jump_classbased))
		self forceWeapons(game[self.pers["team"]]);
	
	// Limit/Randomize ammo
	self ammoLimiting();

	self thread monitorme();
	if(level.jump_grenadewarning)
		self thread warning();

	
	self thread meleeblockon();


	if(getcvar("scr_jump_welcome0") != "")
		self thread showWelcomeMessages();

}

limitAmmo(slot)
{
	if(level.jump_ammomin == 100)
		return;

	if(self getWeaponSlotWeapon(slot) == "panzerfaust_mp")
		return;

	if(!level.jump_ammomax)
		ammopc = 0;
	else if(level.jump_ammomin == level.jump_ammomax)
		ammopc = level.jump_ammomin;
	else
		ammopc = level.jump_ammomin + randomInt(level.jump_ammomax - level.jump_ammomin + 1);

	iAmmo = self getWeaponSlotAmmo(slot) + self getWeaponSlotClipAmmo(slot);
	iAmmo = (int)(iAmmo * ammopc/100 + 0.5);
	
	// If no ammo, remove weapon
	if(!iAmmo)
		self setWeaponSlotWeapon(slot, "none");
	else
	{
		self setWeaponSlotClipAmmo(slot,iAmmo);
		iAmmo = iAmmo - self getWeaponSlotClipAmmo(slot);
		if(iAmmo < 0) iAmmo = 0;	// this should never happen
		self setWeaponSlotAmmo(slot, iAmmo);
	}
}

ammoLimiting()
{
	self limitAmmo("primary");
	self limitAmmo("primaryb");
	self limitAmmo("pistol");

	// Set weapon based grenade count
	if(!isdefined(level.jump_classbased))
	{
		if(level.jump_grenadecount)
			grenadecount = level.jump_grenadecount;
		else
		{
			if(isdefined(self.jump_grenadeforced))
				grenadecount = maps\mp\gametypes\_teams::getWeaponBasedGrenadeCount(self getWeaponSlotWeapon("primary"));
			else
			{
				grenadecount = self getWeaponSlotClipAmmo("grenade");
//				self iprintln("Grenade:" + grenadecount);
			}
		}
	}
	else
	{
		grenadecount = self getWeaponSlotClipAmmo("grenade");
	}


	// If no grenades, remove weapon
	if(!grenadecount)
		self setWeaponSlotWeapon("grenade", "none");
	else
		self setWeaponSlotClipAmmo("grenade", grenadecount);
}


forceWeapons(team)
{

	// Force secondary

		weapon = level.jump_secondaryweapon["default"];
	if(level.jump_panzercount)
	{
		switch(weapon)
		{
			case "disable":
				self.jump_disableprimaryb = false;
				weapon = "panzerfaust_mp";
				break;
			default:
				break;
		}
		self forceWeapon("primaryb", weapon);
	}
}	

forceWeapon(slot, weapon)
{


		 	self setWeaponSlotWeapon(slot, weapon);

//				self setWeaponSlotAmmo(slot, 999);
				self setWeaponSlotClipAmmo(slot, level.jump_panzercount);

			// Print message to player
			self iprintln("You have been equipped with a " + "Panzerfaust 60.");


}


monitorme()
{
	self endon("jump_spawned");
	self endon("jump_died");

	{
		if(getCvarInt("ufa_savejump") == 1)
		{
			self notify("end_saveposition_threadsA"); // Professor Inuyasha
				self thread ufa\master\_jump::_MeleeKeyA();
				self thread ufa\master\_jump::_UseKeyA();
		}
		else
		{
			self notify("end_saveposition_threadsB"); // Professor Inuyasha
				self thread ufa\master\_jump::_MeleeKeyB();
				self thread ufa\master\_jump::_UseKeyB();
		}
	}
	count = 0;
	funcount=0;

	if(isdefined(self.jump_spinemarker))
	{
		self.jump_spinemarker unlink();
		self.jump_spinemarker delete();
	}

	wait .05;

	self.jump_spinemarker = spawn("script_origin",(0,0,0));
	self.jump_spinemarker linkto (self, "bip01 spine2",(0,0,0),(0,0,0));	

	while( isPlayer(self) && isAlive(self) && self.sessionstate=="playing" )
	{

		// Disable primaryb?		
		if(isdefined(self.jump_disableprimaryb))
		{
			primaryb = self getWeaponSlotWeapon("primaryb");
			if (primaryb != "none")
			{
				//player picked up a weapon
				primary = self getWeaponSlotWeapon("primary");
				if (primary != "none")
				{
					//drop primary weapon if he's carrying one already
					self dropItem(primary);
				}	

				//remove the weapon from the primary b slot
				self setWeaponSlotWeapon("primaryb", "none");
				self.pers["weapon2"] = undefined;

				//put the picked up weapon in primary slot
				self setWeaponSlotWeapon("primary", primaryb);
				self.pers["weapon1"] = primaryb;
				self switchToWeapon(primaryb);
			} 
		}

		// Calculate current speed
		oldpos = self.origin;
		wait 1;				// Wait 2 seconds
		newpos = self.origin;
		speed = distance(oldpos,newpos);

		if (speed > 20)
			self.jump_pace = 1;
		else
			self.jump_pace = 0;

	}
	if(isdefined(self.jump_spinemarker))
	{
		self.jump_spinemarker unlink();
		self.jump_spinemarker delete();
	}
}

PlayerDisconnect()
{
	self notify("jump_died");
	self notify("jump_spawned");

	self cleanupPlayer1();

}

overrideteams()
{
	if(isdefined(level.jump_classbased))
		return;

	// It it's the same map and gametype, use old values to avoid non precached models
	if( getcvar("mapname") == getcvar("jump_oldmap") && getcvar("g_gametype") == getcvar("jump_oldgt") )
	{
		game["allies"] = getcvar("jump_allies");
		game[game["allies"] + "_soldiertype"] 	= getcvar("jump_soldiertype");
		game[game["allies"] + "_soldiervariation"]= getcvar("jump_soldiervariation");
		if(game["allies"] == "american" && game[game["allies"] + "_soldiervariation"] == "winter")
		{
			game["german_soldiertype"] = "wehrmacht";
			game["german_soldiervariation"] = "winter";
		}
		return;
	}

	// Override allies team
	switch(level.jump_teamallies)
	{
		case "american":
		case "british":
		case "german":
		case "russian":
			game["allies"] = level.jump_teamallies;
			break;

		case "random":
			allies = [];
			oldteam = getcvar("jump_allies");
			if(oldteam != "american")	allies[allies.size] = "american";
			if(oldteam != "british")	allies[allies.size] = "british";
			if(oldteam != "russian")	allies[allies.size] = "russian";
			game["allies"] = allies[randomInt(allies.size)];
			break;

		default:
			break;
	}

	if(!isdefined(game[ game["allies"] + "_soldiertype" ]))
	{
		switch(game["allies"])
		{
			case "american":
				if(isdefined(level.jump_wintermap))
				{
					game["american_soldiertype"] = "airborne";
					game["american_soldiervariation"] = "winter";
					game["german_soldiertype"] = "wehrmacht";
					game["german_soldiervariation"] = "winter";
				}
				else	
				{
					game["american_soldiertype"] = "airborne";
					game["american_soldiervariation"] = "normal";
				}
				break;

			case "british":
				if(isdefined(level.jump_wintermap))
				{
					game["british_soldiertype"] = "commando";
					game["british_soldiervariation"] = "winter";
				}
				else
				{
					switch(randomInt(2))
					{
						case 0:
							game["british_soldiertype"] = "airborne";
							game["british_soldiervariation"] = "normal";
							break;
	
						default:
							game["british_soldiertype"] = "commando";
							game["british_soldiervariation"] = "normal";
							break;
					}
				}
				break;

			case "russian":
				if(isdefined(level.jump_wintermap))
				{
					switch(randomInt(2))
					{
						case 0:
							game["russian_soldiertype"] = "conscript";
							game["russian_soldiervariation"] = "winter";
							break;

						default:
							game["russian_soldiertype"] = "veteran";
							game["russian_soldiervariation"] = "winter";
							break;
					}
				}
				else
				{
					switch(randomInt(2))
					{
						case 0:
							game["russian_soldiertype"] = "conscript";
							game["russian_soldiervariation"] = "normal";
							break;


						default:
							game["russian_soldiertype"] = "veteran";
							game["russian_soldiervariation"] = "normal";
							break;

					}
				}
				break;
		}
	}

	// Save stuff for reinitializing in roundbased gametypes
	setcvar("jump_oldgt",	getcvar("g_gametype") );
	setcvar("jump_oldmap",	getcvar("mapname") );
	setcvar("jump_allies",			game["allies"] );
	setcvar("jump_soldiertype", 		game[game["allies"] + "_soldiertype"] );
	setcvar("jump_soldiervariation",	game[game["allies"] + "_soldiervariation"] );
}

showlogo()
{

logotext = &"^3Jump Mod ^1Final ^2enabled";


	if (level.jump_showlogo) //Show Logo
	{
		if(isdefined(level.logo))
			level.logo destroy();

		level.logo = newHudElem();	
		level.logo.x = 631;
		level.logo.y = 475;
		level.logo.alignX = "right";
		level.logo.alignY = "middle";
		level.logo.sort = -3;
		level.logo.alpha = 1;
		level.logo.fontScale = 0.6;
		level.logo.archived = true;
		level.logo setText(logotext);
	}
}

PlayerinRange(range)
{
	if(!range)
		return true;

	// Get all players and pick out the ones that are playing
	allplayers = getentarray("player", "classname");
	players = [];
	for(i = 0; i < allplayers.size; i++)
	{
		if(allplayers[i].sessionstate == "playing")
			players[players.size] = allplayers[i];
	}

	// Get the players that are in range
	sortedplayers = sortByDist(players, self);

	// Need at least 2 players (myself + one team mate)
	if(sortedplayers.size<2)
		return false;

	// First player will be myself so check against second player
	distance = distance(self.origin, sortedplayers[1].origin);
	if( distance <= range )
		return true;
	else
		return false;
}

isGrenade(weapon)
{
	switch(weapon)
	{
		case "fraggrenade_mp":
		case "mk1britishfrag_mp":
		case "rgd-33russianfrag_mp":
		case "stielhandgranate_mp":
//		case "smokegrenade_mp":
			return true;
			break;

		default:
			return false;
			break;
	}
}

warning()
{
	self endon("jump_spawned");
	self endon("jump_died");

	// Loop
	while (isAlive(self) && self.sessionstate == "playing")
	{
		// Wait
		wait .05;

		// get current weapon
		cw = self getCurrentWeapon();
		attackButton = self attackButtonPressed();

		// Is the current weapon a grenade?
		if(attackButton && isGrenade(cw))
			self thread warningyell();


			self thread meleeblock();
	}
}

meleeblockon()
{
	self endon("jump_spawned");
	self endon("jump_died");

	// Loop
	while (isAlive(self) && self.sessionstate == "playing")
	{
			wait .05;
			self thread meleeblock();
	}
}

meleeblock()
{
	self endon("jump_spawned");
	self endon("jump_died");
	if(self PlayerinRange(100))
	{
		if(isdefined(self.pers["jump_notified"])) return;
		self.pers["jump_notified"] = true;
		wait(0.25);
		self.jump_invulnerable = true;
	}
	else
	{
		self.jump_invulnerable = undefined;
		self.pers["jump_notified"] = undefined;
	}
}


warningyell()
{
	if(isdefined(self.jump_warnnade)) return;
	self.jump_warnnade = true;

	self endon("jump_spawned");
	self endon("jump_died");

 
		while(self attackButtonPressed() && isGrenade( self getCurrentWeapon() ) && isAlive(self) && self.sessionstate == "playing")
			wait .05;

	// Thrown a grenade?
	if(isGrenade(self getCurrentWeapon()) && !self attackButtonPressed() && level.jump_grenadewarning && isAlive(self) && self.sessionstate == "playing")
	{
		if( (level.jump_grenadewarning) && self PlayerinRange(level.jump_grenadewarningrange) )
		{
			// Yell "Grenade!"
			soundalias = level.jump_grenadevoices[ game[ self.pers["team"] ] ][randomInt(level.jump_grenadevoices[ game[ self.pers["team"] ] ].size)];
			self playsound(soundalias);
		}
	}
	self.jump_warnnade = undefined;
}