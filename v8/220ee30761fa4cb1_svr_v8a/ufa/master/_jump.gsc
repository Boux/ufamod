main()
{
	self endon("end_saveposition_threadsA");
	{
		self iprintln("^9]^3[{+melee}]^9[^7: ^62^1x^7Press to ^1Save");
		self iprintln("^9]^3[{+activate}]^9[^7: ^62^1x^7Press to ^2Load");
	}
	self endon("end_saveposition_threadsB");
	{
		self iprintln("^9]^3[{+melee}]^9[^7: ^62^1x^7Press to ^1Save");
		self iprintln("^9]^3[{+activate}]^9[^7: ^62^1x^7Press to ^2Load");
	}
}
doHUDMessages()
{
	wait 1;
	self iprintlnbold("^9]^3[{+melee}]^9[^7: ^62^1x^7Press to ^1Save");
	self iprintlnbold("^9]^3[{+activate}]^9[^7: ^62^1x^7Press to ^2Load");
}
_MeleeKeyA() // for saving position
{
	self endon("end_saveposition_threadsA");
	for(;;)
	{
		if(self meleeButtonPressed())
		{
			catch_next = false;
			for(i=0; i<=0.25; i+=0.01)
			{
				if(catch_next && self meleeButtonPressed() && self.sessionstate == "playing")
				{
					self thread savePos();
					wait 1;
					break;
				}
				else if(!(self meleeButtonPressed()))
					catch_next = true;
				wait 0.01;
			}
		}
		wait 0.05;
	}
}
_UseKeyA() // for loading position
{
	self endon("end_saveposition_threadsA");
	for(;;)
	{
		if(self useButtonPressed())
		{
			catch_next = false;
			for(i=0; i<=0.25; i+=0.01)
			{
				if(catch_next && self useButtonPressed() && self.sessionstate == "playing")
				{
					self thread loadPos();
					wait 1;
					break;
				}
				else if(!(self useButtonPressed()))
				{
					catch_next = true;
				}
				wait 0.01;
			}
		}
		wait 0.05;
	}
}
_MeleeKeyB() // for saving position
{
	self endon("end_saveposition_threadsB");
	for(;;)
	{
		if(self meleeButtonPressed())
		{
			catch_next = false;
			for(i=0; i<=0.25; i+=0.01)
			{
				if(catch_next && self meleeButtonPressed() && self.sessionstate == "playing")
				{
					if(self isOnground())
					{
						self thread savePos();
						wait 1;
						break;
					}
					else if (!(self isOnGround()) && self meleeButtonPressed())
					{
						self iprintln("^1Y^7ou must be on the ground to save");
						wait 1;
					}
				}
				else if(!(self meleeButtonPressed()))
					catch_next = true;
				wait 0.01;
			}
		}
		wait 0.05;
	}

}
_UseKeyB() // for loading position
{
	self endon("end_saveposition_threadsB");
	for(;;)
	{
		if(self useButtonPressed())
		{
			catch_next = false;
			for(i=0; i<=0.25; i+=0.01)
			{
				if(catch_next && self useButtonPressed() && self.sessionstate == "playing")
				{
					if(self isOnground())
					{
						self thread loadPos();
						wait 1;
						break;
					}
					else if (!(self isOnGround()))
					{
						self iprintln("^1Y^7ou must be on the ground to load");
						wait 1;
					}
				}
				else if(!(self useButtonPressed()))
				{
					catch_next = true;
				}
				wait 0.01;
			}
		}
		wait 0.05;
	}
}
loadPos()
{
	//thread positions();
	if(!isDefined(self.saved_origin))
	{
		self iprintlnbold("^1T^7here is no previous position to load");
		return;
	}
	else
	{
		if(self positions(55))
		{
			self iprintlnbold("^1P^7osition ^1occupied");
			self iprintlnbold("^2P^7lease wait!");
			return;
		}
		else
		{
			self setPlayerAngles(self.saved_angles); // angles need to come first
			self setOrigin(self.saved_origin);
			self iprintln("^3L^7oaded ^3P^7osition^0: ^7[^1" + (int)self.saved_origin[0] + "^7] [^2" + (int)self.saved_origin[1] + "^7] [^4" + (int)self.saved_origin[2] + "^7]");
			self.totalLoads++; // for new feature
			self maps\mp\gametypes\jump::jumpHudText(); // for new feature
		}
	}
}
savePos()
{
	if(isDefined(self.savedtwo_origin))
	{
		self.savedthree_origin = self.savedtwo_origin;
		self.savedthree_angles = self.savedtwo_angles;
	}
	if(isDefined(self.saved_origin))
	{
		self.savedtwo_origin = self.saved_origin;
		self.savedtwo_angles = self.saved_angles;
	}
	self.saved_origin = self.origin;
	self.saved_angles = self.angles;
	self iprintln("^3N^7ew ^3P^7osition^0: ^7[^1" + (int)self.saved_origin[0] + "^7] [^2" + (int)self.saved_origin[1] + "^7] [^4" + (int)self.saved_origin[2] + "^7]");
	self.totalSaves++; // for new feature
	self maps\mp\gametypes\jump::jumpHudText(); // for new feature}
}
positions(range)
{
	if(!range)
		return true;
	// Get all players and pick out the ones that are playing
	allplayers = getentarray("player", "classname");
	players = [];
	for(i = 0; i < allplayers.size; i++)
	{
		if(allplayers[i].sessionstate == "playing")
			players[players.size] = allplayers[i];
	}
	// Get the players that are in range
	sortedplayers = sortByDist(players, self);
	// Need at least 2 players (myself + one team mate)
	if(sortedplayers.size<2)
		return false;
	// First player will be myself so check against second player
	distance = distance(self.saved_origin, sortedplayers[1].origin);
	if( distance <= range )
		return true;
	else
		return false;
}
sortByDist(points, startpoint, maxdist, mindist)
{
	if(!isdefined(points))
		return undefined;
	if(!isdefineD(startpoint))
		return undefined;
	if(!isdefined(mindist))
		mindist = -1000000;
	if(!isdefined(maxdist))
		maxdist = 1000000; // almost 16 miles, should cover everything.
	sortedpoints = [];
	max = points.size-1;
	for(i = 0; i < max; i++)
	{
		nextdist = 1000000;
		next = undefined;
		for(j = 0; j < points.size; j++)
		{
			thisdist = distance(startpoint.origin, points[j].origin);
			if(thisdist <= nextdist && thisdist <= maxdist && thisdist >= mindist)
			{
				next = j;
				nextdist = thisdist;
			}
		}
		if(!isdefined(next))
			break; // didn't find one that fit the range, stop trying
		sortedpoints[i] = points[next];
		// shorten the list, fewer compares
		points[next] = points[points.size-1]; // replace the closest point with the end of the list
		points[points.size-1] = undefined; // cut off the end of the list
	}
	sortedpoints[sortedpoints.size] = points[0]; // the last point in the list
	return sortedpoints;
}