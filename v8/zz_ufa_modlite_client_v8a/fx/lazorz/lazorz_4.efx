Particle
{
	name				smoke_linger_mist

	flags				useAlpha

	spawnFlags			rgbComponentInterpolation

	count				5 7

	life				2000 3000

	delay				100 500

	cullrange			32000

	origin				-15 -25 -25 300 25 25

	rotation			0 360

	rotationDelta		-10 10

	velocity			-20 -90 -90 140 90 90

	acceleration		-15 -15 -15 15 15 15

	gravity				0 25

	rgb
	{
		start			0.1 0.6 0.8
		end				0.2 0.3 0.8
		flags			linear
	}

	alpha
	{
		end				0
		parm			0.15 0.5
		flags			linear
	}

	size
	{
		start			50 100
		end				150 200
		flags			linear
	}

	length
	{
		flags			linear
	}

	shaders
	[
		smoke/smk_p_none_wht_a
		smoke/smk_p_none_wht_b
		smoke/smk_p_none_wht_c
	]
}

Particle
{
	name				smoke_shroom

	flags				clampRotation useAlpha

	spawnFlags			oppositeRotation

	count				5

	life				1500 2000

	delay				200 600

	cullrange			6000

	origin				350 -30 -30 550 30 30

	rotation			0 360

	rotationDelta		-5 5

	rotationAccel		5

	rotationClamp		-5 5

	velocity			85 -170 -170 400 170 170

	acceleration		0 -25 -25 -50 25 25

	gravity				-100 -225

	rgb
	{
		start			0.1 0.1 0.8
		end				0.1 0.1 0.8
		flags			linear
	}

	alpha
	{
		end				0
		parm			0.15 0.25
		flags			linear
	}

	size
	{
		start			100 125
		end				150 200
		parm			50 75
		flags			linear
	}

	length
	{
		flags			linear
	}

	shaders
	[
		smoke/smk_p_out_wht_a
		smoke/smk_p_out_wht_b
		smoke/smk_p_out_wht_c
	]
}

Decal
{
	name				scortch_mark

	life				5000

	delay				50 75

	cullrange			6000

	rotation			0 360

	rgb
	{
		start			0.1 0.1 0.8
		end				0.1 0.1 0.8
		flags			linear
	}

	alpha
	{
		start			0.5 1
	}

	size
	{
		start			16 32
	}

	shaders
	[
		impact/scorch_concrete_a
		impact/scorch_concrete_b
	]
}

Particle
{
	name				smoke_plumer_shaft

	flags				useAlpha

	count				5 8

	life				1000 1200

	delay				125 150

	cullrange			32000

	origin				-15 -25 -25 425 25 25

	rotation			-15 15

	rotationDelta		-5 5

	velocity			245 -125 -125 660 125 125

	gravity				0 25

	rgb
	{
		start			0.3 0.8 0.8
		end				0.1 0.1 0.8
		flags			linear
	}

	alpha
	{
		end				0
		parm			0.15 0.5
		flags			linear
	}

	size
	{
		start			500 62.5
		end				200 300
		flags			linear
	}

	length
	{
		flags			linear
	}

	shaders
	[
		smoke/smk_p_top_wht_a
		smoke/smk_p_top_wht_b
		smoke/smk_p_top_wht_c
	]
}

Particle
{
	name				smoke_plume_suckin

	flags				useAlpha

	count				2

	life				125 200

	delay				100

	cullrange			4000

	origin				-50 -100 -100 50 100 100

	rotation			-5 5

	rotationDelta		-5 5

	velocity			0 -40 -40 300 40 40

	gravity				0 100

	rgb
	{
		start			0.1 0.1 0.8
		end				0.1 0.5 0.8
		flags			linear
	}

	alpha
	{
		start			0.5 1
		end				0
		parm			0.15 0.5
		flags			linear
	}

	size
	{
		start			150 225
		end				50 100
		parm			50
		flags			linear
	}

	length
	{
		flags			linear
	}

	shaders
	[
		smoke/smk_p_out_wht_a
		smoke/smk_p_out_wht_b
		smoke/smk_p_out_wht_c
	]
}

Particle
{
	name				smoke_shock_out

	flags				useAlpha

	spawnFlags			evenDistribution

	count				5

	life				300

	delay				25

	cullrange			4000

	origin				-50 -50 -50 0 50 50

	rotation			-5 5

	rotationDelta		-5 5

	velocity			0 -790 -790 470 790 790

	gravity				0 100

	rgb
	{
		start			0.23 0.6 0.8
		end				0.32 0.5 0.7
		flags			linear
	}

	alpha
	{
		start			0.5 1
		end				0
		parm			0.15 0.5
		flags			linear
	}

	size
	{
		start			37.5 50
		end				175 225
		parm			50
		flags			linear
	}

	length
	{
		flags			linear
	}

	shaders
	[
		smoke/smk_p_out_wht_a
		smoke/smk_p_out_wht_b
		smoke/smk_p_out_wht_c
	]
}

Particle
{
	name				dirt_plume

	flags				useAlpha

	nonUniformScale		1

	count				4

	life				2200 3200

	delay				125 150

	cullrange			6000

	origin				-15 -25 -25 325 25 25

	rotation			-5 5

	rotationDelta		-1 1

	velocity			170 -40 -40 585 40 40

	gravity				-600 -450

	rgb
	{
		start			0.2 0.1 0.8
		end				0.1 0.4 0.8
		flags			linear
	}

	alpha
	{
		end				0
		parm			0.15 0.5
		flags			linear
	}

	size
	{
		start			12.5 25
		end				200 250		flags			linear
	}

	size2
	{
		start			50 100
		end				350
		flags			linear
	}

	length
	{
		flags			linear
	}

	shaders
	[
		smoke/smk_t_wht_b
		smoke/smk_t_wht_c
	]
}

Particle
{
	name				gib_dirt

	flags				usePhysics

	count				5

	life				1000 2000

	delay				250 500

	cullrange			2000

	bounce				0.25 0.5

	origin				0 -30 -30 300 30 30

	rotation			0 360

	rotationDelta		-45 45

	velocity			170 -240 -240 770 240 240

	gravity				-800 -500

	rgb
	{
		start			0.1 0.1 0.8
		end				0.1 0.4 0.8
		flags			linear
	}

	size
	{
		start			1 4
		end				0
		parm			90 80
		flags			nonlinear
	}

	shaders
	[
		debree/debree_gib_concrete_a
		debree/debree_gib_concrete_b
	]
}

Tail
{
	name				dirt_eject

	flags				useAlpha

	count				7

	life				450 550

	delay				50 150

	cullrange			4000

	origin				-25 -50 -50 100 50 50

	velocity			990 -770 -770 1750 770 770

	gravity				-600 -400

	rgb
	{
		start			0.1 0.1 0.6
		end				0.1 0.1 0.5
		flags			linear
	}

	alpha
	{
		end				0
		parm			0.15 0.25
		flags			linear
	}

	size
	{
		start			25 50
		end				125 100
		parm			0.15 0.25
		flags			linear
	}

	length
	{
		start			200
		end				1600
		flags			linear
	}

	shaders
	[
		debree/debree_t_gen_a
		debree/debree_t_gen_b
		debree/debree_t_gen_c
	]
}

Particle
{
	name				debree_p

	flags				useAlpha

	count				5

	life				2500 3000

	delay				125 150

	cullrange			4000

	origin				-15 -25 -25 0 25 25

	rotation			0 360

	rotationDelta		-10 10

	velocity			240 -20 -20 600 20 20

	gravity				-800 -400

	rgb
	{
		start			0.1 0.5 0.5
		end				0.2 0.2 0.65
		parm			0.25 0.75
		flags			linear
	}

	alpha
	{
		end				0
		parm			0.15
		flags			linear
	}

	size
	{
		start			50
		end				200
		flags			linear
	}

	length
	{
		flags			linear
	}

	shaders
	[
		debree/debree_plg_concrete_a
		debree/debree_plg_concrete_b
		debree/debree_plg_concrete_c
	]
}

Line
{
	name				smoke_line

	flags				useAlpha

	count				5 8

	life				350 850

	delay				0 150

	cullrange			4000

	origin2				600 -400 -400 600 400 400

	rgb
	{
		start			0.1 0.5 0.8
		end				0.1 0.1 0.45
		flags			linear
	}

	alpha
	{
		end				0
		parm			0.25 0.5
		flags			linear
	}

	size
	{
		end				50 125
		flags			linear
	}

	shaders
	[
		smoke/smk_l_wht_a
		smoke/smk_l_wht_b
		smoke/smk_l_wht_c
	]
}

Tail
{
	name				dirt_eject_2

	flags				useAlpha

	spawnFlags			rgbComponentInterpolation

	count				5

	life				450 550

	delay				50 75

	cullrange			4000

	origin				0 -50 -50 50 50 50

	velocity			1250 -770 -770 1850 770 770

	gravity				-600 -400

	rgb
	{
		start			0.1 0.1 0.5
		end				0.1 0.5 0.8
		flags			linear
	}

	alpha
	{
		end				0
		parm			0.15 0.25
		flags			linear
	}

	size
	{
		start			12.5 25
		end				75
		parm			0.15 0.25
		flags			linear
	}

	length
	{
		start			200
		end				1000
		flags			linear
	}

	shaders
	[
		debree/debree_t_gen_a
		debree/debree_t_gen_b
		debree/debree_t_gen_c
	]
}

Particle
{
	name				_effect_base

	flags				useAlpha

	count				3

	life				2000 2500

	delay				50

	cullrange			32000

	origin				-15 -150 -150 25 150 150

	rotation			-15 15

	rotationDelta		-5 5

	velocity			-20 -20 -20 20 20 20

	gravity				0 25

	rgb
	{
		start			0.1 0.6 0.6
		end				0.1 0.1 0.6
		flags			linear
	}

	alpha
	{
		end				0
		parm			0.15 0.25
		flags			linear
	}

	size
	{
		start			50 62.5
		end				200 300
		flags			linear
	}

	length
	{
		flags			linear
	}

	shaders
	[
		smoke/smk_p_top_wht_a
		smoke/smk_p_top_wht_b
		smoke/smk_p_top_wht_c
	]
}

Particle
{
	name				fire_exp_core

	count				1 2

	life				100 250

	cullrange			32000

	origin				-25 0 0 0 0 0

	rotation			0 360

	rotationDelta		-45 45

	velocity			0 -20 -20 80 20 20

	rgb
	{
		start			0.1 0.2 0.6
		end				0.1 0.2 0.3
		flags			linear
	}

	alpha
	{
		start			0.25 0.5
		end				0
		parm			50 75
		flags			nonlinear
	}

	size
	{
		end				75 112.5
		flags			linear
	}

	shaders
	[
		fire/exp_gen_a
		fire/exp_gen_b
		fire/exp_gen_c
		fire/fire_ball_a
		fire/fire_ball_b
		fire/fire_ball_c
	]
}

Light
{
	life				600 1500

	origin				0 0 0

	rgb
	{
		start			0.1 0.9 0.95
		end				0.2 0.45 0.8
	}

	size
	{
		start			200 400
		end				750 1200
		flags			linear
	}

	alpha
	{
		end				0
		parm			0.15 0.25
		flags			linear
	}
}

Particle
{
	name				fire

	flags				setShaderTime

	spawnFlags			evenDistribution rgbComponentInterpolation affectedByWind

	count				5 7

	life				225 450

	delay				0 100

	cullrange			16000

	origin				0 0 0

	radius				60 50

	rotation			0 360

	velocity			-16 -16 -16 16 16 16

	rotationDelta		-15 15

	rgb
	{
		start			0.1 0.9 0.9
		end				0.3 0.40 0.75
		flags			linear
	}

	alpha
	{
		start			0.35 0.50
		end				0
		parm			0.15 0.25
		flags			linear
	}

	size
	{
		start			3 5
		end				192.5 125
		flags			linear
	}

	shaders
	[
		fire/fire_ball_a
		fire/fire_ball_b
		fire/fire_ball_c
	]
}
