////////////////////////////////////////////////////////
////////////// Blood Splats ////////////////////////////
////////////////////////////////////////////////////////

XtremE/bloodsplat1
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodsplat1
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
//	{
//		perlight
//		map gfx/fear_blood/bloodsplat1
//		blendfunc GL_SRC_ALPHA GL_ONE
//		rgbGen vertex
//		nextbundle
//		map $dlight
//	}
}

XtremE/bloodsplat2
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodsplat2
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
//	{
//		perlight
//		map gfx/fear_blood/bloodsplat2
//		blendfunc GL_SRC_ALPHA GL_ONE
//		rgbGen vertex
//		nextbundle
//		map $dlight
//	}
}

XtremE/bloodsplat3
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodsplat3
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
//	{
//		perlight
//		map gfx/fear_blood/bloodsplat3
//		blendfunc GL_SRC_ALPHA GL_ONE
//		rgbGen vertex
//		nextbundle
//		map $dlight
//	}
}


XtremE/bloodsplat4
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodsplat4
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
//	{
//		perlight
//		map gfx/fear_blood/bloodsplat4
//		blendfunc GL_SRC_ALPHA GL_ONE
//		rgbGen vertex
//		nextbundle
//		map $dlight
//	}
}

XtremE/bloodsplat5
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodsplat5
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
//	{
//		perlight
//		map gfx/fear_blood/bloodsplat5
//		blendfunc GL_SRC_ALPHA GL_ONE
//		rgbGen vertex
//		nextbundle
//		map $dlight
//	}
}


XtremE/bloodsplat6
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodsplat6
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
//	{
//		perlight
//		map gfx/fear_blood/bloodsplat6
//		blendfunc GL_SRC_ALPHA GL_ONE
//		rgbGen vertex
//		nextbundle
//		map $dlight
//	}
}

gore/chunk_splat
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/effects/gore/chunk_splat
		blendfunc gl_zero gl_one_minus_src_color
	}
}

gore/chunk_splat2
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/effects/gore/chunk_splat2
		blendfunc gl_zero gl_one_minus_src_color
	}
}

gore/chunk_splat3
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/effects/gore/chunk_splat3
		blendfunc gl_zero gl_one_minus_src_color
	}
}


////////////////////////////////////////////////////////
////////////// Blood Pools /////////////////////////////
////////////////////////////////////////////////////////

gfx/fear_blood/bloodpool1
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodpool1
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
	{
		perlight
		map gfx/fear_blood/bloodpool1
		blendfunc GL_SRC_ALPHA GL_ONE
		rgbGen vertex
		nextbundle
		map $dlight
	}
}

gfx/fear_blood/bloodpool2
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodpool2
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
	{
		perlight
		map gfx/fear_blood/bloodpool2
		blendfunc GL_SRC_ALPHA GL_ONE
		rgbGen vertex
		nextbundle
		map $dlight
	}
}

death_blood_puddle1
{
	entityMergable
	polygonOffset2
	{
		map gfx/effects/blood_puddle1
		blendfunc blend
		rgbGen vertex 
	}
}

death_blood_puddle2
{
	entityMergable
	polygonOffset2
	{
		map gfx/effects/blood_puddle2
		blendfunc blend
		rgbGen vertex 
	}
}



////////////////////////////////////////////////////////
////////////// Blood Spray/Mists ///////////////////////
////////////////////////////////////////////////////////

gfx/fear_blood/bloodmist1
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodmist1
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
	{
		perlight
		map gfx/fear_blood/bloodmist1
		blendfunc GL_SRC_ALPHA GL_ONE
		rgbGen vertex
		nextbundle
		map $dlight
	}
}

gfx/fear_blood/bloodmist2
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodmist2
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
	{
		perlight
		map gfx/fear_blood/bloodmist2
		blendfunc GL_SRC_ALPHA GL_ONE
		rgbGen vertex
		nextbundle
		map $dlight
	}
}



////////////////////////////////////////////////////////
////////////// Other Blood Stuff ///////////////////////
////////////////////////////////////////////////////////

gore/blood_trail
{
	entityMergable
	{
		map gfx/effects/gore/blood_trail.tga
		blendfunc blend
		rgbGen vertex
	}
}

Anim_bloodspray
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		AnimMap 10
		map gfx/fear_blood/bloodsplat1
		map gfx/fear_blood/bloodsplat2
		map gfx/fear_blood/bloodsplat3
		map gfx/fear_blood/bloodsplat4
		map gfx/fear_blood/bloodsplat5
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
}



////////////////////////////////////////////////////////
////////////// Puke ////////////////////////////////////
////////////////////////////////////////////////////////

gfx/merciless/puke_pool
{
	entityMergable
	polygonOffset2
	{
		map gfx/merciless/puke_pool1
		blendfunc blend
		rgbGen vertex 
	}
}

gfx/merciless/pukesplash1
{
	entityMergable
	polygonOffset2
	{
		map gfx/merciless/pukesplash1
		blendfunc blend
		rgbGen vertex 
	}
}

gfx/merciless/pukesplash2
{
	entityMergable
	polygonOffset2
	{
		map gfx/merciless/pukesplash2
		blendfunc blend
		rgbGen vertex 
	}
}

gfx/merciless/pukesplash3
{
	entityMergable
	polygonOffset2
	{
		map gfx/merciless/pukesplash3
		blendfunc blend
		rgbGen vertex 
	}
}




gfx/beam_aqua_01
{
	entityMergable
	polygonOffset2
	{
		map gfx/beam_aqua_01
		blendfunc blend
		rgbGen vertex 
	}
}

gfx/beam_plasma_d
{
	entityMergable
	polygonOffset2
	{
		map gfx/beam_plasma_d
		blendfunc blend
		tcMod Scroll 0 -2
		rgbGen vertex 
	}
}

gfx/beam_nail_d
{
	entityMergable
	polygonOffset2
	{
		map gfx/beam_nail_d
		blendfunc blend
		rgbGen vertex 
	}
}

gfx/beam_blue_02
{
	entityMergable
	polygonOffset2
	{
		map gfx/beam_blue_02
		blendfunc blend
		rgbGen vertex 
	}
}

gfx/beam_aqua_02
{
	entityMergable
	polygonOffset2
	{
		map gfx/beam_aqua_02
		blendfunc blend
		rgbGen vertex 
	}
}

gfx/beam_electric
{
	entityMergable
	polygonOffset2
	{
		map gfx/beam_electric
		blendfunc blend
		rgbGen vertex 
	}
}

gfx/beam_missile_d2
{
	entityMergable
	polygonOffset2
	{
		map gfx/beam_missile_d2
		blendfunc blend
		rgbGen vertex 
	}
}

XtremE/ion_decal
{
	entityMergable
	polygonOffset2
	{
		map gfx/effects/explosion/explosion_wisps1
		blendfunc blend
		rgbGen vertex 
	}
}

XtremE/cloudflash1
{
	entityMergable
	sort	additive
	cull	disable
//	cull    none
	nofog
	{
		map gfx/effects/cloudflash1
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}

XtremE/cloudflash1a
{
	entityMergable
	sort    additive
	cull    disable
//	cull    none
	nofog
	{
		map gfx/effects/cloudflash1a
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}