////////////////////////////////////////////////////////
////////////// Blood Splats ////////////////////////////
////////////////////////////////////////////////////////

zombie/bloodsplat1
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodsplat1
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
//	{
//		perlight
//		map gfx/fear_blood/bloodsplat1
//		blendfunc GL_SRC_ALPHA GL_ONE
//		rgbGen vertex
//		nextbundle
//		map $dlight
//	}
}

zombie/bloodsplat2
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodsplat2
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
//	{
//		perlight
//		map gfx/fear_blood/bloodsplat2
//		blendfunc GL_SRC_ALPHA GL_ONE
//		rgbGen vertex
//		nextbundle
//		map $dlight
//	}
}

zombie/bloodsplat3
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodsplat3
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
//	{
//		perlight
//		map gfx/fear_blood/bloodsplat3
//		blendfunc GL_SRC_ALPHA GL_ONE
//		rgbGen vertex
//		nextbundle
//		map $dlight
//	}
}


zombie/bloodsplat4
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodsplat4
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
//	{
//		perlight
//		map gfx/fear_blood/bloodsplat4
//		blendfunc GL_SRC_ALPHA GL_ONE
//		rgbGen vertex
//		nextbundle
//		map $dlight
//	}
}

zombie/bloodsplat5
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodsplat5
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
//	{
//		perlight
//		map gfx/fear_blood/bloodsplat5
//		blendfunc GL_SRC_ALPHA GL_ONE
//		rgbGen vertex
//		nextbundle
//		map $dlight
//	}
}


zombie/bloodsplat6
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodsplat6
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
//	{
//		perlight
//		map gfx/fear_blood/bloodsplat6
//		blendfunc GL_SRC_ALPHA GL_ONE
//		rgbGen vertex
//		nextbundle
//		map $dlight
//	}
}

gore/chunk_splat
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/effects/gore/chunk_splat
		blendfunc gl_zero gl_one_minus_src_color
	}
}

gore/chunk_splat2
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/effects/gore/chunk_splat2
		blendfunc gl_zero gl_one_minus_src_color
	}
}

gore/chunk_splat3
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/effects/gore/chunk_splat3
		blendfunc gl_zero gl_one_minus_src_color
	}
}


////////////////////////////////////////////////////////
////////////// Blood Pools /////////////////////////////
////////////////////////////////////////////////////////

death_blood_puddle1
{
	entityMergable
	polygonOffset2
	{
		map gfx/effects/blood_puddle1
		blendfunc blend
		rgbGen vertex 
	}
}

death_blood_puddle2
{
	entityMergable
	polygonOffset2
	{
		map gfx/effects/blood_puddle2
		blendfunc blend
		rgbGen vertex 
	}
}



////////////////////////////////////////////////////////
////////////// Blood Spray/Mists ///////////////////////
////////////////////////////////////////////////////////

gfx/fear_blood/bloodmist1
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodmist1
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
	{
		perlight
		map gfx/fear_blood/bloodmist1
		blendfunc GL_SRC_ALPHA GL_ONE
		rgbGen vertex
		nextbundle
		map $dlight
	}
}

gfx/fear_blood/bloodmist2
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/fear_blood/bloodmist2
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
	{
		perlight
		map gfx/fear_blood/bloodmist2
		blendfunc GL_SRC_ALPHA GL_ONE
		rgbGen vertex
		nextbundle
		map $dlight
	}
}

////////////////////////////////////////////////////////
////////////// Other Stuff /////////////////////////////
////////////////////////////////////////////////////////

zombie/cloudflash1
{
	entityMergable
	sort	additive
	cull	disable
//	cull    none
	nofog
	{
		map gfx/effects/cloudflash1
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}

zombie/cloudflash1a
{
	entityMergable
	sort    additive
	cull    disable
//	cull    none
	nofog
	{
		map gfx/effects/cloudflash1a
		blendFunc GL_ONE GL_ONE
		rgbGen vertex
	}
}

plasma_ball1
{
	entityMergable
//	sort additive
	{
		map gfx/weapons/plasma/plasma_ball1
		blendfunc add
		rgbGen vertex
	}
}

plasma_ball2
{
	entityMergable
//	sort additive
	{
		map gfx/weapons/plasma/plasma_ball2
		blendfunc add
		rgbGen vertex
	}
}

plasma_ball3
{
	entityMergable
//	sort additive
	{
		map gfx/weapons/plasma/plasma_ball3
		blendfunc add
		rgbGen vertex
	}
}

impact/plasma
{
	entityMergable
	surfaceparm nonsolid
	surfaceparm trans
	polygonOffset2
	{
		map gfx/weapons/plasma/impact_plasma
		blendfunc blend
		rgbGen vertex
		nextbundle
		map $lightmap
	}
	{
		perlight
		map gfx/weapons/plasma/impact_plasma
		blendfunc GL_SRC_ALPHA GL_ONE
		rgbGen vertex
		nextbundle
		map $dlight
	}
}

staff
{
	entityMergable
	{
		AnimMap 20
		map gfx/zombie/staff
		map gfx/zombie/staff2
		map gfx/zombie/staff3
		blendfunc blend
		rgbGen vertex
	}
}