loadStuff()
{
	level._effect["bouxhead"] = loadfx("fx/boux/head.efx");
	level._effect["bouxfoot"] = loadfx("fx/boux/foot.efx");
	level._effect["bouxleg"] = loadfx("fx/boux/leg.efx");
	level._effect["bouxarm"] = loadfx("fx/boux/arm.efx");
	level._effect["bouxhand"] = loadfx("fx/boux/hand.efx");
	level._effect["bouxbody"] = loadfx("fx/boux/body.efx");
	level._effect["bouxscream"] = loadfx("fx/boux/scream.efx");

	level.ufa_logoText = newHudElem();
	level.ufa_logoText.x = 2;
	level.ufa_logoText.y = 474;
	level.ufa_logoText.alignX = "left";
	level.ufa_logoText.alignY = "middle";
	level.ufa_logoText.alpha = 1;
	level.ufa_logoText.sort = -3;
	level.ufa_logoText.fontScale = 0.6;
	level.ufa_logoText.archieved = true;
	level.ufa_logoText setText( &"^4{^1U^9nited ^1F^9ederation ^1A^9cademy^4} ^1B^7adass ^1R^7ifle ^1M^7od v1.2 ^3By Boux ^4--- ^2www.ufaclan.net^7" );

	precacheString(&"^1Badass^0-^7Scoped^0-^7Mosin-Nagant");	precacheString(&"^2Yes");	precacheString(&"^1No");	precacheString(&"Undefined");	precacheString(&"^1Badass^0-^7Scoped^0-^7Kar98k");	precacheString(&"^1Badass^0-^7Lee-Enfield");	precacheString(&"^1Badass^0-^7Springfield");	precacheString(&"^1Badass^0-^7Mosin-Nagant");	precacheString(&"^1Badass^0-^7Kar98k");

	maps\mp\_cvarDef::main( "scr_badass", 0, 0, 1, "int" );
	maps\mp\_cvarDef::main( "scr_badass_score", 15, 0, 90, "int" );
}

loadPlayerStuff(){	if(!isdefined(self.kar98kScore))		self.kar98kScore = 0;	if(!isdefined(self.kar98kSniperScore))		self.kar98kSniperScore = 0;	if(!isdefined(self.nagantScore))		self.nagantScore = 0;	if(!isdefined(self.nagantSniperScore))		self.nagantSniperScore = 0;	if(!isdefined(self.enfieldScore))		self.enfieldScore = 0;	if(!isdefined(self.springfieldScore))		self.springfieldScore = 0;			if(!isdefined(self.superKar98k))		self.superKar98k = 0;	if(!isdefined(self.superKar98kSniper))		self.superKar98kSniper = 0;	if(!isdefined(self.superNagant))		self.superNagant = 0;	if(!isdefined(self.superNagantSniper))		self.superNagantSniper = 0;	if(!isdefined(self.superEnfield))		self.superEnfield = 0;	if(!isdefined(self.superSpringfield))		self.superSpringfield = 0;}

blood( eInflictor, attacker, iDamage, sMeansOfDeath, sWeapon, vDir, sHitLoc )
{
	if(iDamage >= self.health)
	{
		if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "head")
		{
			playfxontag (level._effect["bouxhead"],self,"Bip01 Head");
			if(isdefined(self.hatModel))
				self detach( self.hatModel , "");
			if( !isDefined( self.ufa_headmodel ) || self.ufa_headmodel != self getAttachModelName( 0 ) )
				self.ufa_headmodel = self getAttachModelName( 0 );
			self detach( self.ufa_headmodel , "");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "helmet")
		{
			playfxontag (level._effect["bouxhead"],self,"Bip01 Head");
			if(isdefined(self.hatModel))
				self detach( self.hatModel , "");
			if( !isDefined( self.ufa_headmodel ) || self.ufa_headmodel != self getAttachModelName( 0 ) )
				self.ufa_headmodel = self getAttachModelName( 0 );
			self detach( self.ufa_headmodel , "");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "left_foot")
		{
			playfxontag (level._effect["bouxfoot"],self,"Bip01 L Hand");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "right_foot")
		{
			playfxontag (level._effect["bouxfoot"],self,"Bip01 L Hand");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "left_leg_lower")
		{
			playfxontag (level._effect["bouxleg"],self,"Bip01 L Hand");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "right_leg_lower")
		{
			playfxontag (level._effect["bouxleg"],self,"Bip01 L Hand");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "left_leg_upper")
		{
			playfxontag (level._effect["bouxleg"],self,"Bip01 L Hand");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "right_leg_upper")
		{
			playfxontag (level._effect["bouxleg"],self,"Bip01 L Hand");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "left_hand")
		{
			playfxontag (level._effect["bouxhand"],self,"Bip01 L Hand");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "right_hand")
		{
			playfxontag (level._effect["bouxhand"],self,"Bip01 R Hand");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "left_arm_lower")
		{
			playfxontag (level._effect["bouxarm"],self,"Bip01 R Forearm");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "right_arm_lower")
		{
			playfxontag (level._effect["bouxarm"],self,"Bip01 R Forearm");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "left_arm_upper")
		{
			playfxontag (level._effect["bouxarm"],self,"Bip01 R UpperArm");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "right_arm_upper")
		{
			playfxontag (level._effect["bouxarm"],self,"Bip01 R UpperArm");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "neck")
		{
			playfxontag (level._effect["bouxhead"],self,"Bip01 Head");
			if(isdefined(self.hatModel))
				self detach( self.hatModel , "");
			if( !isDefined( self.ufa_headmodel ) || self.ufa_headmodel != self getAttachModelName( 0 ) )
				self.ufa_headmodel = self getAttachModelName( 0 );
			self detach( self.ufa_headmodel , "");
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "torso_lower")
		{
			fx = level._effect["bouxbody"];
			playFx( fx, self.origin + (0, 0, 50) );
		}
		else if(sMeansOfDeath != "MOD_MELEE" && sHitLoc == "torso_upper")
		{
			fx = level._effect["bouxbody"];
			playFx( fx, self.origin + (0, 0, 50) );
		}
		else
		{
			fx = level._effect["bouxhand"];
			playFx( fx, self.origin + (0, 0, 50) );
		}
	}
}


restrict_boux(response)
{
			switch(response)		
			{
			case "springfield_mp":
				if(!getcvar("scr_allow_springfield"))
				{
					self iprintln(&"MPSCRIPT_SPRINGFIELD_IS_A_RESTRICTED");
					response = "restricted";
				}
				break;

			case "enfield_mp":
				if(!getcvar("scr_allow_enfield"))
				{
					self iprintln(&"MPSCRIPT_LEEENFIELD_IS_A_RESTRICTED");
					response = "restricted";
				}
				break;

			case "mosin_nagant_mp":
				if(!getcvar("scr_allow_nagant"))
				{
					self iprintln(&"MPSCRIPT_MOSINNAGANT_IS_A_RESTRICTED");
					response = "restricted";
				}
				break;

			case "mosin_nagant_sniper_mp":
				if(!getcvar("scr_allow_nagantsniper"))
				{
					self iprintln(&"MPSCRIPT_SCOPED_MOSINNAGANT_IS");
					response = "restricted";
				}
				break;

			case "kar98k_mp":
				if(!getcvar("scr_allow_kar98k"))
				{
					self iprintln(&"MPSCRIPT_KAR98K_IS_A_RESTRICTED");
					response = "restricted";
				}
				break;


			case "kar98k_sniper_mp":
				if(!getcvar("scr_allow_kar98ksniper"))
				{
					self iprintln(&"MPSCRIPT_SCOPED_KAR98K_IS_A_RESTRICTED");
					response = "restricted";
				}
				break;

			default:
				self iprintln(&"MPSCRIPT_UNKNOWN_WEAPON_SELECTED");
				response = "restricted";
				break;
			}
	return response;
}

loadText(){
	if(isDefined(self.kar98kText))
		self.kar98kText destroy();
	if(isDefined(self.kar98kText2))
		self.kar98kText2 destroy();

	if(isDefined(self.nagantText))
		self.nagantText destroy();
	if(isDefined(self.nagantText2))
		self.nagantText2 destroy();

	if(isDefined(self.nagantSniperText))
		self.nagantSniperText destroy();
	if(isDefined(self.nagantSniperText2))
		self.nagantSniperText2 destroy();

	if(isDefined(self.kar98kSniperText))
		self.kar98kSniperText destroy();
	if(isDefined(self.kar98kSniperText2))
		self.kar98kSniperText2 destroy();

	if(isDefined(self.enfieldText))
		self.enfieldText destroy();
	if(isDefined(self.enfieldText2))
		self.enfieldText2 destroy();

	if(isDefined(self.springfieldText))
		self.springfieldText destroy();
	if(isDefined(self.springfieldText2))
		self.springfieldText2 destroy();

	badass = getCvarInt("scr_badass");

	if(badass == 1)
	{
	self.kar98kText = newClientHudElem( self );
	self.kar98kText.x = 593;
	self.kar98kText.y = 40;
	self.kar98kText.alignX = "right";
	self.kar98kText.alignY = "middle";
	self.kar98kText.alpha = 1;
	self.kar98kText.fontScale = 0.6;
	self.kar98kText setText(&"^1Badass^0-^7Kar98k");
	self.kar98kText2 = newClientHudElem( self );
	self.kar98kText2.x = 599;
	self.kar98kText2.y = 40;
	self.kar98kText2.alignX = "left";
	self.kar98kText2.alignY = "middle";
	self.kar98kText2.alpha = 1;
	self.kar98kText2.fontScale = 0.6;
	if(self.superKar98k == 1)
		self.kar98kText2 setText(&"^2Yes");
	else
		self.kar98kText2 setText(&"^1No");

	self.kar98kSniperText = newClientHudElem( self );
	self.kar98kSniperText.x = 593;
	self.kar98kSniperText.y = 50;
	self.kar98kSniperText.alignX = "right";
	self.kar98kSniperText.alignY = "middle";
	self.kar98kSniperText.alpha = 1;
	self.kar98kSniperText.fontScale = 0.6;
	self.kar98kSniperText setText(&"^1Badass^0-^7Scoped^0-^7Kar98k");
	self.kar98kSniperText2 = newClientHudElem( self );
	self.kar98kSniperText2.x = 599;
	self.kar98kSniperText2.y = 50;
	self.kar98kSniperText2.alignX = "left";
	self.kar98kSniperText2.alignY = "middle";
	self.kar98kSniperText2.alpha = 1;
	self.kar98kSniperText2.fontScale = 0.6;
	if(self.superKar98kSniper == 1)
		self.kar98kSniperText2 setText(&"^2Yes");
	else
		self.kar98kSniperText2 setText(&"^1No");

	self.nagantText = newClientHudElem( self );
	self.nagantText.x = 593;
	self.nagantText.y = 60;
	self.nagantText.alignX = "right";
	self.nagantText.alignY = "middle";
	self.nagantText.alpha = 1;
	self.nagantText.fontScale = 0.6;
	self.nagantText setText(&"^1Badass^0-^7Mosin-Nagant");
	self.nagantText2 = newClientHudElem( self );
	self.nagantText2.x = 599;
	self.nagantText2.y = 60;
	self.nagantText2.alignX = "left";
	self.nagantText2.alignY = "middle";
	self.nagantText2.alpha = 1;
	self.nagantText2.fontScale = 0.6;
	if(self.superNagant == 1)
		self.nagantText2 setText(&"^2Yes");
	else
		self.nagantText2 setText(&"^1No");

	self.nagantSniperText = newClientHudElem( self );
	self.nagantSniperText.x = 593;
	self.nagantSniperText.y = 70;
	self.nagantSniperText.alignX = "right";
	self.nagantSniperText.alignY = "middle";
	self.nagantSniperText.alpha = 1;
	self.nagantSniperText.fontScale = 0.6;
	self.nagantSniperText setText(&"^1Badass^0-^7Scoped^0-^7Mosin-Nagant");
	self.nagantSniperText2 = newClientHudElem( self );
	self.nagantSniperText2.x = 599;
	self.nagantSniperText2.y = 70;
	self.nagantSniperText2.alignX = "left";
	self.nagantSniperText2.alignY = "middle";
	self.nagantSniperText2.alpha = 1;
	self.nagantSniperText2.fontScale = 0.6;
	if(self.superNagantSniper == 1)
		self.nagantSniperText2 setText(&"^2Yes");
	else
		self.nagantSniperText2 setText(&"^1No");

	self.enfieldText = newClientHudElem( self );
	self.enfieldText.x = 593;
	self.enfieldText.y = 80;
	self.enfieldText.alignX = "right";
	self.enfieldText.alignY = "middle";
	self.enfieldText.alpha = 1;
	self.enfieldText.fontScale = 0.6;
	self.enfieldText setText(&"^1Badass^0-^7Lee-Enfield");
	self.enfieldText2 = newClientHudElem( self );
	self.enfieldText2.x = 599;
	self.enfieldText2.y = 80;
	self.enfieldText2.alignX = "left";
	self.enfieldText2.alignY = "middle";
	self.enfieldText2.alpha = 1;
	self.enfieldText2.fontScale = 0.6;
	if(self.superEnfield == 1)
		self.enfieldText2 setText(&"^2Yes");
	else
		self.enfieldText2 setText(&"^1No");

	self.springfieldText = newClientHudElem( self );
	self.springfieldText.x = 593;
	self.springfieldText.y = 90;
	self.springfieldText.alignX = "right";
	self.springfieldText.alignY = "middle";
	self.springfieldText.alpha = 1;
	self.springfieldText.fontScale = 0.6;
	self.springfieldText setText(&"^1Badass^0-^7Springfield");
	self.springfieldText2 = newClientHudElem( self );
	self.springfieldText2.x = 599;
	self.springfieldText2.y = 90;
	self.springfieldText2.alignX = "left";
	self.springfieldText2.alignY = "middle";
	self.springfieldText2.alpha = 1;
	self.springfieldText2.fontScale = 0.6;
	if(self.superSpringfield == 1)
		self.springfieldText2 setText(&"^2Yes");
	else
	{
		self.springfieldText2 setText(&"^1No");
	}
	}
}